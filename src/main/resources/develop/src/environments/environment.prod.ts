import {GLOBAL} from './global';
import * as deepmerge from 'deepmerge';
import {Settings} from './settings';

export const environment = deepmerge.all([{}, GLOBAL, {
    production: true
}]) as Settings;
