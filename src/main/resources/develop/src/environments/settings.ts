export interface Settings {
    production: boolean;
    version: string;
}
