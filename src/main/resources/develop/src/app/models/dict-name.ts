export enum DictName {
    ArchiveRr = 'archives-rr', // Архивы для читального зала
    RrArchive = 'rrs-archive', // Читальные залы для архива
    Month = 'months', // месяцы
    OrderState = 'order-states', // Статус заказа
    OrderType = 'order-types', // Вид заказа
    ArchiveShort = 'archives-short', // Архивы, отделы - сокращенные названия
    User = 'users', // Пользователи
    Executor = 'executors', // Исполнители
    ReadingRoom = 'reading-rooms', // Читальный зал
    AccessGroup = 'access-groups', // Группы доступа
    AccessRole = 'access-roles', // Права доступа

    Country = 'countries', // Гражданство
    RejectReason = 'reject-reasons', // Причина отказа
    PermissionIssue = 'permission-issues', // Разрешение на выдачу
    Archive = 'archives', // Архивы, отделы
    ScienceRank = 'science-ranks', // Научное звание
    Education = 'educations', // Образование
    TopicHeading = 'topic-headings', // Рубрикатор тем
    RrDetails = 'reading-rooms', // Адреса читальных залов - только для админки
    Storage = 'storages', // Архивохранилища
    ResearchTheme = 'research-themes' // Названия тем пользователя для определенного архива
}

