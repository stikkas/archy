export class User {
    id: number;
    firstName: string;
    lastName: string;
    middleName: string;
    login: string;
    roles?: string[];
    groups?: string[] | number[];
    rraddress: string;
    archiveId: number;
    storageId: number;
}
