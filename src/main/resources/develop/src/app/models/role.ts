export enum Role {
// Администратор
    Admin = 'ADMIN',
// Сотрудник ЧЗ1
    Rr1 = 'RR1',
// Сотрудник ЧЗ2
    Rr2 = 'RR2',
// Сотрудник ЧЗ3
    Rr3 = 'RR3',
// Начальник всех читальных залов
    ChiefRr = 'CHIEF_RR',
// Начальник отдела хранения
    ChiefArchive = 'CHIEF_ARCHIVE',
// Исполнитель
    Executor = 'EXECUTOR'
}

// tslint:disable-next-line:no-namespace
export namespace Role {
    // export function hasStaffRole(roles: string[]) {
    //     return roles && [Role.Rr1, Role.Rr2, Role.Rr3, Role.ChiefArchive, Role.ChiefRr, Role.Keeper, Role.Executor]
    //         .some(it => roles.indexOf(it) !== -1);
    // }

    export function hasAdminRole(roles: string[]) {
        return roles && roles.indexOf(Role.Admin) !== -1;
    }

    /**
     * Проверяет есть ли хоть одна роль из желаемых среди ролей пользователя
     * roles - роли пользователя
     * required - список ролей, одна из которых должна присутсвовать
     */
    export function hasAny(roles: string[], required: Role[]) {
        return roles && required.some(r => roles.indexOf(r) > -1);
    }
}

