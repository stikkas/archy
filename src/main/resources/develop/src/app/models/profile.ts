import {ArchRecord} from './arch-record';

export class Profile {
    id: number;
    lastName: string;
    firstName: string;
    middleName: string;
    changeFIO: string;
    birthYear: number;
    countryId = 1;
    personNumber: number;
    firstRegDate = new Date();
    issuePassDate: Date;
    ban = false;
    banCause: string;
    regs: ArchRecord[] = [];
    organization: string;
    position: string;
    educationId: number;
    scienceRankId: number;
    remark: string;
    oldNumbers: string;
    email: string;
    registered = false;

    static normalizeDates(profile: Profile): Profile {
        if (typeof profile.firstRegDate === 'string') {
            profile.firstRegDate = new Date(profile.firstRegDate);
        }
        if (typeof profile.issuePassDate === 'string') {
            profile.issuePassDate = new Date(profile.issuePassDate);
        }
        profile.regs.forEach(it => {
            if (typeof it.approveDate === 'string') {
                it.approveDate = new Date(it.approveDate);
            }
            it.topics.forEach(t => {
                if (typeof t.startDate === 'string') {
                    t.startDate = new Date(t.startDate);
                }
                if (typeof t.endDate === 'string') {
                    t.endDate = new Date(t.endDate);
                }
            });
        });
        return profile;
    }
}

