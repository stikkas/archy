import {ResearchTopic} from './research-topic';

export interface ArchRecord {
    id: number;
    archiveId: number;
    personNumber: string;
    approveDate: Date;
    topics: ResearchTopic[];
}
