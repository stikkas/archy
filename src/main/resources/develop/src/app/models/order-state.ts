export enum OrderState {
    Pre = 'PRE',
    Accept = 'ACCEPT',
    Dump = 'DUMP',
    Cancel1 = 'CANCEL1',
    Cancel2 = 'CANCEL2',
    Cancel3 = 'CANCEL3',
    Reject = 'REJECT',
    Onexec = 'ONEXEC',
    Ready = 'READY',
    Exec = 'EXEC',
    Return = 'RETURN'
}

