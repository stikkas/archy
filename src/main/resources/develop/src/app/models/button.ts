import {ButtonMenu} from './button-menu';

export interface Button {
    action?: () => void;
    clazz?: string;
    ico: string;
    label: string;
    disabled?: boolean;
    hidden?: boolean;
    menu?: ButtonMenu[];
}
