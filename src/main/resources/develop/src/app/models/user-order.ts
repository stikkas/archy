export class UserOrder {
    id: number;
    /**
     * № заказа
     */
    nymber: number;

    /**
     * Дата приема заказа
     */
    regDate: Date;

    /**
     * Читальный зал
     */
    room: string;

    /**
     * Отдел хранения
     */
    archive: string;

    /**
     * Статус заказа
     */
    state: string;
}
