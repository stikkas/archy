export interface ResearchTopic {
    id: number;
    topicId: number;
    themeName: string;
    period: string;
    target: string;
    startDate: Date;
    endDate: Date;
}
