export class Visit {
    id: number;
    date: Date;
    archives: string[];
    room: string;
}
