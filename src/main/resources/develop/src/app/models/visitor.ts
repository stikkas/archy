import {Role} from './role';

export class Visitor {
    id: number;
    visitDate = new Date();
    lastName: string;
    firstName: string;
    middleName: string;
    archives: number[] | string[];
    once: boolean;
    docCause: string;
    target: string;
    usedDocs: string;
    remark: string;
    registered: boolean;
    profileId: number;
    rr: string;

    constructor(roles?: string[]) {
        if (roles) {
            for (const role of roles) {
                if (Role.Rr1 == role || Role.Rr2 == role || Role.Rr3 == role) {
                    this.rr = role;
                    break;
                }
            }
        }
    }

    static normalizeDates(visitor: Visitor): Visitor {
        if (typeof visitor.visitDate === 'string') {
            visitor.visitDate = new Date(visitor.visitDate);
        }
        return visitor;
    }
}
