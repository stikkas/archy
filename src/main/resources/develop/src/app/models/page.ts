export class Page<T> {
    data: Array<T>;
    size: number; // размер страницы
    count: number; // всего найдено элементов
    pages: number; // всего страниц
    page: number; // номер текущей страницы начинается с 1
    last: boolean; // текущая страница первая
    first: boolean; // текущая страница последняя

    constructor(data: Array<T> = [], size: number = 50, count: number = 0, page: number = 1) {
        this.data = data;
        this.size = size;
        this.count = count;
        this.page = page;
        this.pages = count === 0 ? 0 : count <= size ? 1 : Math.floor(count / size) + ((count % size === 0) ? 0 : 1);
        this.last = this.pages === 0 || page === this.pages;
        this.first = this.pages === 0 || page === 1;
    }
}

