export interface Dict {
    value: number | string;
    label: string;
    short?: string;
}
