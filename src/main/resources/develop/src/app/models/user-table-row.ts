export class UserTableRow {
    id: number;
    /**
     * Номер личного дела
     */
    personNumber: number;

    /**
     * Фамилия
     */
    lastName: string;

    /**
     * Имя
     */
    firstName: string;

    /**
     * Отчество
     */
    middleName: string;

    /**
     * Год рождения
     */
    birthYear: number;

    /**
     * Отдел хранения
     */
    archives: string[];
}
