export class VisitorTableRow {
    id: number;
    /**
     * Дата посещения
     */
    visitDate: Date;

    /**
     * Фамилия
     */
    lastName: string;

    /**
     * Имя
     */
    firstName: string;

    /**
     * Отчество
     */
    middleName: string;

    /**
     * Отдел хранения
     */
    target: string;
}

