import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';
import * as fromRoot from '../../reducers/index';
import {Store} from '@ngrx/store';

@Component({
    selector: 'aisrr-header',
    templateUrl: './header.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
    title$ = this.store.select(fromRoot.title);
    buttons$ = this.store.select(fromRoot.buttons);

    constructor(private store: Store<fromRoot.State>) {
    }
}

