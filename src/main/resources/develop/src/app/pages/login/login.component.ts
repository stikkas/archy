import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Router} from '@angular/router';

@Component({
    templateUrl: './login.component.html',
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
    group = new FormGroup({
        user: new FormControl('', Validators.required),
        pass: new FormControl('', Validators.required)
    });

    constructor(private authService: AuthService, router: Router) {
        authService.getUser().pipe(takeUntil(componentDestroyed(this)))
            .subscribe(res => router.navigate(['/main']));
    }

    submit() {
        this.authService.login(this.group.value);
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
    }
}

