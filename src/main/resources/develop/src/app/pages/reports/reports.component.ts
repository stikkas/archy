import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromButtons from '../../reducers/buttons';
import {Router} from '@angular/router';
import {title} from '../../reducers/title';
import {DateOptions} from '../../injectors/date-optinos';
import {ReportsService} from '../../services/reports.service';

@Component({
    templateUrl: './reports.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ReportsComponent {
    reports = [{
        title: 'Посещаемость читального зала',
        startDate: null,
        endDate: null,
        start: null,
        end: null,
        create: me => this.rs.create('quantity-rr', {start: me.startDate, end: me.endDate}),
        disabled: me => !(me.startDate && me.endDate)
    }];

    constructor(private store: Store<fromRoot.State>, router: Router, @Inject(DateOptions) public dops,
                private rs: ReportsService) {
        store.dispatch(fromButtons.buttons([{
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));

        store.dispatch(title({text: 'Отчеты'}));
    }

    dateChanged(date: any, obj: any, field: string, elId: string) {
        if (date) {
            obj[field] = date.singleDate.jsDate;
        } else {
            obj[field] = null;
            (document.getElementById(elId) as HTMLInputElement).value = '';
        }
    }
}

