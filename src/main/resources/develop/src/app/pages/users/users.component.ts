import {Component, ViewEncapsulation} from '@angular/core';
import {UsersService} from '../../services/users.service';
import * as fromRoot from '../../reducers';
import {Store} from '@ngrx/store';
import {SearchComponent} from '../search.component';
import {UserTableRow} from '../../models/user-table-row';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
    templateUrl: './users.component.html',
    encapsulation: ViewEncapsulation.None
})
export class UsersComponent extends SearchComponent<UserTableRow> {
    constructor(ss: UsersService, store: Store<fromRoot.State>, router: Router, auth: AuthService) {
        super(ss, store, router, 'Пользователи', '/users/profiles', auth);
    }
}


