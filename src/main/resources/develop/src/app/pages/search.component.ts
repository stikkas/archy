import {OnDestroy, OnInit} from '@angular/core';
import {SearchService} from '../services/search.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers/index';
import * as fromButtons from '../reducers/buttons';
import {Router} from '@angular/router';
import {title} from '../reducers/title';
import {SortEvent} from '../models/sort-event';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Role} from '../models/role';
import {AuthService} from '../services/auth.service';
import {Button} from '../models/button';

export abstract class SearchComponent<T> implements OnDestroy, OnInit {
    page$ = this.ss.getPage();
    filter = this.ss.filter;

    protected constructor(protected ss: SearchService<T>, private store: Store<fromRoot.State>,
                          router: Router, label: string, addUrl: string, auth: AuthService, addBtn?: Button) {
        store.dispatch(title({text: label}));
        store.dispatch(fromButtons.buttons([
            addBtn || {
                action: () => router.navigate([addUrl]),
                ico: 'fa-plus-square',
                disabled: true,
                label: 'Добавить'
            }, {
                action: () => this.load(),
                ico: 'fa-search',
                clazz: 'ml-2',
                label: 'Поиск'
            }, {
                action: () => this.clear(),
                ico: 'fa-times',
                clazz: 'ml-2',
                label: 'Очистить'
            }, {
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));
        auth.getUser().pipe(takeUntil(componentDestroyed(this))).subscribe(user => {
            if (user) {
                if (Role.hasAny(user.roles, [Role.Rr3, Role.Rr2, Role.Rr1])) {
                    store.dispatch(fromButtons.change({
                        0: {disabled: false}
                    }));
                }
            }
        });
    }

    load(page?: number, sort?: SortEvent) {
        this.ss.loadPage(page, sort);
    }

    clear() {
        this.ss.clear();
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }
}

