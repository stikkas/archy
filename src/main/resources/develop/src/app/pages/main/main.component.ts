import {Component, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {map} from 'rxjs/operators';
import {Role} from '../../models/role';
import {buttons} from '../../reducers/buttons';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../reducers/index';

@Component({
    templateUrl: './main.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MainComponent {

    admin$ = this.authService.getUser().pipe(map(it => Role.hasAdminRole(it.roles)));
    canUsers$ = this.authService.getUser().pipe(map(it => Role.hasAny(it.roles,
        [Role.Admin, Role.Rr1, Role.Rr2, Role.Rr3, Role.ChiefRr, Role.ChiefArchive])));

    constructor(private authService: AuthService, store: Store<fromRoot.State>) {
        store.dispatch(buttons([{
                action: () => window.location.href = '/logout',
                ico: 'fa-sign-out',
                label: 'Выход'
            }]
        ));
    }
}

