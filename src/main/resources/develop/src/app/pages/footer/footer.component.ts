import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {timer} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../../models/user';

@Component({
    selector: 'aisrr-footer',
    templateUrl: './footer.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {
    @Input() user: User;
    date$ = timer(0, 300000).pipe(map(() => new Date()));
}
