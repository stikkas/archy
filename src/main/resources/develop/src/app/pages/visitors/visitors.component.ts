import {Component, Inject, ViewEncapsulation} from '@angular/core';
import * as fromRoot from '../../reducers';
import {Store} from '@ngrx/store';
import {VisitorsService} from '../../services/visitors.service';
import {SearchComponent} from '../search.component';
import {VisitorTableRow} from '../../models/visitor-table-row';
import {DateOptions} from '../../injectors/date-optinos';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
    templateUrl: './visitors.component.html',
    encapsulation: ViewEncapsulation.None
})
export class VisitorsComponent extends SearchComponent<VisitorTableRow> {
    dates = {
        from: null,
        to: null
    };

    constructor(vs: VisitorsService, store: Store<fromRoot.State>, @Inject(DateOptions) public dops,
                router: Router, auth: AuthService) {
        super(vs, store, router, 'Посетители', '/visitors/new', auth);
    }

    dateChanged(date: any, obj: any, field: string, elId: string) {
        if (date) {
            obj[field] = date.singleDate.jsDate;
        } else {
            obj[field] = null;
            (document.getElementById(elId) as HTMLInputElement).value = '';
        }
    }

    clear() {
        super.clear();
        this.dates.from = null;
        this.dates.to = null;
    }
}


