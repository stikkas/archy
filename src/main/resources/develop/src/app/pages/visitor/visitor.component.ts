import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import * as fromRoot from '../../reducers/index';
import {Store} from '@ngrx/store';
import {DictService} from '../../services/dict.service';
import {ActivatedRoute, Router} from '@angular/router';
import {VisitorService} from '../../services/visitor.service';
import {Visitor} from '../../models/visitor';
import {DictName} from '../../models/dict-name';
import * as fromCardMode from '../../reducers/card-mode';
import * as fromProfile from '../../reducers/profile';
import {DateOptions} from '../../injectors/date-optinos';
import {VisitorsService} from '../../services/visitors.service';
import {title} from '../../reducers/title';
import * as fromButtons from '../../reducers/buttons';
import {Dict} from '../../models/dict';
import {combineLatest, concat, forkJoin, Observable, of, Subject} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, switchMap, takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Role} from '../../models/role';
import {AuthService} from '../../services/auth.service';

@Component({
    templateUrl: './visitor.component.html',
    encapsulation: ViewEncapsulation.None
})
export class VisitorComponent implements OnInit, OnDestroy {
    editMode$ = this.store.select(fromRoot.selectEditMode);
    data: Visitor;
    usersInput$ = new Subject<string>();
    users$: Observable<Dict[]>;
    archivesOfRr$: Observable<Dict[]>;
    dates = {
        visitDate: null
    };
    dn = DictName;
    dicts = {
        [DictName.Archive]: null as Dict[],
        [DictName.ReadingRoom]: null as Dict[]
    };
    @ViewChild('vd', {static: true}) visitDateEl: ElementRef;

    constructor(private store: Store<fromRoot.State>, private dictService: DictService,
                private route: ActivatedRoute, private visitorService: VisitorService,
                private router: Router, @Inject(DateOptions) public dops,
                private visitorsService: VisitorsService, private auth: AuthService) {
        dictService.getDicts(this.dicts);
        store.dispatch(fromButtons.buttons([{
                action: () => router.navigate(['/visitors']),
                ico: 'fa-arrow-left',
                label: 'Вернуться',
            }, {
                action: () => router.navigate(['/visitors/new']),
                clazz: 'ml-2',
                ico: 'fa-plus-square',
                label: 'Добавить',
            }, {
                action: () => {
                    store.dispatch(fromCardMode.editMode());
                },
                clazz: 'ml-2',
                ico: 'fa-edit',
                label: 'Изменить',
            }, {
                action: () => this.save(),
                clazz: 'ml-2',
                ico: 'fa-save',
                label: 'Сохранить',
                hidden: true
            }, {
                action: () => this.remove(),
                clazz: 'ml-2',
                ico: 'fa-times',
                label: 'Удалить',
            }, {
                action: () => this.save(true),
                clazz: 'ml-3',
                ico: 'fa-check-circle',
                label: 'Регистрировать'
            }, {
                action: () => window.print(),
                clazz: 'ml-2',
                ico: 'fa-print',
                label: 'Печать'
            }, {
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));
        store.dispatch(title({text: 'Посетитель'}));
        combineLatest(this.editMode$, store.select(fromRoot.selectId), auth.getUser(), store.select(fromRoot.selectRegistered))
            .pipe(takeUntil(componentDestroyed(this))).subscribe(([mode, id, user, registered]) => {
            if (mode) {
                store.dispatch(fromButtons.change({
                    1: {disabled: true},
                    2: {hidden: true},
                    3: {hidden: false, disabled: false},
                    4: {disabled: !id || registered},
                    5: {disabled: registered}
                }));
            } else {
                const noAccess = !(user && Role.hasAny(user.roles, [Role.Rr1, Role.Rr2, Role.Rr3]));
                store.dispatch(fromButtons.change({
                    1: {disabled: noAccess},
                    2: {hidden: false, disabled: noAccess},
                    3: {hidden: true},
                    4: {disabled: !id || noAccess || registered},
                    5: {disabled: noAccess || registered}
                }));
            }
        });
    }

    ngOnInit() {
        this.route.data.subscribe((data: { visitor: Visitor }) => {
            const visitor = this.data = data.visitor;
            if (visitor.visitDate) {
                this.dates.visitDate = {isRange: false, singleDate: {jsDate: visitor.visitDate}};
            }
            if (visitor.rr) {
                this.readingRoomChanged({value: visitor.rr});
            }
        });
        this.users$ = concat(this.data.profileId ? this.dictService.load(DictName.User, this.data.lastName) : of([]),
            this.usersInput$.pipe(
                debounceTime(200),
                distinctUntilChanged(),
                switchMap(term => this.dictService.load(DictName.User, term).pipe(
                    catchError(() => of([] as Dict[])), // empty list on error
                ))
            ));
    }

    save(registered = false) {
        this.visitorService.save({...this.data, registered: this.data.registered || registered}).subscribe((res: Visitor) => {
            if (res) {
                if (res.registered !== this.data.registered) {
                    this.store.dispatch(fromProfile.profile({payload: {registered: res.registered}}));
                    this.store.dispatch(fromCardMode.viewMode());
                }

                if (!this.data.id) {
                    this.router.navigate(['/visitors', res.id]);
                } else if (this.data.id === res.id) {
                    this.data = res;
                } else {
                    console.log('Receive another id.');
                }
                this.visitorsService.loadPage();
            }
        });
    }

    remove() {
        this.visitorService.remove(this.data.id).subscribe(res => {
            if (res) {
                this.store.dispatch(fromCardMode.viewMode());
                this.visitorsService.loadPage();
                this.router.navigate(['/visitors']);
            }
        });
    }

    getLabel(id: number | string | number[], dict: DictName) {
        if (id instanceof Array) {
            if (id.length) {
                return forkJoin(id.map(it => this.dictService.getLabel(it, dict))).pipe(map(it => it.join('<br>')));
            } else {
                return of('');
            }
        } else {
            return this.dictService.getLabel(id, dict);
        }
    }

    dateChanged(date: any) {
        if (date) {
            this.data.visitDate = date.singleDate.jsDate;
        } else {
            this.data.visitDate = null;
            this.visitDateEl.nativeElement.value = '';
        }
    }

    ngOnDestroy(): void {
    }

    setFio(data: any) {
        if (data && data.label) {
            const fio = data.label.split(' ');
            this.data.lastName = fio[0];
            this.data.firstName = fio[1];
            this.data.middleName = fio[2] && fio[2].replace(',', '');
        } else {
            this.data.lastName = this.data.firstName = this.data.middleName = null;
        }
    }

    /**
     * Возвращает текст который должен отрисовываться в textarea, но отрисовывается как простой текст
     * @param remark - текст
     */
    textArea(remark: string) {
        return remark ? remark.split('\n').filter(it => it).join('<br>') : '';
    }

    /**
     * Обновляем список отделов хранения, в зависимости от выбранного читального зала
     * @param rr - выбранный читальный зал {value: string, label: string}
     */
    readingRoomChanged(rr: any) {
        if (rr) {
            this.archivesOfRr$ = this.dictService.load(DictName.ArchiveRr, rr.value)
                .pipe(map(it => {
                    const archives = this.data.archives;
                    if (archives && archives.length) {
                        this.data.archives = (archives as number[]).filter(id => it.some(a => a.value == id));
                    }
                    return it;
                }));
        }
    }
}
