import {InjectionToken} from '@angular/core';
import {IAngularMyDpOptions} from 'angular-mydatepicker';

export const DateOptions = new InjectionToken<IAngularMyDpOptions>('date-options');
