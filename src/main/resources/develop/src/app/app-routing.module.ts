import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {MainComponent} from './pages/main/main.component';
import {UsersComponent} from './pages/users/users.component';
import {VisitorsComponent} from './pages/visitors/visitors.component';
import {VisitorComponent} from './pages/visitor/visitor.component';
import {VisitorResolveService} from './services/visitor-resolve.service';
import {AuthGuard} from './guards/auth.guard';
import {LoginComponent} from './pages/login/login.component';
import {AuthLoginGuard} from './guards/auth-login.guard';
import {AdminGuard} from './guards/admin.guard';
import {ReportsComponent} from './pages/reports/reports.component';

const routes: Routes = [{
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard]
}, {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuard]
}, {
    path: 'visitors',
    component: VisitorsComponent,
    canActivate: [AuthGuard]
}, {
    path: 'reports',
    component: ReportsComponent,
    canActivate: [AuthGuard]
}, {
    path: 'visitors/new',
    component: VisitorComponent,
    resolve: {visitor: VisitorResolveService},
    canActivate: [AuthGuard]
}, {
    path: 'visitors/:id',
    component: VisitorComponent,
    resolve: {visitor: VisitorResolveService},
    canActivate: [AuthGuard]
}, {
    path: 'users/profiles',
    loadChildren: () => import('./modules/profiles/profiles.module').then(mod => mod.ProfilesModule),
    canLoad: [AuthGuard]
}, {
    path: 'orders',
    loadChildren: () => import('./modules/orders/orders.module').then(mod => mod.OrdersModule),
    canLoad: [AuthGuard]
}, {
    path: 'admin',
    loadChildren: () => import('./modules/admin/admin.module').then(mod => mod.AdminModule),
    canLoad: [AdminGuard]
}, {
    path: 'enter',
    component: LoginComponent,
    canActivate: [AuthLoginGuard]
}, {
    path: '**',
    redirectTo: 'main'
}];

@NgModule({
    imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

