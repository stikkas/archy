import {Action, createAction, createReducer, on, props} from '@ngrx/store';

export const title = createAction('[Title]', props<{ text: string }>());

export const initialState = {text: ''};

export interface State {
    text: string;
}

const featureReducer = createReducer(initialState,
    on(title, (state: State, data: { text: string }) => ({...state, ...data}))
);

export function reducer(state: State | undefined, action: Action) {
    return featureReducer(state, action);
}

