import {Action, createAction, createReducer, on, props} from '@ngrx/store';
import {OrderState} from '../models/order-state';

export interface OrderPayload {
    status?: OrderState;
    id?: number;
    executorId?: number;
}

export const order = createAction('[Order]', props<OrderPayload>());

export const initialState = {status: null, id: null, executorId: null};

export interface State {
    status: OrderState;
    executorId: number;
    id: number;
}

const featureReducer = createReducer(initialState,
    on(order, (state: State, data: OrderPayload) => ({...state, ...data}))
);

export function reducer(state: State | undefined, action: Action) {
    return featureReducer(state, action);
}

