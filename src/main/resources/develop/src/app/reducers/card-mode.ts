import {Action, createAction, createReducer, on} from '@ngrx/store';

export const viewMode = createAction('[EditMode] view');
export const editMode = createAction('[EditMode] edit');

export const initialState = {edit: false};

export interface State {
    edit: boolean;
}

const featureReducer = createReducer(initialState,
    on(viewMode, (state: State) => ({edit: false})),
    on(editMode, (state: State) => ({edit: true}))
);

export function reducer(state: State | undefined, action: Action) {
    return featureReducer(state, action);
}

