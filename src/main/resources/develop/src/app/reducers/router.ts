import {ActivatedRouteSnapshot, Params, RouterStateSnapshot} from '@angular/router';
import {routerReducer, RouterReducerState, RouterStateSerializer} from '@ngrx/router-store';

export interface RouterStateUrl {
    url: string;
    params: Params;
}

export interface State extends RouterReducerState<RouterStateUrl> {
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl {
        const {url} = routerState;

        let state: ActivatedRouteSnapshot = routerState.root;
        while (state.firstChild) {
            state = state.firstChild;
        }

        const {params} = state;
        return {url, params};
    }
}

export const reducer = routerReducer;

