import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromRouter from './router';
import * as fromCardMode from './card-mode';
import * as fromProfile from './profile';
import * as fromOrder from './order';
import * as fromTitle from './title';
import * as fromButtons from './buttons';

export interface State {
    router: fromRouter.State;
    mode: fromCardMode.State;
    profile: fromProfile.State;
    order: fromOrder.State;
    title: fromTitle.State;
    btns: fromButtons.State;
}

export const reducers: ActionReducerMap<State> = {
    router: fromRouter.reducer,
    mode: fromCardMode.reducer,
    profile: fromProfile.reducer,
    order: fromOrder.reducer,
    title: fromTitle.reducer,
    btns: fromButtons.reducer
};

export const selectRouterState = createSelector(createFeatureSelector<fromRouter.State>('router'), router => router && router.state);
export const selectUrl = createSelector(selectRouterState, routerState => routerState && routerState.url);
export const selectParamId = createSelector(selectRouterState, routerState => routerState && routerState.params['id']);

export const selectEditMode = createSelector((state: State) => state.mode, m => m.edit);

export const profileSelector = (state: State) => state.profile;
export const selectRegistered = createSelector(profileSelector, p => p.registered);
export const selectId = createSelector(profileSelector, p => p.id);

export const orderSelector = (state: State) => state.order;
export const selectOrderStatus = createSelector(orderSelector, p => p.status);
export const selectOrderId = createSelector(orderSelector, p => p.id);
export const selectOrderExecutorId = createSelector(orderSelector, p => p.executorId);

export const titleSelector = (state: State) => state.title;
export const title = createSelector(titleSelector, t => t.text);

export const buttonsSelector = (state: State) => state.btns;
export const buttons = createSelector(buttonsSelector, b => b.btns);
