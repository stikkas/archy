import {Action, createAction, createReducer, on, props} from '@ngrx/store';
import {Button} from '../models/button';

export const buttons = createAction('[Buttons]', props<Button[]>());
export const change = createAction('[Buttons] change', props<{ [key: number]: any }>());
export const changeBulk = createAction('[Buttons] change bulk', props<[number[], { [key: string]: any }]>());

export const initialState = {btns: []};

export interface State {
    btns: Button[];
}

const featureReducer = createReducer(initialState,
    on(buttons, (state: State, data: any) => ({btns: Object.keys(data).filter(k => k !== 'type').map(k => data[k])})),
    on(change, (state: State, data: { [key: number]: any }) => {
            const btns = [];
            state.btns.forEach((b, i) => {
                const nb = data[i];
                btns.push(nb ? {...b, ...nb} : b);
            });
            return {btns};
        }
    ),
    on(changeBulk, (state: State, data) => {
        const btns = [];
        const ids = data[0];
        const nb = data[1];
        state.btns.forEach((b, i) => {
            btns.push(ids.indexOf(i) > -1 ? {...b, ...nb} : b);

        });
        return {btns};
    })
);

export function reducer(state: State | undefined, action: Action) {
    return featureReducer(state, action);
}

