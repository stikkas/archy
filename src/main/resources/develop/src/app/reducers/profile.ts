import {Action, createAction, createReducer, on, props} from '@ngrx/store';

export interface ProfilePayload {
    registered?: boolean;
    id?: number;
}

export const profile = createAction('[Profile]', props<{ payload: ProfilePayload }>());

export const initialState = {registered: false, id: null};

export interface State {
    registered: boolean;
    id: number;
}

const featureReducer = createReducer(initialState,
    on(profile, (state: State, data: { payload: ProfilePayload }) => ({...state, ...data.payload}))
);

export function reducer(state: State | undefined, action: Action) {
    return featureReducer(state, action);
}

