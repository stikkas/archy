import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './pages/header/header.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MainComponent} from './pages/main/main.component';
import {FooterComponent} from './pages/footer/footer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UsersComponent} from './pages/users/users.component';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {CustomRouterStateSerializer} from './reducers/router';
import {StoreModule} from '@ngrx/store';
import {reducers} from './reducers/index';
import {ComponentsModule} from './modules/components/components.module';
import {DateTimePipe} from './pipes/date-time.pipe';
import {ToastrModule} from 'ngx-toastr';
import {VisitorsComponent} from './pages/visitors/visitors.component';
import {VisitorComponent} from './pages/visitor/visitor.component';
import {DateOptions} from './injectors/date-optinos';
import {LoginComponent} from './pages/login/login.component';
import {NgbDropdownModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxMaskModule} from 'ngx-mask';
import {ReportsComponent} from './pages/reports/reports.component';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        HeaderComponent,
        UsersComponent,
        VisitorsComponent,
        FooterComponent,
        DateTimePipe,
        VisitorComponent,
        ReportsComponent,
        LoginComponent
    ],
    imports: [
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        NgbDropdownModule,
        NgbModalModule,
        BrowserAnimationsModule,
        StoreModule.forRoot(reducers, {
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true
            }
        }),
        StoreRouterConnectingModule.forRoot({
            serializer: CustomRouterStateSerializer
        }),
        ComponentsModule,
        NgxMaskModule.forRoot(),
        ToastrModule.forRoot()
    ],
    providers: [
        {
            provide: DateOptions,
            useValue: {
                appendSelectorToBody: true
            }
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

