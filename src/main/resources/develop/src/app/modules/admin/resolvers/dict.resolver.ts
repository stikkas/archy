import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {DictName} from '../../../models/dict-name';
import {DictsService} from '../services/dicts.service';

@Injectable()
export class DictResolver implements Resolve<DictName> {
    constructor(private ds: DictsService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): DictName {
        const dict = route.params.dict;
        this.ds.filter.query = '';
        this.ds.load(dict);
        return dict;
    }
}

