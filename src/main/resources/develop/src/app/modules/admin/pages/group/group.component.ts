import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DictName} from '../../../../models/dict-name';
import {Dict} from '../../../../models/dict';
import {DictService} from '../../../../services/dict.service';
import {Group} from '../../models/group';
import {GroupService} from '../../services/group.service';
import {accessRole} from '../../validators/access-role';

@Component({
    templateUrl: './group.component.html',
    encapsulation: ViewEncapsulation.None
})
export class GroupComponent implements OnInit {
    group: Group;
    groupFG: FormGroup;
    groupsFocused = false;

    accessCheck: (control: AbstractControl) => void;

    dn = DictName;
    dicts = {
        [DictName.AccessRole]: null as Dict[]
    };

    constructor(private route: ActivatedRoute,
                private ds: DictService,
                private gs: GroupService,
                private fb: FormBuilder) {
    }

    ngOnInit() {
        this.route.data
            .subscribe((data: { group: Group }) => {
                const group = this.group = data.group;
                this.groupFG = this.fb.group({
                    name: [group.name, Validators.required],
                    description: [group.description],
                    roles: [group.roles, Validators.required]
                });
                this.accessCheck = accessRole('roles', this.gs, group.id);
            });
        this.ds.getDicts(this.dicts);
    }

    save() {
        this.gs.save({...this.groupFG.value, id: this.group.id}).subscribe(res => {
            if (res) {
                this.group.name = this.groupFG.controls.name.value;
            }
        });
    }
}


