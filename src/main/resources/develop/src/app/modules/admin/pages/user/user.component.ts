import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../../../models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {DictName} from '../../../../models/dict-name';
import {Dict} from '../../../../models/dict';
import {DictService} from '../../../../services/dict.service';
import {matchPasswords} from '../../validators/match-passwords';
import {accessDependant} from '../../validators/access-dependant';
import {GroupService} from '../../services/group.service';
import {accessGroup} from '../../validators/access-group';
import {Role} from '../../../../models/role';

@Component({
    templateUrl: './user.component.html',
    encapsulation: ViewEncapsulation.None
})
export class UserComponent implements OnInit {

    private checkGroup = accessGroup('groups', this.gs);
    arReq = accessDependant('groups', 'archiveId', Role.ChiefArchive, this.gs);
    stReq = accessDependant('groups', 'storageId', Role.Executor, this.gs);

    userFG: FormGroup;
    passFG: FormGroup;
    user: User;
    groupsFocused = false;
    archiveFocused = false;
    storageFocused = false;

    dn = DictName;
    dicts = {
        [DictName.AccessGroup]: null as Dict[],
        [DictName.Archive]: null as Dict[],
        [DictName.Storage]: null as Dict[]
    };
    groupTouched = false;

    constructor(private route: ActivatedRoute,
                private ds: DictService,
                private gs: GroupService,
                private us: UserService,
                private fb: FormBuilder) {
    }

    ngOnInit() {
        this.route.data
            .subscribe((data: { user: User }) => {
                const user = this.user = data.user;
                this.userFG = this.fb.group({
                    firstName: [user.firstName, Validators.required],
                    lastName: [user.lastName, Validators.required],
                    middleName: [user.middleName, Validators.required],
                    groups: [user.groups, Validators.required],
                    archiveId: user.archiveId,
                    storageId: user.storageId
                });
            });
        this.passFG = this.fb.group({
            password: '',
            password2: ''
        }, {validator: matchPasswords('password', 'password2')});
        this.ds.getDicts(this.dicts);
    }

    save() {
        this.us.save({...this.userFG.value, id: this.user.id}).subscribe();
    }

    changePassword() {
        this.us.changePassword(this.user.id, this.passFG.value.password);
    }

    groupChanged() {
        this.arReq(this.userFG);
        this.stReq(this.userFG);
        this.checkGroup(this.userFG);
        this.groupTouched = true;
    }
}


