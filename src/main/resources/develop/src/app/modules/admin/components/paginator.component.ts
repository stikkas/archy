import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {Page} from '../../../models/page';

@Component({
    selector: 'aisrr-pgntr',
    template: `
        <div class="row justify-content-end" *ngIf="page?.pages > 1">
            <div class="col flex-grow-0">
                <ngb-pagination [page]="page.page"
                                [maxSize]="3"
                                [pageSize]="page.size"
                                [collectionSize]="page.count"
                                (pageChange)="changed.emit($event)">
                </ngb-pagination>
            </div>
        </div>
    `,
    encapsulation: ViewEncapsulation.None
})
export class PaginatorComponent {
    @Input() page: Page<any>;
    @Output() changed = new EventEmitter<number>();
}
