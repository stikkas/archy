import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../../../models/user';

@Injectable()
export class UserResolver implements Resolve<User> {
    constructor(private us: UserService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
        return this.us.getEntity(route.params.id);
    }
}

