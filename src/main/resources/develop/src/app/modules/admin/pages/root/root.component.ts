import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../../../services/auth.service';
import {Router} from '@angular/router';
import {DictName} from '../../../../models/dict-name';

@Component({
    templateUrl: './root.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RootComponent implements OnInit {
    user$ = this.auth.getUser();
    collapsed = true;
    dn = DictName;
    w$ = window;

    constructor(private auth: AuthService, private router: Router) {
    }


    ngOnInit() {
        if (window.location.pathname === '/admin') {
            this.router.navigate(['/admin/users']);
        }
    }
}


