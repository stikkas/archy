import {FormGroup} from '@angular/forms';
import {GroupService} from '../services/group.service';
import {Role} from '../../../models/role';

/**
 * Вызываем проверку на change поля.
 * Проверяем на наличие только одной группы с правами читального зала.
 * @param groupsKey - название поля групп
 * @param gs - сервис  для проверки
 */
export function accessGroup(groupsKey: string, gs: GroupService) {
    return (group: FormGroup) => {
        const groups = group.controls[groupsKey];
        if (groups.value.length > 1) {
            gs.hasAnyGroup(groups.value, [Role.Rr1, Role.Rr2, Role.Rr3]).subscribe(res => {
                groups.setErrors(res ? {[groupsKey]: 'many'} : null);
            });
        }
    };
}

