export class Holiday {
    id: number;
    year?: number;
    month?: number | string;
    days: number[] | string;
    rr: string[];
    rrStr?: string;
}

