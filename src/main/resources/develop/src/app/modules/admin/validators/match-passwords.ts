import {FormGroup} from '@angular/forms';

export function matchPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup) => {
        const password = group.controls[passwordKey];
        const confirmPassword = group.controls[confirmPasswordKey];
        if (password.value !== confirmPassword.value) {
            confirmPassword.setErrors({mustMatch: true});
        } else {
            confirmPassword.setErrors(confirmPassword.value ? null : {rePassword: ''});
        }
        password.setErrors(password.value ? null : {password: ''});
    };
}

