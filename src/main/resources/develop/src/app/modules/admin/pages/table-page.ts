import {QueryList, ViewChildren} from '@angular/core';
import {SortableHeaderDirective} from '../directives/sortable-header.directive';
import {EntityService} from '../services/entity-service';
import {SortEvent} from '../../../models/sort-event';

export class TablePage<T> {

    @ViewChildren(SortableHeaderDirective) headers: QueryList<SortableHeaderDirective>;

    constructor(protected es: EntityService<T>) {
    }

    sort(event: SortEvent) {
        this.headers.forEach(header => {
            if (header.sortable !== event.column) {
                header.direction = '';
            }
        });
        this.load(this.es.curPage, event);
    }

    load(page?: number, sort?: SortEvent) {
        if (!page) {
            this.headers.forEach(header => header.direction = '');
        }
        this.es.loadPage(page, sort);
    }
}
