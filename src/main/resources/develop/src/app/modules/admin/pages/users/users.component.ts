import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserService} from '../../services/user.service';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {DictService} from '../../../../services/dict.service';
import {DictName} from '../../../../models/dict-name';
import {Dict} from '../../../../models/dict';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../../models/user';
import {TablePage} from '../table-page';
import {GroupService} from '../../services/group.service';
import {matchPasswords} from '../../validators/match-passwords';
import {accessDependant} from '../../validators/access-dependant';
import {map, switchMap} from 'rxjs/operators';
import {timer} from 'rxjs';
import {accessGroup} from '../../validators/access-group';
import {Role} from '../../../../models/role';

@Component({
    templateUrl: './users.component.html',
    encapsulation: ViewEncapsulation.None
})
export class UsersComponent extends TablePage<User> implements OnInit {

    private modalOpts: NgbModalOptions = {backdrop: 'static', keyboard: false};
    private checkGroup = accessGroup('groups', this.gs);
    arReq = accessDependant('groups', 'archiveId', Role.ChiefArchive, this.gs);
    stReq = accessDependant('groups', 'storageId', Role.Executor, this.gs);

    users$ = this.us.getPage();
    filter = this.us.filter;
    groupsFocused = false;
    archiveFocused = false;
    storageFocused = false;
    user: User;
    newPass: FormGroup;
    newUser: FormGroup;

    dn = DictName;
    dicts = {
        [DictName.AccessGroup]: null as Dict[],
        [DictName.Storage]: null as Dict[],
        [DictName.Archive]: null as Dict[]
    };

    constructor(private us: UserService, private gs: GroupService,
                private ds: DictService,
                private ms: NgbModal,
                private fb: FormBuilder) {
        super(us);

        this.newUser = fb.group({
            login: ['', {validators: Validators.required, asyncValidators: this.loginValidator()}],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            middleName: ['', Validators.required],
            groups: [[], Validators.required],
            archiveId: undefined,
            storageId: undefined,
            ps: fb.group({
                password: '',
                rePassword: '',
            }, {
                validators: matchPasswords('password', 'rePassword')
            }),
        });

    }

    ngOnInit() {
        this.ds.getDicts(this.dicts);
    }

    add() {
        const val = this.newUser.value;
        this.us.save({...val, ps: undefined, ...val.ps}).subscribe(s => {
            if (s) {
                this.newUser.reset();
            }
        });
    }

    openRemove(content: any, user: User) {
        this.user = user;
        this.ms.open(content, this.modalOpts).result.then(result => {
            if (result === 'yes') {
                this.us.remove(user.id).subscribe();
            }
        });
    }

    openChangePassword(content: any, user: User) {
        this.user = user;
        this.newPass = this.fb.group({
            passwd: '',
            passwd2: ''
        }, {validator: matchPasswords('passwd', 'passwd2')});
        this.ms.open(content, this.modalOpts).result.then(result => {
            if (result === 'yes') {
                this.us.changePassword(user.id, this.newPass.value.passwd);
                this.newPass = null;
            }
        });
    }

    getClass(fg: any, control: string) {
        const fc = fg.controls[control];

        return fc.touched || (control === 'rePassword' && fg.controls.password.touched)
        || ((control === 'archiveId' || control === 'storageId') && fg.controls.groups.touched) ? fc.invalid ? 'invalid' : 'valid' : '';
    }

    private loginValidator(): ValidatorFn {
        return (control: AbstractControl) => {
            return timer(300).pipe(switchMap(() => this.us.loginExists(control.value)),
                map(res => res ? {loginExists: true} : null));
        };
    }

    getLoading() {
        return this.us.loading;
    }

    groupChanged() {
        this.arReq(this.newUser);
        this.stReq(this.newUser);
        this.checkGroup(this.newUser);
    }
}


