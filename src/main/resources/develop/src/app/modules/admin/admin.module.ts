import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {UsersComponent} from './pages/users/users.component';
import {RootComponent} from './pages/root/root.component';
import {GroupsComponent} from './pages/groups/groups.component';
import {UserService} from './services/user.service';
import {NgbDropdownModule, NgbModalModule, NgbPaginationModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {UserComponent} from './pages/user/user.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SortableHeaderDirective} from './directives/sortable-header.directive';
import {UserResolver} from './resolvers/user.resolver';
import {GroupService} from './services/group.service';
import {GroupComponent} from './pages/group/group.component';
import {GroupResolver} from './resolvers/group.resolver';
import {HolidaysComponent} from './pages/holidays/holidays.component';
import {HolidayService} from './services/holiday.service';
import {NgxMaskModule} from 'ngx-mask';
import {PaginatorComponent} from './components/paginator.component';
import {DictsComponent} from './pages/dicts/dicts.component';
import {DictResolver} from './resolvers/dict.resolver';
import {DictsService} from './services/dicts.service';

@NgModule({
    declarations: [UsersComponent, RootComponent, GroupsComponent,
        UserComponent, SortableHeaderDirective, GroupComponent, HolidaysComponent,
        PaginatorComponent, DictsComponent],
    imports: [
        CommonModule,
        AdminRoutingModule,
        NgSelectModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        FormsModule,
        NgbPaginationModule,
        NgbModalModule,
        NgbTooltipModule,
        NgxMaskModule.forChild()
    ],
    providers: [
        UserService,
        GroupService,
        HolidayService,
        DictsService,
        UserResolver,
        GroupResolver,
        DictResolver
    ]
})
export class AdminModule {
}
