import {BehaviorSubject, Observable, of} from 'rxjs';
import {Page} from '../../../models/page';
import {AdminFilter} from '../models/admin.filter';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {ActionStatus} from '../../../models/action.status';
import {ToastrService} from 'ngx-toastr';
import {SortEvent} from '../../../models/sort-event';

export abstract class EntityService<T> {
    private page = 1;
    private sort: SortEvent;
    private myloading = false;
    page$ = new BehaviorSubject<Page<T>>(null);

    get loading() {
        return this.myloading;
    }

    protected constructor(protected http: HttpClient,
                          protected tstr: ToastrService,
                          protected baseUrl: string,
                          public filter = new AdminFilter(),
                          private pageSize = 5) {
    }

    getPage() {
        return this.page$.asObservable();
    }

    loadPage(page?: number, sort?: SortEvent) {
        const params = {page: `${page || 1}`, size: `${this.pageSize}`} as any;
        // tslint:disable-next-line:forin
        for (const k in this.filter) {
            const v = this.filter[k];
            if (v) {
                params[k] = v;
            }
        }
        const ssort = this.sort = sort || (page ? this.sort : undefined);

        if (ssort && ssort.direction) {
            params.sortField = ssort.column;
            params.sortOrder = ssort.direction;
        }
        this.myloading = true;
        this.http.get(this.baseUrl, {params}).pipe(map((st: ActionStatus) => {
                if (st.status) {
                    this.page$.next(st.data);
                    this.page = st.data.page;
                    if (st.data.data.length === 0) {
                        this.tstr.success('Ничего не найдено');
                    }
                } else {
                    this.errorHandler(st.data);
                }
                this.myloading = false;
            }),
            catchError(err => this.errorHandler(err))
        ).subscribe();
    }

    save(entity: T) {
        this.myloading = true;
        return this.http.post(this.baseUrl, entity).pipe(map((res: ActionStatus) => {
                this.myloading = false;
                if (res.status) {
                    if (this.page$.getValue()) {
                        this.loadPage(this.page);
                    }
                    this.tstr.success('Данные сохранены');
                    return res.status;
                } else {
                    this.tstr.error(res.data);
                    return false;
                }
            }),
            catchError(err => this.errorHandler(err)));
    }

    remove(id: number) {
        return this.http.delete(`${this.baseUrl}/${id}`)
            .pipe(map((res: ActionStatus) => {
                    if (res.status) {
                        this.loadPage(this.page);
                        this.tstr.success('Данные удалены');
                        return res.status;
                    } else {
                        this.tstr.error(res.data);
                        return false;
                    }
                }),
                catchError(err => this.errorHandler(err)));
    }


    getEntity(id: number): Observable<T> {
        return this.http.get<T>(`${this.baseUrl}/${id}`);
    }

    get curPage() {
        return this.page;
    }

    private errorHandler(err: any) {
        this.myloading = false;
        this.tstr.error(err.error ? err.error.message : err);
        return of(null);
    }
}

