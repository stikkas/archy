import {FormGroup} from '@angular/forms';
import {GroupService} from '../services/group.service';
import {Role} from '../../../models/role';

/**
 * Вызываем проверку на change каждого поля.
 * Зависимое поле от прав доступа.
 * Стандартные методы привязки валидатора для поля или группы не работают - вызываются не всегда.
 * @param groupsKey - название поля групп
 * @param depKey - название зависимого
 * @param role - роль, при которой поле обязательно
 * @param gs - сервис  для проверки
 */
export function accessDependant(groupsKey: string, depKey: string, role: Role, gs: GroupService) {
    return (group: FormGroup) => {
        const groups = group.controls[groupsKey];
        const dep = group.controls[depKey];
        if (groups.value.length) {
            gs.hasRole(groups.value, role).subscribe(res => {
                dep.setErrors((res && !dep.value) ? {[depKey]: 'required'} : null);
            });
        } else {
            dep.setErrors(null);
        }
    };
}

