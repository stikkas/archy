import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EntityService} from './entity-service';
import {ToastrService} from 'ngx-toastr';
import {Holiday} from '../models/holiday';

@Injectable()
export class HolidayService extends EntityService<Holiday> {
    constructor(http: HttpClient, tstr: ToastrService) {
        super(http, tstr, '/api/admin/holidays');
    }
}

