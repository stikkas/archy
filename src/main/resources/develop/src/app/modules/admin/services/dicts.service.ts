import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DictName} from '../../../models/dict-name';
import {EntityService} from './entity-service';
import {ToastrService} from 'ngx-toastr';
import {SortEvent} from '../../../models/sort-event';
import {DictEntity} from '../models/dict-entity';
import {AdminFilter} from '../models/admin.filter';

@Injectable()
export class DictsService extends EntityService<DictEntity> {
    private url = '/api/admin/dicts/';

    constructor(http: HttpClient, tstr: ToastrService) {
        super(http, tstr, '', new AdminFilter(), 10);
    }

    load(dict: DictName, page?: number, sort?: SortEvent) {
        this.baseUrl = this.url + dict;
        this.loadPage(page, sort);
    }
}
