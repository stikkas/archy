import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DictName} from '../../../../models/dict-name';
import {DictsService} from '../../services/dicts.service';
import {ActivatedRoute} from '@angular/router';
import {DictEntity} from '../../models/dict-entity';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Dict} from '../../../../models/dict';
import {DictService} from '../../../../services/dict.service';
import {TablePage} from '../table-page';

@Component({
    templateUrl: './dicts.component.html',
    encapsulation: ViewEncapsulation.None
})
export class DictsComponent extends TablePage<DictEntity> implements OnInit {
    private modalOpts: NgbModalOptions = {backdrop: 'static', keyboard: false};

    page$ = this.es.getPage();
    title: string;
    rr: boolean;
    short: boolean;
    storage: boolean;
    filter = this.es.filter;
    currentId: number;
    dict: DictEntity;
    created = [] as DictEntity[];
    dn = DictName;
    dicts = {
        [DictName.ArchiveShort]: null as Dict[],
        [DictName.ReadingRoom]: null as Dict[]
    };

    constructor(ds: DictsService,
                private dictService: DictService,
                private ms: NgbModal,
                private route: ActivatedRoute) {
        super(ds);
    }

    ngOnInit(): void {
        this.dictService.getDicts(this.dicts);
        this.route.data
            .subscribe((data: { dict: DictName }) => {
                this.short = this.rr = this.storage = false;
                switch (data.dict) {
                    case DictName.Archive:
                        this.title = 'Архив';
                        this.short = true;
                        break;
                    case DictName.Country:
                        this.title = 'Гражданство';
                        break;
                    case DictName.RejectReason:
                        this.title = 'Причина отказа';
                        break;
                    case DictName.ScienceRank:
                        this.title = 'Научное звание';
                        break;
                    case DictName.Education:
                        this.title = 'Образование';
                        break;
                    case DictName.TopicHeading:
                        this.title = 'Рубрикатор тем';
                        break;
                    case DictName.RrDetails:
                        this.title = 'Читальный зал';
                        this.rr = true;
                        break;
                    case DictName.Storage:
                        this.title = 'Архивохранилище';
                        this.storage = true;
                        break;
                }
            });
    }


    save(dict: DictEntity) {
        this.es.save(dict).subscribe(res => {
            if (res) {
                this.cancel();
                this.load(this.es.curPage);
            }
        });
    }

    cancel() {
        this.currentId = undefined;
        this.created.length = 0;
    }

    edit(d: DictEntity) {
        this.currentId = d.id;
    }

    openRemove(content: any, dict: DictEntity) {
        this.dict = dict;
        this.ms.open(content, this.modalOpts).result.then(result => {
            if (result === 'yes') {
                this.es.remove(dict.id).subscribe();
            }
        });
    }

    getLoading() {
        return this.es.loading;
    }

    add() {
        this.created.push({} as DictEntity);
    }

    disableRemove(dict: any) {
        return this.rr && ['RR1', 'RR2', 'RR3'].indexOf(dict.rr) > -1;
    }
}



