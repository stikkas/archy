import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Group} from '../models/group';
import {EntityService} from './entity-service';
import {ToastrService} from 'ngx-toastr';
import {Role} from '../../../models/role';
import {Observable} from 'rxjs';

@Injectable()
export class GroupService extends EntityService<Group> {
    constructor(http: HttpClient, tstr: ToastrService) {
        super(http, tstr, '/api/admin/groups');
    }

    /**
     * Проверяет есть ли роль среди ролей групп
     * @param groups - список идентификаторов, интересующих групп
     * @param role - интересующая роль
     */
    hasRole(groups: number[], role: Role): Observable<boolean> {
        return this.http.get<boolean>(`${this.baseUrl}/has-role/${role}`, {params: {groups: groups.map(it => `${it}`)}});
    }

    /**
     * Проверяет есть ли уже группа с такой ролью
     * @param role - интересующая роль
     * @param id - идентификатор группы, когда проверяем в режиме редоктирования существующей
     */
    hasGroup(role: Role, id?: number): Observable<boolean> {
        const params = {} as any;
        if (id) {
            params.id = id;
        }
        return this.http.get<boolean>(`${this.baseUrl}/has-group/${role}`, {params});
    }

    /**
     * Проверяет есть ли группа с одной из ролей
     * @param groups - список идентификаторов, интересующих групп
     * @param roles - интересующие роли
     */
    hasAnyGroup(groups: number[], roles: Role[]): Observable<boolean> {
        return this.http.get<boolean>(`${this.baseUrl}/has-any-group`, {params: {groups: groups.map(it => `${it}`), roles}});
    }
}



