import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './pages/users/users.component';
import {RootComponent} from './pages/root/root.component';
import {GroupsComponent} from './pages/groups/groups.component';
import {UserComponent} from './pages/user/user.component';
import {UserResolver} from './resolvers/user.resolver';
import {GroupComponent} from './pages/group/group.component';
import {GroupResolver} from './resolvers/group.resolver';
import {HolidaysComponent} from './pages/holidays/holidays.component';
import {DictsComponent} from './pages/dicts/dicts.component';
import {DictResolver} from './resolvers/dict.resolver';

const routes: Routes = [{
    path: '',
    component: RootComponent,
    children: [{
        path: 'users',
        component: UsersComponent
    }, {
        path: 'groups',
        component: GroupsComponent
    }, {
        path: 'users/:id',
        component: UserComponent,
        resolve: {user: UserResolver}
    }, {
        path: 'groups/:id',
        component: GroupComponent,
        resolve: {group: GroupResolver}
    }, {
        path: 'holidays',
        component: HolidaysComponent
    }, {
        path: 'dicts/:dict',
        component: DictsComponent,
        resolve: {dict: DictResolver}
    }]
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
