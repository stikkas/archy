export class Group {
    id: number;
    name: string;
    description: string;
    roles: string[];
}

