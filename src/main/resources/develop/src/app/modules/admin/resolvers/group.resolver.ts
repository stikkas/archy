import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {GroupService} from '../services/group.service';
import {Group} from '../models/group';

@Injectable()
export class GroupResolver implements Resolve<Group> {
    constructor(private gs: GroupService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Group> {
        return this.gs.getEntity(route.params.id);
    }
}

