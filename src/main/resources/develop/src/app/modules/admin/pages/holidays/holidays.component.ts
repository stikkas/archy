import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DictName} from '../../../../models/dict-name';
import {Dict} from '../../../../models/dict';
import {DictService} from '../../../../services/dict.service';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {TablePage} from '../table-page';
import {Holiday} from '../../models/holiday';
import {HolidayService} from '../../services/holiday.service';

@Component({
    templateUrl: './holidays.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HolidaysComponent extends TablePage<Holiday> implements OnInit {

    private modalOpts: NgbModalOptions = {backdrop: 'static', keyboard: false};
    private allowedKeys = ['arrowleft', 'arrowright', 'backspace', 'delete', 'enter'];

    holidays$ = this.hs.getPage();
    filter = this.hs.filter;
    record: FormGroup;

    dn = DictName;
    dicts = {
        [DictName.Month]: null as Dict[],
        [DictName.ReadingRoom]: null as Dict[]
    };
    monthFocused = false;
    rrFocused = false;
    currentId: number;
    holiday: Holiday;

    @ViewChild('din', {static: false})
    din: ElementRef;
    disabledSave = false;

    constructor(private hs: HolidayService, private ds: DictService,
                private ms: NgbModal, fb: FormBuilder) {
        super(hs);
        const now = new Date();
        this.record = fb.group({
            year: [now.getFullYear(), Validators.required],
            month: [now.getMonth(), Validators.required],
            rr: '',
            days: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.ds.getDicts(this.dicts);
    }

    add() {
        this.hs.save(this.record.value).subscribe(s => {
            if (s) {
                this.record.reset();
            }
        });
    }

    openRemove(content: any, holiday: Holiday) {
        this.holiday = holiday;
        this.ms.open(content, this.modalOpts).result.then(result => {
            if (result === 'yes') {
                this.hs.remove(holiday.id).subscribe();
            }
        });
    }

    getClass(name: string) {
        const fc = this.record.controls[name];
        return fc.touched ? fc.invalid ? 'invalid' : 'valid' : '';
    }

    getLoading() {
        return this.hs.loading;
    }

    days(days: number[]): string {
        return days ? days.sort((a, b) => a - b).join(', ') : '';
    }

    save(hd: Holiday) {
        const days = this.din.nativeElement.value;
        if (days) {
            this.cancel();
            this.hs.save({id: hd.id, rr: hd.rr, days}).subscribe();
        }
    }

    cancel() {
        this.currentId = undefined;
        this.disabledSave = false;
    }

    checkDay(key: KeyboardEvent) {
        if (key.ctrlKey || key.metaKey || this.allowedKeys.indexOf(key.code.toLowerCase()) > -1
            || /[\d,;\s]/.test(key.key)) {
            return key;
        }
        return false;
    }

    edit(h: Holiday) {
        this.currentId = h.id;
    }

    disableSave() {
        this.disabledSave = !(this.din && this.din.nativeElement.value);
    }
}


