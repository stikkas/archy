import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DictName} from '../../../../models/dict-name';
import {Dict} from '../../../../models/dict';
import {GroupService} from '../../services/group.service';
import {Group} from '../../models/group';
import {DictService} from '../../../../services/dict.service';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {TablePage} from '../table-page';
import {accessRole} from '../../validators/access-role';

@Component({
    templateUrl: './groups.component.html',
    encapsulation: ViewEncapsulation.None
})
export class GroupsComponent extends TablePage<Group> implements OnInit {
    groups$ = this.gs.getPage();
    filter = this.gs.filter;
    newGroup: FormGroup;

    groupsFocused = false;
    group: Group;
    accessCheck = accessRole('roles', this.gs);
    private modalOpts: NgbModalOptions = {backdrop: 'static', keyboard: false};

    dn = DictName;
    dicts = {
        [DictName.AccessRole]: null as Dict[]
    };

    constructor(private gs: GroupService, private ds: DictService,
                private ms: NgbModal, fb: FormBuilder) {
        super(gs);
        this.newGroup = fb.group({
            name: ['', Validators.required],
            roles: ['', Validators.required],
            description: ['']
        });
    }

    ngOnInit() {
        this.ds.getDicts(this.dicts);
    }

    add() {
        this.gs.save(this.newGroup.value).subscribe(s => {
            if (s) {
                this.newGroup.reset();
                this.ds.reset(DictName.AccessGroup);
            }
        });
    }

    openRemove(content: any, group: Group) {
        this.group = group;
        this.ms.open(content, this.modalOpts).result.then(result => {
            if (result === 'yes') {
                this.gs.remove(group.id).subscribe(res => {
                    if (res) {
                        this.ds.reset(DictName.AccessGroup);
                    }
                });
            }
        });
    }

    getClass(name: string) {
        const fc = this.newGroup.controls[name];
        return fc.touched ? fc.invalid ? 'invalid' : 'valid' : '';
    }

    getLoading() {
        return this.gs.loading;
    }
}


