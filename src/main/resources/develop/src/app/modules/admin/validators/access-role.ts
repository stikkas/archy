import {FormGroup} from '@angular/forms';
import {Role} from '../../../models/role';
import {GroupService} from '../services/group.service';

export function accessRole(keyRole: string, gs: GroupService, id?: number) {
    return (group: FormGroup) => {
        const roles = group.controls[keyRole];
        const values = roles.value;
        if (values.length) {
            if (Role.hasAny(values, [Role.Rr3, Role.Rr2, Role.Rr1])) {
                if (values.length > 1) {
                    roles.setErrors({[keyRole]: 'only'});
                } else {
                    gs.hasGroup(values[0], id).subscribe(res => {
                        roles.setErrors(res ? {[keyRole]: 'exists'} : null);
                    });
                }
            } else {
                roles.setErrors(null);
            }
        } else {
            roles.setErrors({[keyRole]: 'required'});
        }
    };
}

