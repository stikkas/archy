import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EntityService} from './entity-service';
import {User} from '../../../models/user';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs';

@Injectable()
export class UserService extends EntityService<User> {

    constructor(http: HttpClient, tstr: ToastrService) {
        super(http, tstr, '/api/admin/users');
    }

    changePassword(id: number, password: string) {
        this.http.post(`${this.baseUrl}/${id}`, {password}).subscribe(res => {
            this.tstr.success('Новый пароль сохранен');
        });
    }

    loginExists(login: string): Observable<boolean> {
        return this.http.get<boolean>(`${this.baseUrl}/exists/${login}`);
    }
}


