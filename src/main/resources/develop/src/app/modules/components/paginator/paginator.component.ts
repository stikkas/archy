import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {Page} from '../../../models/page';

@Component({
    selector: 'aisrr-paginator',
    templateUrl: './paginator.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginatorComponent {
    @Input() page: Page<any>;
    @Output() next = new EventEmitter<number>();

    constructor() {
    }

    pages() {
        const pages = this.page.pages;
        const current = this.page.page;
        const res = [];
        const many = pages > 6;
        const max = Math.min(8, pages + 1);
        for (let i = 1; i < max; ++i) {
            if (i === 1) {
                res.push({l: 1, v: 1});
            } else if (i === 2) {
                if (many && current >= 5) {
                    res.push({l: '...', v: false});
                } else {
                    res.push({l: 2, v: 2});
                }
            } else if (i === 3) {
                if (!many || current < 5) {
                    res.push({l: 3, v: 3});
                } else if (current > pages - 4) {
                    res.push({l: pages - 4, v: pages - 4});
                } else {
                    res.push({l: current - 1, v: current - 1});
                }
            } else if (i === 4) {
                if (!many || current < 5) {
                    res.push({l: 4, v: 4});
                } else if (current > pages - 4) {
                    res.push({l: pages - 3, v: pages - 3});
                } else {
                    res.push({l: current, v: current});
                }
            } else if (i === 5) {
                if (!many || current < 5) {
                    res.push({l: 5, v: 5});
                } else if (current > pages - 4) {
                    res.push({l: pages - 2, v: pages - 2});
                } else {
                    res.push({l: current + 1, v: current + 1});
                }
            } else if (i === 6) {
                if (many && current < pages - 3) {
                    res.push({l: '...', v: false});
                } else {
                    res.push({l: pages - 1, v: pages - 1});
                }
            } else if (i === 7) {
                res.push({l: pages, v: pages});
            }
        }
        return res;
    }
}

