import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaginatorComponent} from './paginator.component';
import {Page} from '../../../models/page';
import {Component} from '@angular/core';

describe('PaginatorComponent', () => {
    let testHostCmp: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PaginatorComponent, TestHostComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostCmp = testHostFixture.componentInstance;
    });

    it('should be hidden', () => {
        testHostFixture.detectChanges();
        expect(testHostFixture.nativeElement.querySelector('div')).toBeFalsy();

        testHostCmp.page = new Page();
        testHostFixture.detectChanges();
        expect(testHostFixture.nativeElement.querySelector('div')).toBeFalsy();

        testHostCmp.page = new Page(['hello'], 20, 1);
        testHostFixture.detectChanges();
        expect(testHostFixture.nativeElement.querySelector('div')).toBeFalsy();

        testHostCmp.page = new Page(['hello'], 20, 20);
        testHostFixture.detectChanges();
        expect(testHostFixture.nativeElement.querySelector('div')).toBeFalsy();

        testHostCmp.page = new Page(['hello'], 20, 21);
        testHostFixture.detectChanges();
        expect(testHostFixture.nativeElement.querySelector('div')).toBeTruthy();
    });

    it('should be 2 pages', () => {
        testHostCmp.page = new Page(['hello'], 10, 11);
        testHostFixture.detectChanges();
        firstDisabledEndEnabled(2);

        testHostCmp.page = new Page(['hello'], 10, 60);
        testHostFixture.detectChanges();
        firstDisabledEndEnabled(6);

    });

    const firstDisabledEndEnabled = (expected: number) => {
        const btns = testHostFixture.nativeElement.querySelectorAll('button.page-btn');
        expect(btns.length).toEqual(expected);
        expect(testHostFixture.nativeElement.querySelector('.tostart-btn')
            .getAttribute('disabled')).toEqual('');
        expect(testHostFixture.nativeElement.querySelector('.back-btn')
            .getAttribute('disabled')).toEqual('');
        expect(testHostFixture.nativeElement.querySelector('.toend-btn')
            .getAttribute('disabled')).toBeFalsy();
        expect(testHostFixture.nativeElement.querySelector('.forward-btn')
            .getAttribute('disabled')).toBeFalsy();
    };

    @Component({
        selector: 'aisrr-host-component',
        template: '<aisrr-paginator [page]="page" (next)="load($event)"></aisrr-paginator>'
    })
    class TestHostComponent {
        page: Page<any>;

        load(page: number) {

        }
    }
});
