import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaginatorComponent} from './paginator/paginator.component';
import {AngularMyDatePickerModule} from 'angular-mydatepicker';
import {SortableDirective} from './directives/sortable.directive';
import {SectionsComponent} from './sections/sections.component';
import {RouterModule} from '@angular/router';
import {TrainComponent} from './train/train.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {OrderComponent} from './order/order.component';

@NgModule({
    declarations: [PaginatorComponent, SortableDirective, SectionsComponent, TrainComponent, OrderComponent],
    imports: [
        CommonModule,
        AngularMyDatePickerModule,
        RouterModule,
        NgSelectModule
    ],
    exports: [
        PaginatorComponent,
        AngularMyDatePickerModule,
        SortableDirective,
        SectionsComponent,
        TrainComponent,
        OrderComponent,
        NgSelectModule
    ]
})
export class ComponentsModule {
}

