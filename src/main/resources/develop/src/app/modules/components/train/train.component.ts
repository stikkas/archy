import {Component, Input, ViewEncapsulation} from '@angular/core';
import {OrderState} from '../../../models/order-state';
import {DictService} from '../../../services/dict.service';
import {DictName} from '../../../models/dict-name';
import {OrderType} from '../../orders/models/order-type';

@Component({
    selector: 'aisrr-train',
    templateUrl: './train.component.html',
    encapsulation: ViewEncapsulation.None
})
export class TrainComponent {
    statuses = [];

    ot = OrderType;

    @Input()
    type: OrderType;

    constructor(private ds: DictService) {
    }

    @Input()
    set status(value: OrderState) {
        const sts = [];
        if (value) {
            if (!(value == OrderState.Pre || value == OrderState.Accept || value == OrderState.Cancel1)) {
                sts.push(OrderState.Accept);
                if (!(value == OrderState.Reject || value == OrderState.Onexec || value == OrderState.Cancel2)) {
                    sts.push(OrderState.Onexec);
                    if (!(value == OrderState.Ready || value == OrderState.Cancel3)) {
                        sts.push(OrderState.Ready);
                        if (value !== OrderState.Exec) {
                            sts.push(OrderState.Exec);
                            if (value !== OrderState.Dump) {
                                sts.push(OrderState.Dump);
                            }
                        }
                    }
                }
            }
            sts.push(value);
        }
        this.statuses = sts;
    }

    getLabel(state: OrderState) {
        return this.ds.getLabel(state, DictName.OrderState);
    }
}
