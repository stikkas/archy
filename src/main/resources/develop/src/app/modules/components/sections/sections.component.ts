import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'aisrr-sections',
    templateUrl: './sections.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SectionsComponent {
    @Input() id: number;
    @Input() sections: [{ label: string, url: string, disabled?: Observable<boolean> }];
}

