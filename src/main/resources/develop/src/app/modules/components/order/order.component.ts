import {Component, Input, ViewEncapsulation} from '@angular/core';
import {AbstractOrder} from '../../orders/models/abstract-order';

@Component({
    selector: 'aisrr-order',
    templateUrl: './order.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OrderComponent {
    @Input()
    order: AbstractOrder;

    // расширенная версия, используется на вкладках "Начальник отдела хранения" и "Хранитель" и "Читальный зал" когда уже в работе
    @Input()
    expand: boolean;

    // Указывает на то что показываем на вкладке "Читальный зал"
    @Input()
    rr: boolean;

    @Input()
    topic: string;

    @Input()
    archive: string;
}
