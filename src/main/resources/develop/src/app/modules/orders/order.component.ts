import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as fromRoot from '../../reducers';
import {Store} from '@ngrx/store';
import {OrderState} from '../../models/order-state';
import {map} from 'rxjs/operators';

@Component({
    template: `
        <div class="row">
            <div class="content-grid__sidebar">
                <aisrr-sections [id]="id$|async" [sections]="sections"></aisrr-sections>
            </div>
            <div class="content-grid__content">
                <router-outlet></router-outlet>
            </div>
        </div>
    `,
    encapsulation: ViewEncapsulation.None
})
export class OrderComponent implements OnInit {
    id$ = this.store.select(fromRoot.selectOrderId);
    status$ = this.store.select(fromRoot.selectOrderStatus);
    sections = [{
        label: 'Читальный зал',
        url: ''
    }, {
        label: 'Начальник отдела хранения',
        url: 'trans',
        disabled: this.status$.pipe(map(s => s == OrderState.Pre))
    }, {
        label: 'Хранитель',
        url: 'exec',
        disabled: this.status$.pipe(map(s => [OrderState.Pre, OrderState.Accept, OrderState.Reject].indexOf(s) > -1))
    }];

    constructor(private store: Store<fromRoot.State>) {
    }

    ngOnInit() {
    }

}
