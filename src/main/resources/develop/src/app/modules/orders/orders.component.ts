import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import {title} from '../../reducers/title';
import {DictName} from '../../models/dict-name';
import {Dict} from '../../models/dict';
import {DictService} from '../../services/dict.service';

@Component({
    template: `
        <router-outlet></router-outlet>
    `,
    encapsulation: ViewEncapsulation.None
})
export class OrdersComponent implements OnInit {
    dicts = {
        [DictName.OrderState]: null as Dict[]
    };

    constructor(store: Store<fromRoot.State>, dictService: DictService) {
        store.dispatch(title({text: 'Заказы'}));
        dictService.getDicts(this.dicts);
    }

    ngOnInit() {

    }

}
