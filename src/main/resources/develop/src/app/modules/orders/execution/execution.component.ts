import {Component, Inject, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {DictName} from '../../../models/dict-name';
import {DictService} from '../../../services/dict.service';
import {DateOptions} from '../../../injectors/date-optinos';
import * as fromButtons from '../../../reducers/buttons';
import * as fromCardMode from '../../../reducers/card-mode';
import {ActivatedRoute, Router} from '@angular/router';
import {take, takeUntil} from 'rxjs/operators';
import {combineLatest} from 'rxjs';
import {Dict} from '../../../models/dict';
import {Role} from '../../../models/role';
import {AuthService} from '../../../services/auth.service';
import {OrderState} from '../../../models/order-state';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {AngularMyDatePickerDirective} from 'angular-mydatepicker';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../services/order.service';
import {OrdersService} from '../services/orders.service';
import {OrderExecution} from '../models/order-execution';
import {CheckedData} from '../models/checked-data';

@Component({
    templateUrl: './execution.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ExecutionComponent implements OnInit, OnDestroy {
    editMode$ = this.store.select(fromRoot.selectEditMode);
    user$ = this.auth.getUser();

    dn = DictName;
    dates = [] as { returnDate: any }[];
    issueRrDate = null;
    dicts = {
        [DictName.Archive]: null as Dict[],
        [DictName.RejectReason]: null as Dict[],
        [DictName.Storage]: null as Dict[]
    };
    order: OrderExecution;

    @ViewChild('cr', {static: false})
    resultCheckDialog: any;
    st = OrderState;
    researchTheme = '';

    // Результаты проверки
    cresults = {} as CheckedData;
    private modalOpts: NgbModalOptions = {backdrop: 'static', keyboard: false, windowClass: 'execution-check'};
    private lastState = {} as any; // Последнее состояние кнопок

    constructor(private os: OrderService, private ordersService: OrdersService, private store: Store<fromRoot.State>,
                private dictService: DictService, @Inject(DateOptions) public dops, private router: Router, private route: ActivatedRoute,
                private auth: AuthService, private ms: NgbModal) {
        store.dispatch(fromButtons.buttons([{
                action: () => router.navigate(['/orders']),
                ico: 'fa-arrow-left',
                label: 'Вернуться',
            }, {
                action: () => store.dispatch(fromCardMode.editMode()),
                clazz: 'ml-2',
                ico: 'fa-edit',
                label: 'Изменить',
            }, {
                action: () => this.save(),
                clazz: 'ml-2',
                ico: 'fa-save',
                label: 'Сохранить'
            }, {
                action: () => this.check(),
                clazz: 'ml-2',
                ico: 'fa-info-circle',
                label: 'Проверить',
            }, {
                action: () => this.save(this.order.status == OrderState.Onexec ? OrderState.Ready :
                    this.order.status == OrderState.Ready ? OrderState.Exec : OrderState.Return),
                clazz: 'ml-3',
                ico: 'fa-check-circle',
                label: 'Оформить'
            }, {
                action: () => this.os.print(this.order.id),
                clazz: 'ml-2',
                ico: 'fa-print',
                label: 'Печать'
            }, {
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));
        dictService.getDicts(this.dicts);
        combineLatest(this.editMode$, this.user$, store.select(fromRoot.selectOrderStatus),
            store.select(fromRoot.selectOrderExecutorId))
            .pipe(takeUntil(componentDestroyed(this)))
            .subscribe(([mode, user, status, executorId]) => {
                const btns = {
                    1: {
                        hidden: mode,
                        disabled: [OrderState.Onexec, OrderState.Ready, OrderState.Dump].indexOf(status) == -1
                    },
                    2: {hidden: !mode, disabled: !mode},
                    3: {
                        disabled: !this.order || (new Date() < this.order.issueDate) ||
                            [OrderState.Cancel1, OrderState.Cancel2, OrderState.Cancel3,
                                OrderState.Dump, OrderState.Return].indexOf(status) > -1
                    },
                    4: {disabled: [OrderState.Onexec, OrderState.Ready, OrderState.Dump].indexOf(status) == -1}
                };
                if (!mode) {
                    let noAccess = !(user && user.roles.indexOf(Role.Executor) > -1);
                    if (!noAccess && status != OrderState.Onexec) {
                        // При статусе заказа больше чем 'На исполнении' Править заказ может только исполнитель этого заказа
                        noAccess = executorId != user.id;
                    }
                    [1, 4].forEach(k => btns[k].disabled = btns[k].disabled || noAccess);
                }
                this.lastState = btns;
                store.dispatch(fromButtons.change(btns));
            });
    }

    ngOnInit(): void {
        this.route.data.subscribe((data: { order: OrderExecution }) => {
            this.order = data.order;
            this.dictService.load(DictName.ResearchTheme, {
                archive: data.order.archiveId,
                profile: data.order.applicantId
            }).subscribe(res => {
                const v = res ? res.find(it => it.value == this.order.topicHeadingId) : null;
                this.researchTheme = v ? v.label : '';
            });

            if (this.order.issueRrDate) {
                this.issueRrDate = {isRange: false, singleDate: {jsDate: this.order.issueRrDate}};
            }
            this.order.docs.forEach(it => {
                this.dates.push({returnDate: it.returnDate ? {isRange: false, singleDate: {jsDate: it.returnDate}} : null});
            });
            if (!this.order.executorId) { // Если еще нету исполнителя то устанавливает того кто открыл карточку, если у него есть право
                this.user$.pipe(take(1)).subscribe(u => {
                    if (u.roles.indexOf(Role.Executor) > -1) {
                        this.order.executor = `${u.lastName} ${u.firstName} ${u.middleName}`;
                    }
                });
            }
        });
    }

    dateChanged(date: any, obj: any, field: string, elId: string) {
        if (date) {
            obj[field] = date.singleDate.jsDate;
        } else {
            obj[field] = null;
            (document.getElementById(elId) as HTMLInputElement).value = '';
        }
    }

    save(status?: OrderState) {
        this.store.dispatch(fromButtons.changeBulk([[2, 4], {disabled: true}]));
        this.os.saveExecution(status ? {...this.order, status} : this.order).subscribe((res: OrderExecution) => {
            if (res) {
                this.store.dispatch(fromCardMode.viewMode());
                this.order = res;
            } else {
                this.store.dispatch(fromButtons.change(this.lastState));
            }
        });
    }

    getLabel(id: number | string, dict: DictName) {
        return this.dictService.getLabel(id, dict);
    }

    ngOnDestroy(): void {
    }

    private check() {
        this.os.check(this.order.id).subscribe(res => {
            if (res) {
                this.cresults = res;
                this.ms.open(this.resultCheckDialog, this.modalOpts);
            }
        });
    }
}

