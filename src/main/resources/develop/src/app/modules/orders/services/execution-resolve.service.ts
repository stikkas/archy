import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {OrderService} from './order.service';
import * as fromMode from '../../../reducers/card-mode';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {OrderExecution} from '../models/order-execution';

@Injectable()
export class ExecutionResolveService implements Resolve<OrderExecution> {

    constructor(private service: OrderService, private router: Router, private store: Store<fromRoot.State>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderExecution> {
        const id = parseInt(route.paramMap.get('id'));
        if (isNaN(id)) {
            this.router.navigate(['/orders']);
            return null;
        }
        this.store.dispatch(fromMode.viewMode());
        return this.service.getExecution(id).pipe(
            take<OrderExecution>(1),
            map((order: OrderExecution) => {
                if (order) {
                    return order;
                } else {
                    this.router.navigate(['/orders']);
                    return null;
                }
            })
        );
    }
}

