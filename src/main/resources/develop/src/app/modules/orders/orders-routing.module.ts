import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './list/list.component';
import {OrdersComponent} from './orders.component';
import {ReadingRoomComponent} from './reading-room/reading-room.component';
import {OrderComponent} from './order.component';
import {OrderResolveService} from './services/order-resolve.service';
import {ExecutionResolveService} from './services/execution-resolve.service';
import {ProcessResolveService} from './services/process-resolve.service';
import {ProcessingComponent} from './processing/processing.component';
import {ExecutionComponent} from './execution/execution.component';

const routes: Routes = [{
    path: '',
    component: OrdersComponent,
    children: [{
        path: '',
        pathMatch: 'full',
        component: ListComponent
    }, {
        path: 'order',
        component: OrderComponent,
        children: [{
            path: '',
            pathMatch: 'full',
            component: ReadingRoomComponent,
            resolve: {order: OrderResolveService}
        }, {
            path: ':id',
            component: ReadingRoomComponent,
            resolve: {order: OrderResolveService}
        }, {
            path: ':id/trans',
            component: ProcessingComponent,
            resolve: {order: ProcessResolveService}
        }, {
            path: ':id/exec',
            component: ExecutionComponent,
            resolve: {order: ExecutionResolveService}
        }]
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrdersRoutingModule {
}
