import {DocumentCase} from './document-case';
import {OrderState} from '../../../models/order-state';
import {OrderType} from './order-type';

export abstract class AbstractOrder {
    id?: number;
    readRoom?: string;
    issueReadRoom?: string;
    issueReadingRoom?: string;
    executorId?: number;

    /**
     * Идентификатор пользователя делавшего заказ
     */
    applicantId?: number;

    /**
     * Статус заказа
     */
    status?: OrderState;

    /**
     * Номер заказа
     */
    orderNumber?: number;

    /**
     * Номер личного дела пользователя, делавшего заказ
     */
    personNumber?: number | string;

    /**
     * Вид заказа
     */
    type?: OrderType;

    /**
     * Тема исследования
     */
    topicHeadingId?: number;

    /**
     * Отдел хранения
     */
    archiveId?: number;

    /**
     * Выдано в ЧЗ
     */
    issueRrDate?: Date;

    /**
     * Данные по документам
     */
    docs?: DocumentCase[];

    // Только для отображения в просмотре
    /**
     * Фамилия
     */
    lastName?: string;
    /**
     * Имя
     */
    firstName?: string;
    /**
     * Отчество
     */
    middleName?: string;
    /**
     * Год рождения
     */
    birthYear?: string;

    static normalizeDates(order: AbstractOrder): AbstractOrder {
        if (typeof order.issueRrDate == 'string') {
            order.issueRrDate = new Date(order.issueRrDate);
        }
        if (order.docs) {
            order.docs.forEach(it => {
                ['spisanoDate', 'returnDate'].forEach(k => {
                    if (typeof it[k] === 'string') {
                        it[k] = new Date(it[k]);
                    }
                });
            });
        }
        return order;
    }
}

