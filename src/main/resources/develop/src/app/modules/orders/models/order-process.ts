import {AbstractOrder} from './abstract-order';

export class OrderProcess extends AbstractOrder {
    /**
     * Начальник отдела хранения
     */
    chiefArchive?: string;

    /**
     * Разрешение на выдачу
     */
    permissionIssue?: string;

    /**
     * Дата разрешения / отказа
     */
    permissionDate?: Date;

    /**
     * Изменение сроков выдачи
     */
    changeIssueDate?: Date;

    /**
     * Причина (изменения сроков)
     */
    changeIssueReason?: string;

    /**
     * Номер хранилища
     */
    storageId?: number;

    /**
     * Причина отказа
     */
    rejectReason?: string;

    static normalizeDates(process: OrderProcess): OrderProcess {
        ['permissionDate', 'changeIssueDate'].forEach(it => {
            if (typeof process[it] === 'string') {
                process[it] = new Date(process[it]);
            }
        });

        return AbstractOrder.normalizeDates(process);
    }
}

