import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {OrderRegistration} from '../models/order-registration';
import {OrderService} from './order.service';
import * as fromMode from '../../../reducers/card-mode';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';

@Injectable()
export class OrderResolveService implements Resolve<OrderRegistration> {

    constructor(private service: OrderService, private router: Router, private store: Store<fromRoot.State>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderRegistration> {
        const id = parseInt(route.paramMap.get('id'));
        const nan = isNaN(id);
        this.store.dispatch(nan ? fromMode.editMode() : fromMode.viewMode());
        return this.service.getRegistration(nan ? route.queryParams.type : id).pipe(
            take<OrderRegistration>(1),
            map((order: OrderRegistration) => {
                if (order) {
                    return order;
                } else {
                    this.router.navigate(['/orders']);
                    return null;
                }
            })
        );
    }
}

