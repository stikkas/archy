import {AbstractOrder} from './abstract-order';

export class OrderRegistration extends AbstractOrder {
    /**
     * Сотрудник читального зала
     */
    registrator?: string;

    /**
     * Дата приемя заказа
     */
    regDate?: Date;

    /**
     * Дата выдачи
     */
    issueDate?: Date;

    /**
     * Дата возврата (фактическая)
     */
    returnDate?: Date;

    /**
     * Контрольная дата возврата
     */
    controlReturnDate?: Date;

    /**
     * Причина
     */
    cause?: string;


    static normalizeDates(registration: OrderRegistration): OrderRegistration {
        ['regDate', 'issueDate', 'returnDate', 'controlReturnDate'].forEach(it => {
            if (typeof registration[it] === 'string') {
                registration[it] = new Date(registration[it]);
            }
        });

        return AbstractOrder.normalizeDates(registration);
    }
}

