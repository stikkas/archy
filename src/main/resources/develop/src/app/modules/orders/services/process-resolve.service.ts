import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {OrderService} from './order.service';
import * as fromMode from '../../../reducers/card-mode';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {OrderProcess} from '../models/order-process';

@Injectable()
export class ProcessResolveService implements Resolve<OrderProcess> {

    constructor(private service: OrderService, private router: Router, private store: Store<fromRoot.State>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderProcess> {
        const id = parseInt(route.paramMap.get('id'));
        if (isNaN(id)) {
            this.router.navigate(['/orders']);
            return null;
        }
        this.store.dispatch(fromMode.viewMode());
        return this.service.getProcessing(id).pipe(
            take<OrderProcess>(1),
            map((order: OrderProcess) => {
                if (order) {
                    return order;
                } else {
                    this.router.navigate(['/orders']);
                    return null;
                }
            })
        );
    }
}

