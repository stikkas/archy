import {Component, Inject, QueryList, ViewChildren, ViewEncapsulation} from '@angular/core';
import {SearchComponent} from '../../../pages/search.component';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {OrderTableRow} from '../models/order-table-row';
import {OrdersService} from '../services/orders.service';
import {SortEvent} from '../../../models/sort-event';
import {DictName} from '../../../models/dict-name';
import {DictService} from '../../../services/dict.service';
import {DateOptions} from '../../../injectors/date-optinos';
import {SortableDirective} from '../../components/directives/sortable.directive';
import {Router} from '@angular/router';
import {Dict} from '../../../models/dict';
import {AuthService} from '../../../services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Role} from '../../../models/role';
import {OrderType} from '../models/order-type';

export const addUrl = '/orders/order';

@Component({
    templateUrl: './list.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ListComponent extends SearchComponent<OrderTableRow> {
    dn = DictName;
    dates = {
        rdate: null,
        crdate: null,
        edate: null
    };
    address: string;
    showRR: boolean;
    @ViewChildren(SortableDirective) headers: QueryList<SortableDirective>;
    dicts = {
        [DictName.ReadingRoom]: null as Dict[],
        [DictName.ArchiveShort]: null as Dict[],
        [DictName.OrderState]: null as Dict[],
        [DictName.Executor]: null as Dict[],
        [DictName.OrderType]: null as Dict[]
    };

    constructor(ss: OrdersService, store: Store<fromRoot.State>, private dictService: DictService,
                @Inject(DateOptions) public dops, router: Router, auth: AuthService) {
        super(ss, store, router, 'Заказы', '', auth, {
            ico: 'fa-plus-square',
            disabled: true,
            label: 'Добавить',
            menu: [{title: 'ФП заказ на выдачу архивных документов', link: [addUrl], param: {type: OrderType.Fp}},
                {title: 'Заказ на выдачу архивных документов', link: [addUrl], param: {type: OrderType.Standard}}]
        });
        dictService.getDicts(this.dicts);
        auth.getUser().pipe(takeUntil(componentDestroyed(this))).subscribe(user => {
            if (user) {
                if (Role.hasAny(user.roles, [Role.Rr3, Role.Rr2, Role.Rr1])) {
                    this.address = user.rraddress;
                }
                this.showRR = Role.hasAny(user.roles, [Role.Admin, Role.ChiefRr]);
            }
        });
    }

    sort(event: SortEvent) {
        this.headers.forEach(header => {
            if (header.sortable !== event.column) {
                header.direction = '';
            }
        });
        this.load(this.ss.curPage, event);
    }

    dateChanged(date: any, obj: any, field: string) {
        if (date) {
            obj[field] = date.singleDate.jsDate;
        } else {
            obj[field] = null;
            (document.getElementById(field) as HTMLInputElement).value = '';
        }
    }
}
