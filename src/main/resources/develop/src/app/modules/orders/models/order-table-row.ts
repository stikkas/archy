export interface OrderTableRow {
    id: number;
    /**
     * Номер заказа
     */
    orderNumber: number;

    /**
     * Отдел хранения
     */
    archive: string;

    /**
     * ФИО пользователя
     */
    applicant: string;

    /**
     * Дата приемя заказа
     */
    regDate: Date;

    /**
     * Дата исполнения
     */
    execDate: Date;

    /**
     * Контрольная дата возврата
     */
    returnDate: Date;

    /**
     * Вид заказа
     */
    type: string;

    /**
     * Статус заказа
     */
    status: string;

    /**
     * Исполнитель
     */
    executor: string;

    /**
     * Читальный зал
     */
    readRoom: string;
}

