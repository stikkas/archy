export interface DocumentCase {
    id?: number;
    /**
     * Номер фонда
     */
    fundNumber?: string;

    /**
     * Номер описи
     */
    opisNumber?: string;

    /**
     * Номер ед. хранения / дела
     */
    unitNumber?: string;

    /**
     * Заголовок единицы хранения
     */
    unitTitle?: string;

    /**
     * Кол-во листов
     */
    pages?: number;

    /**
     *  Дело подготовлено
     */
    prepared?: boolean;

    /**
     * Причина отказа
     */
    rejectReasonId?: number;

    /**
     * Списано
     */
    spisanoDate?: Date;

    /**
     * Возвращено в архивохранилище
     */
    returnDate?: Date;
}
