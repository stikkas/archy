import {CheckedDataOrder} from './checked-data-order';

export interface CheckedData {
    pass?: boolean;
    date?: Date;
    files?: number;
    pages?: number;
    orders?: CheckedDataOrder[];
}
