import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {DictName} from '../../../models/dict-name';
import {DictService} from '../../../services/dict.service';
import {DateOptions} from '../../../injectors/date-optinos';
import * as fromButtons from '../../../reducers/buttons';
import * as fromCardMode from '../../../reducers/card-mode';
import {ActivatedRoute, Router} from '@angular/router';
import {map, take, takeUntil} from 'rxjs/operators';
import {combineLatest, Observable} from 'rxjs';
import {Dict} from '../../../models/dict';
import {Role} from '../../../models/role';
import {AuthService} from '../../../services/auth.service';
import {OrderState} from '../../../models/order-state';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {OrderService} from '../services/order.service';
import {OrdersService} from '../services/orders.service';
import {OrderProcess} from '../models/order-process';

@Component({
    templateUrl: './processing.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ProcessingComponent implements OnInit, OnDestroy {
    editMode$ = this.store.select(fromRoot.selectEditMode);
    dn = DictName;
    dates = {changeIssueDate: null};

    dicts = {
        [DictName.Archive]: null as Dict[],
        [DictName.ReadingRoom]: null as Dict[],
        [DictName.RejectReason]: null as Dict[],
        [DictName.Storage]: null as Dict[],
        [DictName.PermissionIssue]: null as Dict[]
    };
    order: OrderProcess;
    denied = 'DENIED';
    researchTheme = '';
    rrArchive$: Observable<Dict[]>;
    private lastState = {} as any; // Последнее состояние кнопок
    private user$ = this.auth.getUser();

    constructor(private os: OrderService, private ordersService: OrdersService, private store: Store<fromRoot.State>,
                private dictService: DictService, @Inject(DateOptions) public dops, private router: Router, private route: ActivatedRoute,
                private auth: AuthService) {
        store.dispatch(fromButtons.buttons([{
                action: () => router.navigate(['/orders']),
                ico: 'fa-arrow-left',
                label: 'Вернуться',
            }, {
                action: () => {
                    store.dispatch(fromCardMode.editMode());
                },
                clazz: 'ml-2',
                ico: 'fa-edit',
                label: 'Изменить',
            }, {
                action: () => this.save(),
                clazz: 'ml-2',
                ico: 'fa-save',
                label: 'Сохранить'
            }, {
                action: () => this.save(this.order.permissionIssue === this.denied ? OrderState.Reject : OrderState.Onexec),
                clazz: 'ml-3',
                ico: 'fa-check-circle',
                label: 'Оформить'
            }, {
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));
        dictService.getDicts(this.dicts);


        combineLatest(this.editMode$, this.user$, store.select(fromRoot.selectOrderStatus))
            .pipe(takeUntil(componentDestroyed(this)))
            .subscribe(([mode, user, status]) => {
                const btns = {
                    1: {hidden: mode, disabled: status != OrderState.Accept},
                    2: {hidden: !mode, disabled: !mode},
                    3: {disabled: status != OrderState.Accept},
                };
                if (!mode) {
                    const noAccess = !(user && user.roles.indexOf(Role.ChiefArchive) > -1);
                    [1, 3].forEach(k => btns[k].disabled = btns[k].disabled || noAccess);
                }
                this.lastState = btns;
                store.dispatch(fromButtons.change(btns));
            });
    }

    ngOnInit(): void {
        this.route.data.subscribe((data: { order: OrderProcess }) => {
            this.dictService.load(DictName.ResearchTheme, {
                archive: data.order.archiveId,
                profile: data.order.applicantId
            }).subscribe(res => {
                const v = res ? res.find(it => it.value == this.order.topicHeadingId) : null;
                this.researchTheme = v ? v.label : '';
            });

            this.order = data.order;
            if (this.order.changeIssueDate) {
                this.dates.changeIssueDate = {isRange: false, singleDate: {jsDate: this.order.changeIssueDate}};
            }
            this.user$.pipe(take(1)).subscribe(u => {
                // Если еще нету начальника то устанавливает того кто открыл карточку, если у него есть право
                if (u.roles.indexOf(Role.ChiefArchive) > -1) {
                    this.rrArchive$ = this.dictService.load(DictName.RrArchive, u.archiveId)
                        .pipe(map((it: Dict[]) => {
                            if (it.length == 1 && !this.order.issueReadingRoom) {
                                this.order.issueReadingRoom = it[0].value as string;
                            }
                            this.readingRoomChanged({value: this.order.issueReadingRoom});
                            return it;
                        }));
                    if (!this.order.chiefArchive) {
                        this.order.chiefArchive = `${u.lastName} ${u.firstName} ${u.middleName}`;
                    }
                }
            });
        });
    }

    dateChanged(date: any, elId: string) {
        if (date) {
            this.order.changeIssueDate = date.singleDate.jsDate;
        } else {
            this.order.changeIssueDate = null;
            (document.getElementById(elId) as HTMLInputElement).value = '';
        }
    }

    save(status?: OrderState) {
        this.store.dispatch(fromButtons.changeBulk([[2, 3], {disabled: true}]));
        this.os.saveProcessing(status ? {...this.order, status} : this.order).subscribe((res: OrderProcess) => {
            if (res) {
                this.store.dispatch(fromCardMode.viewMode());
                this.order = res;
                this.ordersService.loadPage();
            } else {
                this.store.dispatch(fromButtons.change(this.lastState));
            }
        });
    }

    getLabel(id: number | string, dict: DictName) {
        return this.dictService.getLabel(id, dict);
    }

    ngOnDestroy(): void {
    }

    readingRoomChanged(dict: any) {
        this.dictService.load(DictName.Storage, {rr: dict.value, archive: this.order.archiveId}).subscribe(res => {
            this.dicts[DictName.Storage] = res;
            if (this.order.storageId && !res.some(it => it.value == this.order.storageId)) {
                this.order.storageId = null;
            }
        });
    }
}

