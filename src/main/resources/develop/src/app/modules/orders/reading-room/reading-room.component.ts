import {Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {DictName} from '../../../models/dict-name';
import {DictService} from '../../../services/dict.service';
import {DateOptions} from '../../../injectors/date-optinos';
import * as fromButtons from '../../../reducers/buttons';
import * as fromCardMode from '../../../reducers/card-mode';
import {ActivatedRoute, Router} from '@angular/router';
import {catchError, debounceTime, distinctUntilChanged, map, switchMap, takeUntil} from 'rxjs/operators';
import {combineLatest, concat, Observable, of, Subject} from 'rxjs';
import {Dict} from '../../../models/dict';
import {OrderRegistration} from '../models/order-registration';
import {OrderType} from '../models/order-type';
import {addUrl} from '../list/list.component';
import {Role} from '../../../models/role';
import {AuthService} from '../../../services/auth.service';
import {OrderState} from '../../../models/order-state';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../services/order.service';
import {OrdersService} from '../services/orders.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    templateUrl: './reading-room.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ReadingRoomComponent implements OnInit, OnDestroy {
    editMode$ = this.store.select(fromRoot.selectEditMode);
    usersInput$ = new Subject<string>();
    users$: Observable<Dict[]>;
    dn = DictName;
    dates = [] as { spisanoDate: any }[];

    dicts = {
        [DictName.Archive]: null as Dict[],
        [DictName.RejectReason]: null as Dict[]
    };
    order: OrderRegistration;

    @ViewChild('rd', {static: false})
    removeDialog: any;

    researchTheme = '';
    st = OrderState;
    archivesOfUser$: Observable<Dict[]>;
    private modalOpts: NgbModalOptions = {backdrop: 'static', keyboard: false};
    private lastState = {} as any; // Последнее состояние кнопок

    constructor(private os: OrderService, private ordersService: OrdersService, private store: Store<fromRoot.State>,
                private dictService: DictService, @Inject(DateOptions) public dops, private router: Router,
                private route: ActivatedRoute, private toastr: ToastrService,
                auth: AuthService, private ms: NgbModal) {
        store.dispatch(fromButtons.buttons([{
                action: () => router.navigate(['/orders']),
                ico: 'fa-arrow-left',
                label: 'Вернуться',
            }, {
                clazz: 'ml-2',
                ico: 'fa-plus-square',
                label: 'Добавить заказ',
                menu: [{title: 'ФП заказ на выдачу архивных документов', link: [addUrl], param: {type: OrderType.Fp}},
                    {title: 'Заказ на выдачу архивных документов', link: [addUrl], param: {type: OrderType.Standard}}]
            }, {
                action: () => {
                    store.dispatch(fromCardMode.editMode());
                },
                clazz: 'ml-2',
                ico: 'fa-edit',
                label: 'Изменить',
            }, {
                action: () => this.save(),
                clazz: 'ml-2',
                ico: 'fa-save',
                label: 'Сохранить'
            }, {
                action: () => this.remove(),
                clazz: 'ml-2',
                ico: 'fa-times',
                label: 'Удалить',
            }, {
                action: () => this.save(this.order.status == OrderState.Exec ? OrderState.Dump : OrderState.Accept),
                clazz: 'ml-3',
                ico: 'fa-check-circle',
                label: 'Оформить'
            }, {
                action: () => this.save(this.order.status == OrderState.Accept ? OrderState.Cancel1 :
                    (this.order.status == OrderState.Reject || this.order.status == OrderState.Onexec) ? OrderState.Cancel2 :
                        OrderState.Cancel3),
                clazz: 'ml-2',
                ico: 'fa-times-circle',
                label: 'Аннулировать'
            }, {
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));
        dictService.getDicts(this.dicts);
        combineLatest(this.editMode$, auth.getUser(), store.select(fromRoot.selectOrderId), store.select(fromRoot.selectOrderStatus))
            .pipe(takeUntil(componentDestroyed(this)))
            .subscribe(([mode, user, id, status]) => {
                const btns = {
                    1: {disabled: mode},
                    2: {hidden: mode, disabled: [OrderState.Exec, OrderState.Pre].indexOf(status) == -1},
                    3: {hidden: !mode, disabled: !mode},
                    4: {disabled: !id || status != OrderState.Pre},
                    5: {disabled: status && [OrderState.Pre, OrderState.Exec].indexOf(status) == -1},
                    6: {
                        disabled: [OrderState.Accept, OrderState.Reject, OrderState.Onexec, OrderState.Ready]
                            .indexOf(status) == -1
                    }
                };
                if (!mode) {
                    const noAccess = !(user && Role.hasAny(user.roles, [Role.Rr1, Role.Rr2, Role.Rr3]));
                    [1, 2, 4, 5, 6].forEach(k => btns[k].disabled = btns[k].disabled || noAccess);
                }
                this.lastState = btns;
                store.dispatch(fromButtons.change(btns));
            });
    }

    ngOnInit(): void {
        this.route.data.subscribe((data: { order: OrderRegistration }) => {
            // this.dicts[DictName.ResearchTheme] = [];
            if (data.order.applicantId) {
                this.archivesOfUser$ = this.dictService.load(DictName.Archive, `${data.order.applicantId}`);
                if (data.order.archiveId) {
                    this.getThemesDict(data.order.archiveId, data.order.applicantId);
                }
            }
            this.order = data.order;
            this.order.docs.forEach(it => {
                this.dates.push({
                    spisanoDate: it.spisanoDate ? {isRange: false, singleDate: {jsDate: it.spisanoDate}} : null
                });
            });

            this.users$ = concat(this.order.applicantId ? this.dictService.load(DictName.User, this.order.lastName) : of([]),
                this.usersInput$.pipe(
                    debounceTime(200),
                    distinctUntilChanged(),
                    switchMap(term => this.dictService.load(DictName.User, term).pipe(
                        catchError(() => of([] as Dict[])), // empty list on error
                    ))
                ));
        });
    }

    dateChanged(date: any, obj: any, field: string, elId: string) {
        if (date) {
            obj[field] = date.singleDate.jsDate;
        } else {
            obj[field] = null;
            (document.getElementById(elId) as HTMLInputElement).value = '';
        }
    }

    save(status?: OrderState) {
        this.store.dispatch(fromButtons.changeBulk([[3, 4, 5, 6], {disabled: true}]));
        this.os.saveRegistration(status ? {...this.order, status} : this.order).subscribe((res: OrderRegistration) => {
            if (res) {
                if (!this.order.id || this.order.id != res.id) {
                    this.router.navigate(['/orders/order', res.id]);
                } else {
                    this.store.dispatch(fromCardMode.viewMode());
                    this.order = res;
                }
                this.ordersService.loadPage();
            } else {
                this.store.dispatch(fromButtons.change(this.lastState));
            }
        });
    }

    delDoc(idx: number) {
        this.order.docs = this.order.docs.filter((it, i) => i !== idx);
        this.dates = this.dates.filter((it, i) => idx !== i);
    }

    addDoc() {
        this.order.docs.push(this.order.docs.length ? {fundNumber: this.order.docs[0].fundNumber} : {});
        this.dates.push({spisanoDate: null});
    }

    userChanged(data: any) {
        if (data && data.label) {
            const fio = data.label.split(' ');
            this.order.lastName = fio[0];
            this.order.firstName = fio[1];
            this.order.middleName = fio[2] && fio[2].replace(',', '');
            this.order.birthYear = fio[3];
            this.order.personNumber = data.short;
            this.archivesOfUser$ = this.dictService.load(DictName.Archive, data.value).pipe(map(it => {
                if (!it || it.length == 0) {
                    this.toastr.warning('Дата разрешения не соответствует текущей дате заказа');
                }
                return it;
            }));
        } else {
            this.order.lastName = this.order.firstName = this.order.middleName = this.order.personNumber = this.order.birthYear = null;
        }
        this.order.topicHeadingId = null;
        this.order.archiveId = null;
    }

    archiveChanged(data: any) {
        if (data && data.label) {
            this.getThemesDict(this.order.archiveId, this.order.applicantId);
        } else {
            this.dicts[DictName.ResearchTheme] = [];
        }
        this.order.topicHeadingId = null;
    }

    getLabel(id: number | string, dict: DictName) {
        return this.dictService.getLabel(id, dict);
    }

    remove() {
        this.ms.open(this.removeDialog, this.modalOpts).result.then(result => {
            if (result === 'yes') {
                this.os.remove(this.order.id).subscribe(res => {
                    if (res) {
                        this.ordersService.loadPage();
                        this.router.navigate(['/orders']);
                    }
                });
            }
        });
    }

    ngOnDestroy(): void {
    }

    fundNumberChanged() {
        const fn = this.order.docs[0].fundNumber;
        this.order.docs.forEach((it, i) => i > 0 && (it.fundNumber = fn));
    }

    private getThemesDict(archive: number, profile: number) {
        this.dictService.load(DictName.ResearchTheme, {archive, profile})
            .subscribe(res => {
                this.dicts[DictName.ResearchTheme] = res;
                this.setResearchTheme();
            });
    }

    private setResearchTheme() {
        const v = this.dicts[DictName.ResearchTheme].find(it => it.value == this.order.topicHeadingId);
        this.researchTheme = v ? v.label : '';
    }
}


