import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SearchService} from '../../../services/search.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {OrderTableRow} from '../models/order-table-row';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class OrdersService extends SearchService<OrderTableRow> {

    constructor(http: HttpClient, store: Store<fromRoot.State>, toastr: ToastrService) {
        super(http, 'orders', toastr, {
            rr: undefined,
            onumber: '',
            archive: undefined,
            applicant: '',
            rdate: null,
            edate: null,
            crdate: null,
            type: undefined,
            state: undefined,
            executor: undefined
        }, store);
        this.clear();
    }

    clear() {
        super.clear();
        this.loadPage();
    }
}

