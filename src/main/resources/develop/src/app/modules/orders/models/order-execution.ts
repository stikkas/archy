import {AbstractOrder} from './abstract-order';

export class OrderExecution extends AbstractOrder {
    /**
     * Исполнитель
     */
    executor?: string;
    executorId?: number;

    /**
     * Номер хранилища
     */
    storageId?: number;

    /**
     * Дата выдачи, нужно для проверки
     */
    issueDate?: Date;

    static normalizeDates(exec: OrderExecution): OrderExecution {
        if (typeof exec.issueDate === 'string') {
            exec.issueDate = new Date(exec.issueDate);
        }
        return AbstractOrder.normalizeDates(exec);
    }
}

