import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OrderRegistration} from '../models/order-registration';
import {AuthService} from '../../../services/auth.service';
import {catchError, map, take} from 'rxjs/operators';
import {OrderType} from '../models/order-type';
import {ActionStatus} from '../../../models/action.status';
import {FieldErrorService} from '../../../services/field-error.service';
import {OrderProcess} from '../models/order-process';
import {OrderExecution} from '../models/order-execution';
import * as fromOrder from '../../../reducers/order';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {AbstractOrder} from '../models/abstract-order';

@Injectable()
export class OrderService {
    private baseUrl = '/api/private/orders';

    constructor(private http: HttpClient, private auth: AuthService, private fes: FieldErrorService,
                private store: Store<fromRoot.State>) {
    }

    getRegistration(id: number | OrderType): Observable<OrderRegistration> {
        if (typeof id === 'number') {
            return this.http.get<OrderRegistration>(`${this.baseUrl}/${id}`)
                .pipe(map(res => {
                        this.setState(res);
                        return OrderRegistration.normalizeDates(res);
                    }),
                    catchError(err => this.fes.errorHandler(err)));
        }
        return this.auth.getUser().pipe(take(1), map(u => {
            const order = {
                registrator: `${u.lastName} ${u.firstName} ${u.middleName}`,
                type: id, docs: []
            };
            this.setState(order);
            return order;
        }));
    }

    getProcessing(id: number): Observable<OrderProcess> {
        return this.http.get<OrderProcess>(`${this.baseUrl}/${id}/process`)
            .pipe(map(res => {
                    this.setState(res);
                    return OrderProcess.normalizeDates(res);
                }),
                catchError(err => this.fes.errorHandler(err)));
    }

    getExecution(id: number): Observable<OrderExecution> {
        return this.http.get<OrderExecution>(`${this.baseUrl}/${id}/execution`)
            .pipe(map(res => {
                    this.setState(res);
                    return OrderExecution.normalizeDates(res);
                }),
                catchError(err => this.fes.errorHandler(err)));
    }

    saveRegistration(data: OrderRegistration) {
        return this.http.post(this.baseUrl, data)
            .pipe(map((res: ActionStatus) => res.status ?
                this.fes.success() && this.setState(res.data) && OrderRegistration.normalizeDates(res.data)
                : this.fes.errorHandler(res.data)),
                catchError(err => this.fes.errorHandler(err)));
    }

    saveProcessing(data: OrderProcess) {
        return this.http.post(`${this.baseUrl}/${data.id}/process`, data)
            .pipe(map((res: ActionStatus) => res.status ?
                this.fes.success() && this.setState(res.data) && OrderProcess.normalizeDates(res.data)
                : this.fes.errorHandler(res.data)),
                catchError(err => this.fes.errorHandler(err)));
    }

    saveExecution(data: OrderExecution) {
        return this.http.post(`${this.baseUrl}/${data.id}/execution`, data)
            .pipe(map((res: ActionStatus) => res.status ?
                this.fes.success() && this.setState(res.data) && OrderExecution.normalizeDates(res.data)
                : this.fes.errorHandler(res.data)),
                catchError(err => this.fes.errorHandler(err)));
    }

    remove(id: number) {
        return this.http.delete<boolean>(`${this.baseUrl}/${id}`)
            .pipe(catchError(err => this.fes.errorHandler(err)));
    }

    check(id: number) {
        return this.http.get(`${this.baseUrl}/${id}/check`)
            .pipe(catchError(err => this.fes.errorHandler(err)));
    }

    print(id: number) {
        window.open(`${this.baseUrl}/${id}/print`);
    }

    private setState(order: AbstractOrder) {
        this.store.dispatch(fromOrder.order({status: order.status, id: order.id, executorId: order.executorId}));
        return true;
    }
}


