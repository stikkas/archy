export interface CheckedDataOrder {
    orderNumber: number;
    archive: string;
    files: number;
    pages: number;
    state: string;
}
