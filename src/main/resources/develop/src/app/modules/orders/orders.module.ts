import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrdersRoutingModule} from './orders-routing.module';
import {OrdersComponent} from './orders.component';
import {FormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';
import {ListComponent} from './list/list.component';
import {ReadingRoomComponent} from './reading-room/reading-room.component';
import {OrdersService} from './services/orders.service';
import {OrderComponent} from './order.component';
import {OrderResolveService} from './services/order-resolve.service';
import {OrderService} from './services/order.service';
import {NgxMaskModule} from 'ngx-mask';
import {ExecutionResolveService} from './services/execution-resolve.service';
import {ProcessResolveService} from './services/process-resolve.service';
import {ProcessingComponent} from './processing/processing.component';
import {ExecutionComponent} from './execution/execution.component';

@NgModule({
    declarations: [OrdersComponent, ListComponent, OrderComponent, ReadingRoomComponent,
        ProcessingComponent, ExecutionComponent],
    imports: [
        CommonModule,
        OrdersRoutingModule,
        FormsModule,
        ComponentsModule,
        NgxMaskModule.forChild()
    ],
    providers: [OrdersService, OrderResolveService, OrderService, ExecutionResolveService, ProcessResolveService]
})
export class OrdersModule {
}

