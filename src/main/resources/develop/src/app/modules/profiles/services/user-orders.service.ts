import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SearchService} from '../../../services/search.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {ToastrService} from 'ngx-toastr';
import {UserOrder} from '../../../models/user-order';

@Injectable()
export class UserOrdersService extends SearchService<UserOrder> {

    constructor(http: HttpClient, store: Store<fromRoot.State>, toastr: ToastrService) {
        super(http, 'user-orders', toastr, {id: null}, store, 12);
    }

    set id(id: number) {
        if (this.filter.id !== id) {
            this.filter.id = id;
            this.loadPage();
        }
    }

}

