import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfilesRoutingModule} from './profiles-routing.module';
import {ProfileComponent} from './profile/profile.component';
import {MainComponent} from './main/main.component';
import {VisitsComponent} from './visits/visits.component';
import {OrdersComponent} from './orders/orders.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {FormsModule} from '@angular/forms';
import {ComponentsModule} from '../components/components.module';
import {ProfileResolveService} from './services/profile-resolve.service';
import {ProfileService} from './services/profile.service';
import {VisitsResolveService} from './services/visits-resolve.service';
import {NgxMaskModule} from 'ngx-mask';
import {VisitsService} from './services/visits.service';
import {UserOrdersService} from './services/user-orders.service';
import {UserOrdersResolveService} from './services/user-orders-resolve.service';

@NgModule({
    declarations: [MainComponent, ProfileComponent, VisitsComponent, OrdersComponent, StatisticsComponent],
    imports: [
        CommonModule,
        ProfilesRoutingModule,
        FormsModule,
        ComponentsModule,
        NgxMaskModule.forChild()
    ],
    providers: [ProfileResolveService, ProfileService, VisitsResolveService, VisitsService,
        UserOrdersService, UserOrdersResolveService]
})
export class ProfilesModule {
}

