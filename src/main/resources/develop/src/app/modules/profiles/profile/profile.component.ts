import {Component, Inject, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import * as fromProfile from '../../../reducers/profile';
import * as fromCardMode from '../../../reducers/card-mode';
import * as fromButtons from '../../../reducers/buttons';
import {Profile} from '../../../models/profile';
import {DictName} from '../../../models/dict-name';
import {DictService} from '../../../services/dict.service';
import {AngularMyDatePickerDirective} from 'angular-mydatepicker';
import {ArchRecord} from '../../../models/arch-record';
import {ActivatedRoute, Router} from '@angular/router';
import {ProfileService} from '../services/profile.service';
import {DateOptions} from '../../../injectors/date-optinos';
import {UsersService} from '../../../services/users.service';
import {title} from '../../../reducers/title';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {combineLatest} from 'rxjs';
import {Dict} from '../../../models/dict';
import {AuthService} from '../../../services/auth.service';
import {Role} from '../../../models/role';
import {ResearchTopic} from '../../../models/research-topic';

export const msInYear = 365 * 24 * 60 * 60 * 1000;

@Component({
    templateUrl: './profile.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnDestroy, OnInit {
    editMode$ = this.store.select(fromRoot.selectEditMode);
    data: Profile;
    currentDep = 0;

    dates = {
        firstRegDate: null,
        issuePassDate: null,
        topics: [] as { startDate: any, endDate: any }[]
    };

    dn = DictName;

    @ViewChildren('depsd')
    depsds: QueryList<AngularMyDatePickerDirective>;

    @ViewChildren('dp')
    dps: QueryList<AngularMyDatePickerDirective>;

    dicts = {
        [DictName.Country]: null as Dict[],
        [DictName.Archive]: null as Dict[],
        [DictName.TopicHeading]: null as Dict[],
        [DictName.Education]: null as Dict[],
        [DictName.ScienceRank]: null as Dict[]
    };

    constructor(private store: Store<fromRoot.State>, private dictService: DictService,
                private route: ActivatedRoute, private profileService: ProfileService,
                private router: Router, @Inject(DateOptions) public dops,
                private usersService: UsersService, auth: AuthService) {
        /** TODO если так и останется то надо этот кусок кода, до конца метода перенести в общий компонент,
         * т.к. он повторяется с VisitorComponent
         */
        dictService.getDicts(this.dicts);
        store.dispatch(fromButtons.buttons([{
                action: () => router.navigate(['/users']),
                ico: 'fa-arrow-left',
                label: 'Вернуться',
            }, {
                action: () => router.navigate(['/users/profiles']),
                clazz: 'ml-2',
                ico: 'fa-plus-square',
                label: 'Добавить',
            }, {
                action: () => {
                    store.dispatch(fromCardMode.editMode());
                },
                clazz: 'ml-2',
                ico: 'fa-edit',
                label: 'Изменить',
            }, {
                action: () => this.save(),
                clazz: 'ml-2',
                ico: 'fa-save',
                label: 'Сохранить'
            }, {
                action: () => this.remove(),
                clazz: 'ml-2',
                ico: 'fa-times',
                label: 'Удалить',
            }, {
                action: () => this.save(true),
                clazz: 'ml-3',
                ico: 'fa-check-circle',
                label: 'Регистрировать'
            }, {
                action: () => window.print(),
                clazz: 'ml-2',
                ico: 'fa-print',
                label: 'Печать'
            }, {
                action: () => router.navigate(['/main']),
                ico: 'fa-sign-out',
                clazz: 'ml-5',
                label: 'Выход'
            }]
        ));

        store.dispatch(title({text: 'Пользователь'}));
        combineLatest(this.editMode$, store.select(fromRoot.selectId), auth.getUser(), store.select(fromRoot.selectRegistered))
            .pipe(takeUntil(componentDestroyed(this))).subscribe(([mode, id, user, registered]) => {
            if (mode) {
                store.dispatch(fromButtons.change({
                    1: {disabled: true}, 2: {hidden: true}, 3: {hidden: false, disabled: false},
                    4: {disabled: !id || registered}, 5: {disabled: registered}
                }));
            } else {
                const noAccess = !(user && Role.hasAny(user.roles, [Role.Rr1, Role.Rr2, Role.Rr3]));
                store.dispatch(fromButtons.change({
                    1: {disabled: noAccess},
                    2: {hidden: false, disabled: noAccess},
                    3: {hidden: true},
                    4: {disabled: !id || noAccess || registered},
                    5: {disabled: noAccess || registered}
                }));
            }
        });
    }

    ngOnInit(): void {
        this.route.data.subscribe((data: { profile: Profile }) => {
            const profile = this.data = data.profile;
            if (profile.firstRegDate) {
                this.dates.firstRegDate = {isRange: false, singleDate: {jsDate: profile.firstRegDate}};
            }
            if (profile.issuePassDate) {
                this.dates.issuePassDate = {isRange: false, singleDate: {jsDate: profile.issuePassDate}};
            }
            this.setTopicDates();
        });
    }

    save(registered = false) {
        this.store.dispatch(fromButtons.change({3: {disabled: true}, 4: {disabled: true}, 5: {disabled: true}}));
        this.profileService.save({...this.data, registered: this.data.registered || registered}).subscribe((res: Profile) => {
            if (res) {
                this.store.dispatch(fromCardMode.viewMode());
                if (res.registered !== this.data.registered) {
                    this.store.dispatch(fromProfile.profile({payload: {registered: res.registered}}));
                }

                if (!this.data.id) {
                    this.router.navigate(['/users/profiles', res.id]);
                } else if (this.data.id === res.id) {
                    this.data = res;
                } else {
                    // TODO think about it
                    console.log('Receive another id. Magic!!!!');
                }
                this.usersService.loadPage();
            } else { // Что-то пошло не так
                this.store.dispatch(fromButtons.change({3: {disabled: false}, 4: {disabled: !this.data.id}, 5: {disabled: false}}));
            }
        });
    }

    remove() {
        this.profileService.remove(this.data.id).subscribe(res => {
            if (res) {
                this.usersService.loadPage();
                this.router.navigate(['/users']);
            }
        });
    }

    ngOnDestroy(): void {
    }

    getLabel(id: number, dict: DictName) {
        return this.dictService.getLabel(id, dict);
    }

    dateChanged(date: any, obj: any, field: string, elId: string) {
        if (date) {
            obj[field] = date.singleDate.jsDate;
            if (field == 'startDate' && !obj.endDate) {
                const future = new Date(obj.startDate.getTime() + msInYear);
                obj.endDate = future;
                this.dates.topics[Number(elId.substr(field.length))].endDate = {
                    isRange: false,
                    singleDate: {jsDate: future}
                };
                this.setApproveDate();
            }
        } else {
            obj[field] = null;
            (document.getElementById(elId) as HTMLInputElement).value = '';
        }
        if (field === 'endDate') {
            this.setApproveDate();
        }
    }

    toggleCalendar(idx: number, dirs: QueryList<AngularMyDatePickerDirective>) {
        const cal = dirs.find((it, i) => i === idx);
        if (cal) {
            cal.toggleCalendar();
        }
    }

    /**
     * Удаляет строку из таблицы с разрешениями
     * @param i - индекс строки
     */
    removeDep(i: number) {
        this.data.regs = this.data.regs.filter((it, idx) => idx !== i);
        this.setCurrentDep(0);
    }

    /**
     * Добавляет строку в таблицу с разрешениями
     */
    addDep() {
        this.data.regs.push({topics: [] as ResearchTopic[]} as ArchRecord);
    }

    addTopic() {
        this.data.regs[this.currentDep].topics.push({} as ResearchTopic);
        this.dates.topics.push({startDate: null, endDate: null});
    }

    removeTopic(i: number) {
        this.data.regs[this.currentDep].topics = this.data.regs[this.currentDep].topics.filter((it, idx) => idx !== i);
        this.dates.topics = this.dates.topics.filter((it, idx) => idx !== i);
        this.setApproveDate();
    }

    /**
     * Поменяли архив в строке таблицы
     */
    archiveChanged(d: ArchRecord) {
        if (this.data.personNumber) {
            d.personNumber = `${this.data.personNumber}-${d.archiveId}`;
        }
    }

    /**
     * Возвращает текст который должен отрисовываться в textarea, но отрисовывается как простой текст
     * @param remark - текст
     */
    textArea(remark: string) {
        return remark ? remark.split('\n').filter(it => it).join('<br>') : '';
    }

    /**
     * Устанавливает текущее дело при щелчке по строке в таблице архивов
     * @param i - индекс
     */
    setCurrentDep(i: number) {
        this.currentDep = i;
        this.setTopicDates();
    }

    private setApproveDate() {
        let time;
        this.data.regs[this.currentDep].approveDate =
            (time = Math.max(...this.dates.topics.map(it => it.endDate ? it.endDate.singleDate.jsDate.getTime() : 0))) && isFinite(time) ?
                new Date(time) : null;
    }

    private setTopicDates() {
        if (this.data.regs.length) {
            this.data.regs[this.currentDep].topics.forEach(it => {
                this.dates.topics.push({
                    startDate: it.startDate ? {isRange: false, singleDate: {jsDate: it.startDate}} : null,
                    endDate: it.endDate ? {isRange: false, singleDate: {jsDate: it.endDate}} : null
                });
            });
        }
    }
}

