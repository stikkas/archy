import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {VisitsService} from './visits.service';

@Injectable()
export class VisitsResolveService implements Resolve<boolean> {

    constructor(private visitsService: VisitsService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.visitsService.id = Number(route.paramMap.get('id'));
        return true;
    }
}

