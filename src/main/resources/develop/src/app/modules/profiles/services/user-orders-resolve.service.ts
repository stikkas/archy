import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {UserOrdersService} from './user-orders.service';

@Injectable()
export class UserOrdersResolveService implements Resolve<boolean> {

    constructor(private orderService: UserOrdersService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.orderService.id = Number(route.paramMap.get('id'));
        return true;
    }
}

