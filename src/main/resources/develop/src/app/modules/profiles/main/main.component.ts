import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as fromRoot from '../../../reducers';
import {Store} from '@ngrx/store';

@Component({
    templateUrl: './main.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

    id$ = this.store.select(fromRoot.selectId);
    sections = [{
        label: 'Личное дело',
        url: ''
    }, {
        label: 'Посещения',
        url: 'visits'
    }, {
        label: 'Заказы',
        url: 'orders'
    }
        // , {
        //     label: 'Статистика',
        //     url: 'statistics'
        // }
    ];

    constructor(private store: Store<fromRoot.State>) {
    }

    ngOnInit() {
    }

}

