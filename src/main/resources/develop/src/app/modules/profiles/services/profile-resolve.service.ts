import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Profile} from '../../../models/profile';
import {EMPTY, Observable, of} from 'rxjs';
import {ProfileService} from './profile.service';
import * as fromRoot from '../../../reducers';
import * as fromMode from '../../../reducers/card-mode';
import {mergeMap, take} from 'rxjs/operators';
import {Store} from '@ngrx/store';

@Injectable()
export class ProfileResolveService implements Resolve<Profile> {

    constructor(private profileService: ProfileService, private router: Router,
                private store: Store<fromRoot.State>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Profile> | Observable<never> {
        const id = route.paramMap.get('id');
        this.store.dispatch(id ? fromMode.viewMode() : fromMode.editMode());
        return this.profileService.getProfile(id).pipe(
            take<Profile>(1),
            mergeMap((profile: Profile) => {
                if (profile) {
                    return of(profile);
                } else {
                    this.router.navigate(['/users']);
                    return EMPTY;
                }
            })
        );
    }
}

