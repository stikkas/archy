import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {VisitsService} from '../services/visits.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {title} from '../../../reducers/title';

@Component({
    templateUrl: './visits.component.html',
    encapsulation: ViewEncapsulation.None
})
export class VisitsComponent implements OnInit, OnDestroy {
    page$ = this.ss.getPage();


    constructor(private ss: VisitsService, private store: Store<fromRoot.State>) {
        store.dispatch(title({text: 'Посещения'}));
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    load(page: number = 1) {
        this.ss.loadPage(page);
    }
}
