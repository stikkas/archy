import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SearchService} from '../../../services/search.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {Visit} from '../../../models/visit';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class VisitsService extends SearchService<Visit> {

    constructor(http: HttpClient, store: Store<fromRoot.State>, toastr: ToastrService) {
        super(http, 'visits', toastr, {id: null}, store, 12);
    }

    set id(id: number) {
        if (this.filter.id !== id) {
            this.filter.id = id;
            this.loadPage();
        }
    }

}

