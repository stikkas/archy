import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Profile} from '../../../models/profile';
import {catchError, map} from 'rxjs/operators';
import {ActionStatus} from '../../../models/action.status';
import * as fromProfile from '../../../reducers/profile';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {FieldErrorService} from '../../../services/field-error.service';

@Injectable()
export class ProfileService {
    private baseUrl = '/api/private/profiles';

    constructor(private http: HttpClient, private fes: FieldErrorService,
                private store: Store<fromRoot.State>) {
    }

    getProfile(id?: number | string) {
        return id ? this.http.get<Profile>(`${this.baseUrl}/${id}`)
                .pipe(map(res => {
                        this.store.dispatch(fromProfile.profile({payload: {registered: res.registered, id: res.id}}));
                        return Profile.normalizeDates(res);
                    }),
                    catchError(err => this.fes.errorHandler(err)))
            : (this.store.dispatch(fromProfile.profile({payload: {registered: false, id: null}})), of(new Profile()));
    }

    save(data: Profile) {
        return this.http.post(this.baseUrl, data)
            .pipe(map((res: ActionStatus) => res.status ?
                this.fes.success() && Profile.normalizeDates(res.data)
                : this.fes.errorHandler(res.data)),
                catchError(err => this.fes.errorHandler(err)));
    }

    remove(id: number) {
        return this.http.delete<boolean>(`${this.baseUrl}/${id}`)
            .pipe(catchError(err => this.fes.errorHandler(err)));
    }

}


