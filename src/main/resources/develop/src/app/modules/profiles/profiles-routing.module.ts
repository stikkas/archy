import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {MainComponent} from './main/main.component';
import {VisitsComponent} from './visits/visits.component';
import {OrdersComponent} from './orders/orders.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {ProfileResolveService} from './services/profile-resolve.service';
import {VisitsResolveService} from './services/visits-resolve.service';
import {UserOrdersResolveService} from './services/user-orders-resolve.service';

const routes: Routes = [{
    path: '',
    component: MainComponent,
    children: [{
        path: '',
        pathMatch: 'full',
        component: ProfileComponent,
        resolve: {profile: ProfileResolveService}
    }, {
        path: ':id',
        component: ProfileComponent,
        resolve: {profile: ProfileResolveService}
    }, {
        path: ':id/visits',
        component: VisitsComponent,
        resolve: {page: VisitsResolveService}
    }, {
        path: ':id/orders',
        component: OrdersComponent,
        resolve: {page: UserOrdersResolveService}
    }, {
        path: ':id/statistics',
        component: StatisticsComponent
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfilesRoutingModule {
}
