import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {title} from '../../../reducers/title';
import {UserOrdersService} from '../services/user-orders.service';

@Component({
    templateUrl: './orders.component.html',
    encapsulation: ViewEncapsulation.None
})
export class OrdersComponent implements OnInit, OnDestroy {
    page$ = this.ss.getPage();


    constructor(private ss: UserOrdersService, private store: Store<fromRoot.State>) {
        store.dispatch(title({text: 'Заказы'}));
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    load(page: number = 1) {
        this.ss.loadPage(page);
    }
}
