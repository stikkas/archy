import {CanLoad, Route, Router, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {map, take} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Role} from '../models/role';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanLoad {
    constructor(private auth: AuthService, private router: Router) {
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
        return this.auth.getUser().pipe(take(1), map(res => {
            if (res) {
                if (Role.hasAdminRole(res.roles)) {
                    return true;
                } else {
                    this.router.navigate(['/main']);
                }
            }
            this.router.navigate(['/enter']);
            return false;
        }));
    }
}

