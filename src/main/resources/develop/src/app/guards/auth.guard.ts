import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
    constructor(private auth: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.check();
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
        return this.check();
    }

    private check() {
        return this.auth.getUser().pipe(take(1), map(res => {
            if (res) {
                if (res.roles.length > 0) {
                    return true;
                } else {
                    // navigate to personal page
                }
            }
            this.router.navigate(['/enter']);
            return false;
        }));
    }
}
