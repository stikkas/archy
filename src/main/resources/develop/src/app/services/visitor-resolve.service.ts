import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {VisitorService} from './visitor.service';
import {Visitor} from '../models/visitor';
import {mergeMap, take} from 'rxjs/operators';
import * as fromMode from '../reducers/card-mode';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';

@Injectable({
    providedIn: 'root'
})
export class VisitorResolveService implements Resolve<Visitor> {

    constructor(private visitorService: VisitorService, private router: Router,
                private store: Store<fromRoot.State>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Visitor> | Observable<never> {
        const id = route.paramMap.get('id');
        this.store.dispatch(id ? fromMode.viewMode() : fromMode.editMode());
        return this.visitorService.getVisitor(id).pipe(
            take<Visitor>(1),
            mergeMap((visitor: Visitor) => {
                if (visitor) {
                    return of(visitor);
                } else {
                    this.router.navigate(['/visitors']);
                    return EMPTY;
                }
            })
        );
    }
}

