import {Injectable} from '@angular/core';
import {UserTableRow} from '../models/user-table-row';
import {HttpClient} from '@angular/common/http';
import {SearchService} from './search.service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {ToastrService} from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class UsersService extends SearchService<UserTableRow> {

    constructor(http: HttpClient, store: Store<fromRoot.State>, toastr: ToastrService) {
        super(http, 'users', toastr, {lname: '', fname: '', mname: '', byear: '', pnumber: ''}, store);
    }

}

