import {Injectable} from '@angular/core';
import {DictName} from '../models/dict-name';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {EMPTY, Observable, of, Subject} from 'rxjs';
import {Dict} from '../models/dict';

@Injectable({
    providedIn: 'root'
})
export class DictService {
    private baseUrl = '/api/private/dicts';
    private cache = {} as any;
    private pending = []; // Сейчас идет запрос на сервер
    private awaited = {} as { [key: string]: any[] }; // те кто пришел, а запрос уже ушел, но ответ еще не вернулся
    // Запрос label пришел раньше чем есть данные по справочнику
    private awaitedLabel = {} as { [key: string]: { id: string | number, subject: Subject<string> }[] };

    constructor(private http: HttpClient) {
    }

    load(name: DictName, query?: string | number | {}): Observable<Dict[]> {
        const params = !query ? null : (typeof query == 'object') ? query : {query};
        return this.http.get<Dict[]>(`${this.baseUrl}/${name}`, {params});
    }

    getDicts(dicts: { [key: string]: Dict[] }, force = {}) {
        Object.keys(dicts).forEach(k => {
            if (!force[k] && this.cache[k]) {
                dicts[k] = this.cache[k];
            } else if (this.pending.indexOf(k) === -1) {
                this.pending.push(k);
                this.http.get(`${this.baseUrl}/${k}`).pipe(
                    map((res: Dict[]) => {
                        dicts[k] = this.cache[k] = res;
                        this.pending = this.pending.map(it => it !== k);
                        let awaited = this.awaited[k];
                        if (awaited) {
                            awaited.forEach(it => it[k] = res);
                        }
                        awaited = this.awaitedLabel[k];
                        if (awaited) {
                            awaited.forEach(it => {
                                const v = res.find(i => it.id == i.value);
                                it.subject.next(v ? v.label : '');
                                it.subject.complete();
                            });
                        }
                        delete this.awaited[k];
                        delete this.awaitedLabel[k];
                    }),
                    catchError(() => {
                            this.cache[k] = null;
                            this.pending = this.pending.map(it => it !== k);
                            return EMPTY;
                        }
                    )).subscribe();
            } else {
                const list = this.awaited[k] || (this.awaited[k] = []);
                list.push(dicts);
            }
        });
    }

    reset(dict: DictName) {
        delete this.cache[dict];
    }

    /**
     * Возвращает значение справочника для заданного id
     * @param id - идентификатор
     * @param dict - справочник
     */
    getLabel(id: number | string, dict: DictName): Observable<string> | Subject<string> {
        const item = this.cache[dict];
        if (item) {
            const v = item.find(it => it.value == id);
            return of(v ? v.label : '');
        } else if (this.awaited[dict] || this.pending.indexOf(dict)) {
            const aws = this.awaitedLabel[dict];
            let bs: Subject<string>;
            if (aws) {
                const el = aws.find(it => it.id == id);
                if (el) {
                    bs = el.subject;
                } else {
                    bs = new Subject();
                    aws.push({id, subject: bs});
                }
            } else {
                bs = new Subject();
                this.awaitedLabel[dict] = [{id, subject: bs}];
            }
            return bs;
        }
        return of('');
    }
}

