import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ReportsService {
    private baseUrl = '/api/private/reports/';

    create(url: string, params: any) {
        const qs = [];
        for (const k in params) {
            const v = params[k];
            if (v === false || v) {
                qs.push(`${k}=${v instanceof Date ? (new Date(v.getTime() - v.getTimezoneOffset() * 60000)).toISOString() : v}`);
            }
        }
        window.open(`${this.baseUrl}${url}${qs.length ? ('?' + qs.join('&')) : ''}`);
    }
}

