import {Injectable} from '@angular/core';
import * as fromRoot from '../reducers/index';
import * as fromProfile from '../reducers/profile';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {Visitor} from '../models/visitor';
import {ActionStatus} from '../models/action.status';
import {FieldErrorService} from './field-error.service';
import {AuthService} from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class VisitorService {
    private baseUrl = '/api/private/visitors';
    private user$ = this.auth.getUser();

    constructor(private http: HttpClient, private fes: FieldErrorService,
                private store: Store<fromRoot.State>, private auth: AuthService) {
    }

    getVisitor(id?: number | string) {
        return id ? this.http.get<Visitor>(`${this.baseUrl}/${id}`)
                .pipe(map(res => {
                        this.store.dispatch(fromProfile.profile({payload: {registered: res.registered, id: res.id}}));
                        return Visitor.normalizeDates(res);
                    }),
                    catchError(err => this.fes.errorHandler(err)))
            : (this.store.dispatch(fromProfile.profile({
                payload: {
                    registered: false,
                    id: null
                }
            })), this.user$.pipe(map(user => new Visitor(user.roles))));
    }

    save(data: Visitor) {
        return this.http.post(this.baseUrl, data)
            .pipe(map((res: ActionStatus) => res.status ?
                this.fes.success() && Visitor.normalizeDates(res.data)
                : this.fes.errorHandler(res.data)),
                catchError(err => this.fes.errorHandler(err)));
    }

    remove(id: number) {
        return this.http.delete<boolean>(`${this.baseUrl}/${id}`)
            .pipe(catchError(err => this.fes.errorHandler(err)));
    }
}


