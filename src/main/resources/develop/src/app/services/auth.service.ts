import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {EMPTY, Observable, ReplaySubject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {User} from '../models/user';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private user$ = new ReplaySubject<User>(1);

    constructor(private http: HttpClient, private ts: ToastrService) {
        this.load();
    }

    load() {
        this.http.get('/api/me').subscribe((res: User) => this.user$.next(res));
    }

    getUser(): Observable<User> {
        return this.user$.asObservable();
    }

    login(param: { pass: string; user: string }) {
        const data = new HttpParams()
            .set('pass', param.pass)
            .set('user', param.user);
        this.http.post('/api/login', data)
            .pipe(map((res: User) => {
                    this.user$.next(res);
                    if (!res) {
                        this.ts.error('Неправильные логин или пароль');
                    }
                }),
                catchError(err => {
                    console.log(err);
                    return EMPTY;
                })).subscribe();
    }
}

