import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SearchService} from './search.service';
import {VisitorTableRow} from '../models/visitor-table-row';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {ToastrService} from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class VisitorsService extends SearchService<VisitorTableRow> {

    constructor(http: HttpClient, store: Store<fromRoot.State>, toastr: ToastrService) {
        super(http, 'visitors', toastr, {fromDate: null, toDate: null, lname: '', fname: '', mname: '', once: false},
            store);
    }
}
