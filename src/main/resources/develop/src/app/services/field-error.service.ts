import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Observable, of} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class FieldErrorService {
    constructor(private ts: ToastrService) {
    }

    success() {
        this.ts.success('', 'Данные сохранены');
        return true;
    }

    errorHandler(err): Observable<null> | null {
        if (err.error && err.error.message) {
            this.ts.error(err.error.message, err.error.message.startsWith('Скорр') ? '' :
                'Произошла ошибка', {timeOut: 3000});
        } else if (err instanceof Array) {
            this.ts.error('<ul>' + err.map(it => `<li>${it.code}</li>`).join('') + '</ul>', 'Требуется заполнить поля:', {
                timeOut: 4000,
                enableHtml: true,
                positionClass: 'toast-top-center'
            });
            return null;
        } else {
            console.log(err);
        }
        return of(null);
    }
}
