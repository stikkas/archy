import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, of} from 'rxjs';
import {Page} from '../models/page';
import {catchError, map} from 'rxjs/operators';
import {ActionStatus} from '../models/action.status';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import {buttons} from '../reducers/buttons';
import {ToastrService} from 'ngx-toastr';
import {SortEvent} from '../models/sort-event';

export abstract class SearchService<T> {

    private page = 1;
    private sort: SortEvent;
    private page$ = new BehaviorSubject<Page<T>>(null);
    private url: string;
    filter;

    protected constructor(private http: HttpClient, private uri: string,
                          protected tstr: ToastrService,
                          private initFilter: { [key: string]: string | number | boolean | Date },
                          store: Store<fromRoot.State>, private pageSize = 10) {
        this.url = `/api/private/search/${uri}`;
        this.filter = {...this.initFilter};
        store.select(fromRoot.selectUrl).subscribe(u => {
            if (u === '/main') {
                this.clear();
                store.dispatch(buttons([]));
            }
        });
    }

    clear() {
        for (const k in this.initFilter) {
            this.filter[k] = this.initFilter[k];
        }
        this.page$.next(null);
    }

    getPage() {
        return this.page$.asObservable();
    }

    get curPage() {
        return this.page;
    }

    loadPage(page?: number, sort?: SortEvent) {
        const params = {page: `${page || 1}`, size: `${this.pageSize}`} as any;
        for (const k in this.filter) {
            const v = this.filter[k];
            if (v === false || v) {
                if (v instanceof Date) {
                    params[k] = (new Date(v.getTime() - v.getTimezoneOffset() * 60000)).toISOString();
                } else {
                    params[k] = v;
                }
            }
        }

        const ssort = this.sort = sort || (page ? this.sort : undefined);

        if (ssort && ssort.direction) {
            params.sortField = ssort.column;
            params.sortOrder = ssort.direction;
        }

        this.http.get(this.url, {params}).pipe(map((st: ActionStatus) => {
                if (st.status) {
                    this.page$.next(st.data);
                    this.page = st.data.page;
                    if (st.data.data.length === 0 && window.location.pathname === `/${this.uri}`) {
                        this.tstr.info('Ничего не найдено');
                    }
                } else {
                    this.errorHandler(st.data);
                }
            }),
            catchError(err => this.errorHandler(err))
        ).subscribe();
    }

    protected errorHandler(err: string) {
        this.tstr.error(err);
        return of(null);
    }
}
