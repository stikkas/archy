import {Pipe, PipeTransform} from '@angular/core';

export const daysOfWeek = ['', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
export const months = ['января', 'февраля', 'марта', 'арпеля', 'мая', 'июня', 'июля', 'августа', 'сентебря', 'октября',
    'ноября', 'декабря'];

@Pipe({
    name: 'dateTime'
})
export class DateTimePipe implements PipeTransform {

    transform(value: any): string {
        if (!value) {
            return '';
        }
        let d: Date = null;
        if (value instanceof Date) {
            d = value;
        } else {
            const v = Number(value);
            if (!isNaN(v)) {
                d = new Date(v);
            }
        }
        if (!d) {
            return '';
        }
        return `${daysOfWeek[d.getDay()]}, ${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}, ${d.getHours()}:${d.getMinutes()}`;
    }
}

