import {Component, ViewEncapsulation} from '@angular/core';
import * as fromRoot from './reducers';
import {Store} from '@ngrx/store';
import {AuthService} from './services/auth.service';

@Component({
    selector: 'aisrr-root',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    url$ = this.store.select(fromRoot.selectUrl);
    user$ = this.authService.getUser();

    constructor(private store: Store<fromRoot.State>,
                private authService: AuthService) {
    }

}

