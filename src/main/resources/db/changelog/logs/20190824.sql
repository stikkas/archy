--liquibase formatted sql

--changeset angelog:1
alter table visitor add column registered boolean not null default false;
