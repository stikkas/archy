--liquibase formatted sql

--changeset angelog:1
drop table store_department cascade;

--changeset angelog:2
create table archive (
    id serial primary key,
    name varchar(100),
    short_name varchar(50)
);

--changeset angelog:3 context:develop
insert into archive(id, name, short_name) values
(1,	'Отдел хранения документов до 1917 г.', 'ОХД до 1917 г.'),
(2,	'Отдел хранения документов после 1917 г.', 'ОХД после 1917 г.'),
(3,	'Отдел хранения научно-технической документации Москвы', 'ОХНТДМ'),
(4,	'Отдел хранения документов общественно-политической истории Москвы', 'ОХДОПИМ'),
(6,	'Отдел хранения документов личных собраний Москвы', 'ОХДЛСМ'),
(7,	'Отдел хранения аудиовизуальных документов', 'ОХАД');

--changeset angelog:4
drop index if exists profile_store_department_id_idx;
alter table profile rename store_department_id to archive_id;
CREATE INDEX ON profile(archive_id);
