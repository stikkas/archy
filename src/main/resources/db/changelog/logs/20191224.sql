--liquibase formatted sql

--changeset stikkas:1
alter table visitor add column archives int[];
alter table visitor drop column archive_id cascade;

