--liquibase formatted sql

--changeset angelog:1
create table education (
    id serial primary key,
    name varchar(200)
);

--changeset angelog:2
create table science_rank (
    id serial primary key,
    name varchar(200)
);

--changeset angelog:3 context:develop
insert into education(id, name) values
(1,	'Высшее'),
(2,	'Неоконченное высшее'),
(3,	'Среднее'),
(4,	'Среднее специальное'),
(6,	'Институт благородных девиц'),
(7,	'Три класса церковно-приходской школы');

--changeset angelog:4 context:develop
insert into science_rank(id, name) values
(1,	'Помощник доцента'),
(2,	'Аспирант'),
(3,	'Доцент'),
(4,	'Кандидат технический наук'),
(6,	'Доктор технических наук'),
(7,	'Профессор');


