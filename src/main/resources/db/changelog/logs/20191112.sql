--liquibase formatted sql

--changeset stikkas:1
alter table arch_record drop column rereg_date;
alter table research_topic add column start_date date;
alter table research_topic add column end_date date;
alter table research_topic drop column profile_id cascade;
alter table research_topic add arch_record_id bigint constraint research_topic_arch_record_idx references arch_record(id);

