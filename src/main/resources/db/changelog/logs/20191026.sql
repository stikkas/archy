--liquibase formatted sql

--changeset stikkas:1
drop table reading_room;

create table reading_room (
   id serial not null primary key,
   rr integer not null,
   role integer not null,
   address varchar(255)
);

CREATE UNIQUE INDEX ON reading_room(rr);

insert into reading_room(rr, address, role)
values(1, 'ул. Профсоюзная, 80', 10), (2, 'ул. Профсоюзная, 82', 11), (3, 'ул. Международная, 10', 12);

