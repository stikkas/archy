--liquibase formatted sql

--changeset stikkas:1
create table booking (
-- идентификатор
    id bigserial not null primary key,
-- сотрудник читального зала, принявший заказ
    registrator_id bigint references account(id),
-- начальник отдела хранения
    chief_archive_id bigint references account(id),
-- хранитель заполняет вкладку хранитель
    keeper_id bigint references account(id),
-- исполнитель
    executor_id bigint references account(id),
-- дата приема заказа
    reg_date timestamp without time zone,
-- дата выдачи заказа
    issue_date timestamp without time zone,
-- дата возврата (фактическая)
    return_date timestamp without time zone,
-- контрольная дата возврата
    control_return_date timestamp without time zone,
-- Причина
    cause varchar(100),
-- Номер заказа
    nymber bigint not null,
-- статус заказа
    state integer,
-- тип заказа
    order_type integer,
-- лицо делающее заказ (не сотрудник архивной системы)
    applicant_id bigint references profile(account_id),
-- отдел хранения
    archive_id integer,
-- тема исследования
    topic_heading_id integer references topic_heading(id),
-- разрешение на выдачу
    permission_issue integer,
-- причина отказа
    reject_reason integer,
-- дата разрешения / отказа
    permission_date timestamp without time zone,
-- изменение сроков выдачи
    change_issue_date timestamp without time zone,
-- причина (изменения сроков выдачи)
    change_issue_reason varchar(100),
-- номер хранилища
    store_id integer,
-- читальный зал
    reading_room integer
);

comment on table booking is 'Данные заказа документов на выдачу';
comment on column booking.id is 'идентификатор';
comment on column booking.registrator_id is 'сотрудник читального зала, принявший заказ';
comment on column booking.chief_archive_id is 'начальник отдела хранения';
comment on column booking.keeper_id is 'хранитель заполняет вкладку хранитель';
comment on column booking.executor_id is 'исполнитель';
comment on column booking.reg_date is 'дата приема заказа';
comment on column booking.issue_date is 'дата выдачи заказа';
comment on column booking.return_date is 'дата возврата (фактическая)';
comment on column booking.control_return_date is 'контрольная дата возврата';
comment on column booking.cause is 'Причина возврата';
comment on column booking.nymber is 'Номер заказа';
comment on column booking.state is 'статус заказа';
comment on column booking.order_type is 'тип заказа';
comment on column booking.applicant_id is 'лицо делающее заказ (не сотрудник архивной системы)';
comment on column booking.archive_id is 'отдел хранения';
comment on column booking.topic_heading_id is 'тема исследования';
comment on column booking.permission_issue is 'разрешение на выдачу';
comment on column booking.reject_reason is 'причина отказа';
comment on column booking.permission_date is 'дата разрешения / отказа';
comment on column booking.change_issue_date is 'изменение сроков выдачи';
comment on column booking.change_issue_reason is 'причина (изменения сроков выдачи)';
comment on column booking.store_id is 'номер хранилища';
comment on column booking.reading_room is 'читальный зал';

--changeset stikkas:2 splitStatements:false
CREATE OR REPLACE FUNCTION calc_booking_nymber() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
    select coalesce(max(nymber), 0) + 1 from booking into NEW.nymber;
    RETURN NEW;
    END;
$$;
CREATE TRIGGER booking_nymber BEFORE INSERT ON booking FOR EACH ROW EXECUTE PROCEDURE calc_booking_nymber();

--changeset stikkas:3
create table document_case (
-- идентификатор
    id bigserial not null primary key,
-- идентификатор заказа
    booking_id bigint references booking(id),
-- номер фонда
    fund_number varchar(15),
-- номер описи
    opis_number varchar(15),
-- номер единицы хранения дела
    unit_number varchar(15),
-- заголовок единицы хранения
    unit_title varchar(50),
-- количество листов
    pages integer,
-- дело подготовлено
    prepared boolean,
-- причина отказа
    reject_reason integer,
-- выдано в ЧЗ
    issue_rr_date timestamp without time zone,
-- списано
    spisano_date timestamp without time zone,
-- возвращено в архивохранилище
    return_date timestamp without time zone
);

comment on table document_case is 'Документы заказа';
comment on column document_case.id is 'идентификатор';
comment on column document_case.booking_id is 'идентификатор заказа';
comment on column document_case.fund_number is 'номер фонда';
comment on column document_case.opis_number is 'номер описи';
comment on column document_case.unit_number is 'номер единицы хранения дела';
comment on column document_case.unit_title is 'заголовок единицы хранения';
comment on column document_case.pages is 'количество листов';
comment on column document_case.prepared is 'дело подготовлено';
comment on column document_case.reject_reason is 'причина отказа';
comment on column document_case.issue_rr_date is 'выдано в ЧЗ';
comment on column document_case.spisano_date is 'списано';
comment on column document_case.return_date is 'возвращено в архивохранилище';

--changeset stikkas:4
create table holiday (
    id bigserial not null primary key,
    year integer not null,
    month integer not null,
    days int[]
);

create index on holiday(year, month);

