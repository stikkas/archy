--liquibase formatted sql

--changeset stikkas:1
alter table booking drop column reject_reason_id cascade;
alter table booking add column reject_reason varchar(100);
