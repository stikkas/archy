--liquibase formatted sql

--changeset stikkas:1
alter table account add archive_id int constraint account_archive_idx references archive(id);
