--liquibase formatted sql

--changeset stikkas:1
update access_group set roles = array_replace(roles, 14, 16) where 14 = any(roles);

alter table account add storage_id int constraint account_storage_idx references storage(id);
