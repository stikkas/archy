--liquibase formatted sql

--changeset stikkas:1
create table month (
    id int not null primary key,
    name varchar(8)
);

--changeset stikkas:2
insert into month(id, name) values
(0, 'Январь'),
(1, 'Февраль'),
(2, 'Март'),
(3, 'Апрель'),
(4, 'Май'),
(5, 'Июнь'),
(6, 'Июль'),
(7, 'Август'),
(8, 'Сентябрь'),
(9, 'Октябрь'),
(10, 'Ноябрь'),
(11, 'Декабрь');

--changeset stikkas:3
alter table holiday add CONSTRAINT holiday_month_fk FOREIGN KEY (month) REFERENCES month(id);

