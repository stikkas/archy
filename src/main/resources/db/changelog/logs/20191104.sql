--liquibase formatted sql

--changeset stikkas:4
alter table holiday add column rrs int[];
drop index if exists holiday_year_month_idx;
create index holiday_year_month_idx on holiday(year, month);

