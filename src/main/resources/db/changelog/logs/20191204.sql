--liquibase formatted sql

--changeset stikkas:1
alter table booking add column issue_rr_date timestamp without time zone;
alter table document_case drop column issue_rr_date cascade;
