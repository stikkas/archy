--liquibase formatted sql

--changeset stikkas:1
alter table profile add column client_ids bigint[];
create index on profile(client_ids);
comment on column profile.client_ids is 'идентификаторы из старой базы Clients.lkod';

