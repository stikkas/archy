--liquibase formatted sql

--changeset stikkas:1
delete from reading_room;
alter table reading_room add role integer not null;
insert into reading_room(id, address, role) values(1, 'ул. Профсоюзная, 80', 10), (2, 'ул. Профсоюзная, 82', 11), (3, 'ул. Международная, 10', 12);

