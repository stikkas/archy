--liquibase formatted sql

--changeset stikkas:1
alter table account add last_name varchar(50), add first_name varchar(50), add middle_name varchar(50),
    drop fio cascade;
CREATE INDEX ON account USING gin (last_name gin_trgm_ops);
CREATE INDEX ON account USING gin (first_name gin_trgm_ops);
CREATE INDEX ON account USING gin (middle_name gin_trgm_ops);

--changeset stikkas:2
alter table profile drop last_name cascade, drop first_name cascade, drop middle_name cascade;

--changeset stikkas:3
delete from profile;
delete from arch_record;
delete from research_topic;

--changeset stikkas:4
alter table profile drop id cascade, add primary key (account_id);

