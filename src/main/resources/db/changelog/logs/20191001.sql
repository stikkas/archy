--liquibase formatted sql

--changeset stikkas:1
create table access_group (
    id serial not null primary key,
    name varchar(150),
    description varchar(255),
    roles int[]
);

--changeset stikkas:2
insert into access_group(id, name, description, roles) values(1, 'Администратор', 'Пользователь, обладающий правами администрирования системы', '{1}'::int[]);
select setval('access_group_id_seq', 1, true);

--changeset stikkas:3
create table account (
    id bigserial not null primary key,
    login varchar(50),
    password varchar(60),
    groups int[],
    fio varchar(30)
);

create unique index on account(login);
create index account_login_s_idx on account using gin (login gin_trgm_ops);

--changeset stikkas:4
insert into account(fio, login, password, groups)
values('Иванов И.И.', 'admin', '$2a$10$Y54/19GClhcoU1Sg6dWb1eCoWWSObdFFs96pOsmpepklnuDUz1Fw2', -- 654321
 '{1}'::int[]);

--changeset stikkas:5
alter table profile add account_id bigint;
alter table visitor add profile_id bigint;
create index on profile(account_id);
create index on visitor(profile_id);
