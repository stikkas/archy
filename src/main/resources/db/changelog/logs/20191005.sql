--liquibase formatted sql

--changeset stikkas:1
create table reading_room (
   id integer not null primary key,
   address varchar(255)
);

--changeset stikkas:2
insert into reading_room(id, address) values(1, 'ул. Профсоюзная, 80'), (2, 'ул. Профсоюзная, 82'), (3, 'ул. Международная, 10');
