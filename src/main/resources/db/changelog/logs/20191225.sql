--liquibase formatted sql

--changeset stikkas:1
alter table booking add column issue_reading_room integer;

comment on column booking.issue_reading_room is 'читальный зал выдачи заказа';
