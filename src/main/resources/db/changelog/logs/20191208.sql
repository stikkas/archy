--liquibase formatted sql

--changeset stikkas:1
alter table booking drop column topic_heading_id cascade;
alter table booking add topic_heading_id bigint constraint booking_topic_heading_id_fkey references research_topic(id);

