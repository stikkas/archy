--liquibase formatted sql

--changeset angelog:1
create table topic_heading (
    id serial primary key,
    name varchar(200)
);

--changeset angelog:2 context:develop
insert into topic_heading(id, name) values
(1,	'Мировая история'),
(2,	'Древний Рим'),
(3,	'Византийское царство'),
(4,	'Русь языческая'),
(6,	'Дело большевиков'),
(7,	'Троцкисты, эссеры');

