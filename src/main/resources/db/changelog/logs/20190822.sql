--liquibase formatted sql

--changeset angelog:1
alter table profile add column change_fio varchar(50);
alter table profile add column country_id integer;
alter table profile add column person_num integer;
alter table profile add column first_reg_date date;
alter table profile add column issue_pass_date date;
alter table profile add column ban boolean not null default false;
alter table profile add column ban_cause varchar(120);
alter table profile add column organization varchar(200);
alter table profile add column position varchar(120);
alter table profile add column education_id integer;
alter table profile add column science_rank_id integer;
alter table profile add column remark text;
alter table profile add column old_numbers text;
alter table profile add column email varchar(120);
alter table profile add column registered boolean not null default false;
alter table profile drop personal_number cascade;
alter table profile drop archive_id cascade;

CREATE INDEX ON profile(country_id);
CREATE INDEX ON profile(education_id);
CREATE INDEX ON profile(science_rank_id);
CREATE UNIQUE INDEX ON profile(person_num);

--changeset angelog:2
create table research_topic (
    id bigserial primary key,
    profile_id bigint not null,
    topic_id integer,
    theme_name text,
    period varchar(100),
    target text
);

CREATE INDEX ON research_topic(profile_id);
CREATE INDEX ON research_topic(topic_id);

--changeset angelog:3
create table arch_record (
    id bigserial primary key,
    profile_id bigint not null,
    archive_id integer,
    person_num varchar(15),
    approve_date date,
    rereg_date date
);

CREATE INDEX ON arch_record(profile_id);
CREATE INDEX ON arch_record(archive_id);

--changeset angelog:4  splitStatements:false
CREATE OR REPLACE FUNCTION calc_person_num() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
    select coalesce(max(person_num), 0) + 1 from profile into NEW.person_num;
    RETURN NEW;
    END;
$$;
CREATE TRIGGER profile_person_num BEFORE INSERT ON profile FOR EACH ROW EXECUTE PROCEDURE calc_person_num();

--changeset angelog:5 context:develop
delete from profile;
insert into profile(last_name, first_name, middle_name, birth_year) values
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978),
('Мотылькова', 'Марина', 'Васильевна', 1968),
('Кошелькова', 'Ирина', 'Сергеевна', 1975),
('Бабочкина', 'Полина', 'Александровна', 1965),
('Собашкина', 'Викторина', 'Петровна', 1980),
('Кубарев', 'Иван', 'Петрович', 1958),
('Михайлов', 'Юрий', 'Муслимович', 1985),
('Лизгунов', 'Олег', 'Федорович', 1972),
('Кононова', 'Елезовета', 'Викторовна', 1983),
('Серебрин', 'Алексей', 'Владимирович', 1978);
