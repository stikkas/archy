--liquibase formatted sql

--changeset stikkas:1
create table reject_reason (
    id serial primary key,
    name varchar(50),
    code varchar(30)
);

insert into reject_reason (name) values
('Гостайна'),
('Персонал. данные'),
('Неудовлет. cост.'),
('Огран. доступа'),
('Превыш. листажа'),
('На реставрации'),
('На сканир./микроф.'),
('На руках'),
('Наруш. срока повтор.'),
('Невоз. повтор.выдачи'),
('Нет разреш. УПДК'),
('Нет разреш .фондосдат.'),
('Выбыло'),
('В розыске'),
('Отсутствует'),
('Уничтожено'),
('Не соот.стр/ад.'),
('Наруш. ст.16');

comment on table reject_reason is 'Справочник Причины отказа';
comment on column reject_reason.id is 'идентификатор';
comment on column reject_reason.name is 'значение';
comment on column reject_reason.code is 'код';

--changeset stikkas:2
alter table booking drop column reject_reason cascade;
alter table document_case drop column reject_reason cascade;

alter table booking add reject_reason_id int constraint booking_reject_reason_idx references reject_reason(id);
alter table document_case add reject_reason_id int constraint document_case_reject_reason_idx references reject_reason(id);
