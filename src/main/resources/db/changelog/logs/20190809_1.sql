--liquibase formatted sql

--changeset angelog:1
create table profile (
    id bigserial primary key,
    last_name varchar(50),
    first_name varchar(50),
    middle_name varchar(50),
    personal_number varchar(50),
    birth_year smallint,
    store_department_id integer
);

CREATE INDEX ON profile USING gin (last_name gin_trgm_ops);
CREATE INDEX ON profile USING gin (first_name gin_trgm_ops);
CREATE INDEX ON profile USING gin (middle_name gin_trgm_ops);
CREATE INDEX ON profile(personal_number);
CREATE INDEX ON profile(birth_year);
CREATE INDEX ON profile(store_department_id);

--changeset angelog:2
create table store_department (
    id serial primary key,
    name varchar(150)
);

