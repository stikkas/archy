--liquibase formatted sql

--changeset stikkas:1
drop index if exists holiday_year_month_idx;
create unique index holiday_year_month_idx on holiday(year, month);

