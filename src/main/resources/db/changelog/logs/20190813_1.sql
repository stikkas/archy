--liquibase formatted sql

--changeset angelog:1
create table country (
    id serial primary key,
    name varchar(100)
);

--changeset angelog:2 context:develop
insert into country(id, name) values
(1, 'Россия'), (2, 'Белоруссия'), (3, 'Украина'), (4, 'Франция'), (5, 'Германия'), (6, 'Австрия'), (7, 'Польша'),
(8, 'Финляндия'), (9, 'Эстония');
