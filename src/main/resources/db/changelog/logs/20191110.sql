--liquibase formatted sql
--changeset stikkas:1  splitStatements:false
CREATE OR REPLACE FUNCTION calc_person_num() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
    IF NEW.person_num is null THEN
        select coalesce(max(person_num), 0) + 1 from profile into NEW.person_num;
    END IF;
    RETURN NEW;
    END;
$$;
