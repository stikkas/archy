package ru.insoft.aisrr.exception

class ViolationException(msg: String) : RuntimeException(msg)
