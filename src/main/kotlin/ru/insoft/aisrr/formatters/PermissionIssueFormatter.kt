package ru.insoft.aisrr.formatters

import ru.insoft.aisrr.enums.PermissionIssue

class PermissionIssueFormatter : AbstractEnumFormatter<PermissionIssue>(PermissionIssue.ALLOWED)
