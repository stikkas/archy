package ru.insoft.aisrr.formatters

import mu.KotlinLogging
import org.springframework.format.Formatter
import ru.insoft.aisrr.enums.IntId
import java.io.IOException
import java.text.ParseException
import java.util.*

private val logger = KotlinLogging.logger { }

open class AbstractEnumFormatter<T : IntId>(private val example: T) : Formatter<T> {

    override fun print(obj: T, locale: Locale): String = obj.toString()

    override fun parse(json: String, locale: Locale): T =
            try {
                example.codes().firstOrNull { it.code == json } as T
            } catch (ex: IOException) {
                logger.error(ex) { example::class.java.name + "Formatter" }
                throw ParseException(ex.message, 0)
            }
}
