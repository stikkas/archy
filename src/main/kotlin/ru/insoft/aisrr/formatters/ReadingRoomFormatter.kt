package ru.insoft.aisrr.formatters

import ru.insoft.aisrr.enums.ReadingRoom

class ReadingRoomFormatter : AbstractEnumFormatter<ReadingRoom>(ReadingRoom.RR1)
