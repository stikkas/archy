package ru.insoft.aisrr.formatters

import ru.insoft.aisrr.enums.OrderType

class OrderTypeFormatter : AbstractEnumFormatter<OrderType>(OrderType.FP)
