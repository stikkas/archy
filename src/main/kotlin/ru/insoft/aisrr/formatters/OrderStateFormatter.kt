package ru.insoft.aisrr.formatters

import ru.insoft.aisrr.enums.OrderState

class OrderStateFormatter : AbstractEnumFormatter<OrderState>(OrderState.ACCEPT)
