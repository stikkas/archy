package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserDTO {
    var id: Long? = null
    var firstName: String = ""
    var lastName: String = ""
    var middleName: String = ""
    lateinit var login: String
    lateinit var password: String
    var groups: List<Int>? = null
    var archiveId: Int? = null
    var storageId: Int? = null
}

