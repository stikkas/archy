package ru.insoft.aisrr.admin.models

class UserRecord {
    var id: Long = 0
    var lastName: String? = null
    var firstName: String? = null
    var middleName: String? = null
    var login: String? = null
    var groups: List<String>? = null
}
