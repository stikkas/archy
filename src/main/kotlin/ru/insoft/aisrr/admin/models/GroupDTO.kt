package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import ru.insoft.aisrr.admin.serialize.RoleDeserializer
import ru.insoft.aisrr.enums.Role

class GroupDTO {
    var id: Long? = null
    var description: String? = null
    lateinit var name: String
    @JsonDeserialize(using = RoleDeserializer::class)
    var roles: List<Role>? = null
}

