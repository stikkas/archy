package ru.insoft.aisrr.admin.models

import ru.insoft.aisrr.enums.ReadingRoom
import java.net.URLDecoder

class Criteria {
    var query: String? = null
        set(value) {
            val rrs = ArrayList<ReadingRoom>()
            var strings = ArrayList<String>()
            value?.let { URLDecoder.decode(it, "UTF-8").trim() }?.split(Regex("[\\s,;]"))?.forEach {
                if (!it.isNullOrBlank()) {
                    val num = it.toLongOrNull()
                    if (num != null) {
                        id = num
                    } else {
                        val code = it.toUpperCase();
                        val rr = ReadingRoom.RR1.codes().firstOrNull { r -> r.code == code }
                        if (rr != null) {
                            rrs.add(rr)
                        } else {
                            strings.add(it)
                        }
                    }

                    if (rrs.isNotEmpty()) {
                        this.rrs = rrs;
                    }
                    if (strings.isNotEmpty()) {
                        field = strings.joinToString(" ")
                    }
                }
            }
        }

    var id: Long? = null
        private set

    var rrs: List<ReadingRoom>? = null
        private set

    var sortField: String? = null
    var sortOrder: String? = null
}
