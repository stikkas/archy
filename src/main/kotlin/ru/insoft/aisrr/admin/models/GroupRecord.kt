package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.aisrr.enums.Role

class GroupRecord {
    var id: Long = 0
    var name: String? = null
    var description: String? = null
    @JsonIgnore
    var roles: List<Role>? = null

    @JsonProperty("roles")
    fun getRolesNames() = roles?.map { it.label }
}
