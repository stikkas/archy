package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.aisrr.enums.ReadingRoom

class DictStorageEntity : DictEntity() {
    var rr: ReadingRoom? = null
    var archiveId: Int? = null
    var archive: String? = null
    @JsonProperty("rrStr")
    fun rrStr() = rr?.label
}
