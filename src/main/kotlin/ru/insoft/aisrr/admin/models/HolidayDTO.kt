package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import ru.insoft.aisrr.admin.serialize.ListIntDeserializer
import ru.insoft.aisrr.admin.serialize.ReadingRoomDeserializer
import ru.insoft.aisrr.enums.ReadingRoom

class HolidayDTO {
    var id: Long? = null
    var year: Int? = null
    var month: Int? = null
    @JsonDeserialize(using = ListIntDeserializer::class)
    var days: List<Int>? = null

    @JsonDeserialize(using = ReadingRoomDeserializer::class)
    var rr: List<ReadingRoom>? = null

}

