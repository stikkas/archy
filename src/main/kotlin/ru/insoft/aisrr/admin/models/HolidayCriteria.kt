package ru.insoft.aisrr.admin.models

import ru.insoft.aisrr.enums.ReadingRoom
import java.net.URLDecoder

class HolidayCriteria {
    var query: String? = null
        set(value) {
            val months = ArrayList<String>()
            val rrs = ArrayList<ReadingRoom>()
            val years = ArrayList<Int>()
            val days = ArrayList<Int>()
            value?.let { URLDecoder.decode(it, "UTF-8").trim() }?.split(Regex("[\\s,;]"))?.forEach {
                if (!it.isNullOrBlank()) {
                    val num = it.toIntOrNull()
                    if (num != null) {
                        if (num > 0) {
                            if (num > 31) {
                                years.add(num)
                            } else {
                                days.add(num)
                            }
                        }
                    } else {
                        val code = it.toUpperCase();
                        val rr = ReadingRoom.RR1.codes().firstOrNull { r -> r.code == code }
                        if (rr != null) {
                            rrs.add(rr)
                        } else {
                            months.add(it)
                        }
                    }
                }
            }
            if (months.isNotEmpty()) {
                this.months = months;
            }
            if (years.isNotEmpty()) {
                this.years = years;
            }
            if (days.isNotEmpty()) {
                this.days = days;
            }
            if (rrs.isNotEmpty()) {
                this.rrs = rrs;
            }
        }

    var months: List<String>? = null
        private set

    var years: List<Int>? = null
        private set

    var days: List<Int>? = null
        private set

    var rrs: List<ReadingRoom>? = null
        private set

    var sortField: String? = null
    var sortOrder: String? = null
}

