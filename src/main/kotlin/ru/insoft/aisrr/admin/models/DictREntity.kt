package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.enums.Role

class DictREntity : DictEntity() {
    var rr: ReadingRoom? = null
    var role: Role? = null
    @JsonProperty("rrStr")
    fun rrStr() = rr?.label

    @JsonProperty("roleStr")
    fun roleStr() = role?.label
}
