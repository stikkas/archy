package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.aisrr.enums.ReadingRoom

class HolidayRecord {
    var id: Long = 0
    var year: Int? = null
    var month: String? = null

    @JsonIgnore
    var days: List<Int>? = null

    var rr: List<ReadingRoom>? = null

    @JsonProperty("days")
    fun getRightDays() = days?.filter { it in 1..31 }

    @JsonProperty("rrStr")
    fun rrStr() = rr?.let {
        if (it.isNotEmpty()) {
            it.joinToString { r -> r.label }
        } else {
            ""
        }
    } ?: ""
}

