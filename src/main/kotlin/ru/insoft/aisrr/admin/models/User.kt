package ru.insoft.aisrr.admin.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.security.core.userdetails.UserDetails
import ru.insoft.aisrr.enums.Role

/**
 * Пользователь системы
 */
class User : UserDetails {
    var id: Long? = null
    var firstName: String? = null
    var lastName: String? = null
    var middleName: String? = null
    var login: String? = null
        get() = field?.trim()?.toLowerCase()
    @JsonIgnore
    var pass: String? = null
    @JsonIgnore
    var roles: List<Role>? = null

    var rraddress: String? = null
    var archiveId: Int? = null
    var storageId: Int? = null

    @JsonProperty("roles")
    override fun getAuthorities() = roles?.toMutableList() ?: mutableListOf()

    @JsonProperty("login")
    override fun getUsername() = login

    @JsonIgnore
    override fun getPassword() = pass

    @JsonIgnore
    override fun isEnabled() = true

    @JsonIgnore
    override fun isCredentialsNonExpired() = true

    @JsonIgnore
    override fun isAccountNonExpired() = true

    @JsonIgnore
    override fun isAccountNonLocked() = true
}
