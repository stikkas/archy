package ru.insoft.aisrr.admin.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.handlers.AbstractEnumHandler

@MappedTypes(Role::class)
class RoleHandler : AbstractEnumHandler<Role>(Role.RR1)

