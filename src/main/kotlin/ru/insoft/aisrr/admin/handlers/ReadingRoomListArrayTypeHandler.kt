package ru.insoft.aisrr.admin.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.ReadingRoom

@MappedTypes(List::class)
class ReadingRoomListArrayTypeHandler : AbstractIntIdListArrayTypeHandler<ReadingRoom>(ReadingRoom.RR1)

