package ru.insoft.aisrr.admin.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import ru.insoft.aisrr.enums.IntId
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

abstract class AbstractIntIdListArrayTypeHandler<T : IntId>(private val ex: T) : BaseTypeHandler<List<T>>() {
    override fun getNullableResult(p0: ResultSet?, p1: String?): List<T>? =
            p0?.getArray(p1)?.let {
                it.array?.let { a ->
                    (a as Array<Int>).mapNotNull { i -> ex.codes().find { it.id == i } as? T }
                }
            }

    override fun getNullableResult(p0: ResultSet?, p1: Int): List<T>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getNullableResult(p0: CallableStatement?, p1: Int): List<T>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setNonNullParameter(p0: PreparedStatement?, p1: Int, p2: List<T>?, p3: JdbcType?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

