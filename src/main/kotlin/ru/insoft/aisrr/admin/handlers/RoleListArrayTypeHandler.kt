package ru.insoft.aisrr.admin.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.Role

@MappedTypes(List::class)
class RoleListArrayTypeHandler : AbstractIntIdListArrayTypeHandler<Role>(Role.ADMIN)
