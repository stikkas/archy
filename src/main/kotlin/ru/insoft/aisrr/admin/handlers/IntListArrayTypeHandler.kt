package ru.insoft.aisrr.admin.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import org.apache.ibatis.type.MappedTypes
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet

@MappedTypes(List::class)
class IntListArrayTypeHandler : BaseTypeHandler<List<Int>>() {
    override fun getNullableResult(p0: ResultSet?, p1: String?): List<Int>? =
            p0?.getArray(p1)?.let {
                (it.array as Array<Int>).map { i -> i }
            }

    override fun getNullableResult(p0: ResultSet?, p1: Int): List<Int>? =
            p0?.getArray(p1)?.let {
                (it.array as Array<Int>).map { i -> i }
            }

    override fun getNullableResult(p0: CallableStatement?, p1: Int): List<Int>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setNonNullParameter(p0: PreparedStatement?, p1: Int, p2: List<Int>?, p3: JdbcType?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

