package ru.insoft.aisrr.admin.serialize

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

class ListIntDeserializer : JsonDeserializer<List<Int>>() {
    override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): List<Int>? {
        return parser.text.split(Regex("[,\\s;]")).map {
            it.toIntOrNull()?.let { i ->
                if (i in 1..31) {
                    i
                } else {
                    null
                }
            }
        }.filterNotNull().let {
            if (it.isNotEmpty()) {
                it
            } else {
                null
            }
        }
    }
}
