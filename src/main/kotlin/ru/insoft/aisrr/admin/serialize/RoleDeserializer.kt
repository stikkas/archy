package ru.insoft.aisrr.admin.serialize

import ru.insoft.aisrr.enums.Role

class RoleDeserializer : AbstractIntIdListDeserializer<Role>(Role.ADMIN)
