package ru.insoft.aisrr.admin.serialize

import ru.insoft.aisrr.enums.ReadingRoom

class ReadingRoomDeserializer : AbstractIntIdListDeserializer<ReadingRoom>(ReadingRoom.RR1)
