package ru.insoft.aisrr.admin.serialize

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import ru.insoft.aisrr.enums.IntId


open class AbstractIntIdListDeserializer<T : IntId>(private val ex: T) : JsonDeserializer<List<T>>() {

    private val om = ObjectMapper()

    override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): List<T>? {
        val items = ArrayList<T>()
        if (parser.text.isNullOrBlank()) {
            return null
        }
        om.readValue(parser, List::class.java).map { r ->
            ex.codes().firstOrNull { it.code == r }?.let {
                items.add(it as T)
            }
        }
        return items;
    }
}

