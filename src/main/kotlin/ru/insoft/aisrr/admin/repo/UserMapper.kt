package ru.insoft.aisrr.admin.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.User
import ru.insoft.aisrr.admin.models.UserDTO
import ru.insoft.aisrr.admin.models.UserRecord
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.models.Dict

@Mapper
interface UserMapper {

    fun findByLogin(login: String): User?

    fun existsLogin(login: String): Boolean

    fun findById(id: Long): UserDTO?

    fun insert(user: UserDTO)

    fun update(user: UserDTO)

    fun remove(id: Long)

    fun dict(@Param("role") role: Role): List<Dict<Long>>

    fun count(@Param("filter") filter: Criteria): Long

    fun findAll(@Param("filter") filter: Criteria, @Param("offset") offset: Long,
                @Param("limit") limit: Int): List<UserRecord>

    fun changePassword(@Param("id") id: Long, @Param("password") password: String)
}
