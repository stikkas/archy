package ru.insoft.aisrr.admin.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.admin.models.HolidayCriteria
import ru.insoft.aisrr.admin.models.HolidayDTO
import ru.insoft.aisrr.admin.models.HolidayRecord

@Mapper
interface HolidayMapper {

    fun insert(holiday: HolidayDTO)

    fun update(holiday: HolidayDTO)

    fun remove(id: Long)

    fun count(@Param("filter") filter: HolidayCriteria): Long

    fun findAll(@Param("filter") filter: HolidayCriteria, @Param("offset") offset: Long,
                @Param("limit") limit: Int): List<HolidayRecord>

}

