package ru.insoft.aisrr.admin.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.GroupDTO
import ru.insoft.aisrr.admin.models.GroupRecord
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.models.Dict

@Mapper
interface GroupMapper {
    fun findById(id: Int): GroupDTO?

    fun insert(group: GroupDTO)

    fun update(group: GroupDTO)

    fun remove(id: Int)
    fun dict(): List<Dict<Int>>
    fun count(@Param("filter") filter: Criteria): Long

    fun findAll(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                @Param("limit") limit: Int): List<GroupRecord>

    fun hasRole(@Param("groups") groups: List<Int>, @Param("role") role: Role): Boolean
    fun hasGroup(@Param("role") role: Role, @Param("id") id: Int?): Boolean
    fun hasAnyGroup(@Param("groups") groups: List<Int>, @Param("roles") roles: List<Role>): Boolean
}
