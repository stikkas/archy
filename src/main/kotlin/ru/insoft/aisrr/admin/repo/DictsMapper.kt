package ru.insoft.aisrr.admin.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.admin.models.*
import ru.insoft.aisrr.admin.models.DictSEntity

@Mapper
interface DictsMapper {

    fun findArchives(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                     @Param("limit") limit: Int): List<DictSEntity>

    fun countArchives(@Param("filter") filter: Criteria): Long
    fun insertArchive(entity: DictSEntity)
    fun updateArchive(entity: DictSEntity)
    fun removeArchive(id: Int)

    fun findCountries(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                      @Param("limit") limit: Int): List<DictEntity>

    fun countCountries(@Param("filter") filter: Criteria): Long
    fun insertCountry(entity: DictEntity)
    fun updateCountry(entity: DictEntity)
    fun removeCountry(id: Int)

    fun findRejectReasons(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                      @Param("limit") limit: Int): List<DictEntity>

    fun countRejectReasons(@Param("filter") filter: Criteria): Long
    fun insertRejectReason(entity: DictEntity)
    fun updateRejectReason(entity: DictEntity)
    fun removeRejectReason(id: Int)

    fun findStorages(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                     @Param("limit") limit: Int): List<DictStorageEntity>

    fun countStorages(@Param("filter") filter: Criteria): Long
    fun insertStorage(entity: DictStorageEntity)
    fun updateStorage(entity: DictStorageEntity)
    fun removeStorage(id: Int)

    fun findScienceRanks(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                         @Param("limit") limit: Int): List<DictEntity>

    fun countScienceRanks(@Param("filter") filter: Criteria): Long
    fun insertScienceRank(entity: DictEntity)
    fun updateScienceRank(entity: DictEntity)
    fun removeScienceRank(id: Int)

    fun findEducations(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                       @Param("limit") limit: Int): List<DictEntity>

    fun countEducations(@Param("filter") filter: Criteria): Long
    fun insertEducation(entity: DictEntity)
    fun updateEducation(entity: DictEntity)
    fun removeEducation(id: Int)

    fun findTopicHeadings(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                          @Param("limit") limit: Int): List<DictEntity>

    fun countTopicHeadings(@Param("filter") filter: Criteria): Long
    fun insertTopicHeading(entity: DictEntity)
    fun updateTopicHeading(entity: DictEntity)
    fun removeTopicHeading(id: Int)

    fun findRrDetails(@Param("filter") filter: Criteria, @Param("offset") offset: Int,
                      @Param("limit") limit: Int): List<DictREntity>

    fun countRrDetails(@Param("filter") filter: Criteria): Long
    fun insertRrDetail(entity: DictREntity)
    fun updateRrDetail(entity: DictREntity)
    fun removeRrDetail(id: Int)
}

