package ru.insoft.aisrr.admin.controllers

import mu.KotlinLogging
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.DictEntity
import ru.insoft.aisrr.admin.service.DictsService
import ru.insoft.aisrr.controllers.ActionStatus
import ru.insoft.aisrr.controllers.Urls

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping(DictsUrls.SCIENCERANK)
class DictsScienceRankController(private val dictsService: DictsService) {

    @PostMapping
    fun save(@RequestBody entity: DictEntity): ActionStatus<Unit> {
        dictsService.saveScienceRank(entity)
        return ActionStatus.success()
    }

    @DeleteMapping(Urls.ID)
    fun remove(@PathVariable id: Int): ActionStatus<Unit> {
        dictsService.removeScienceRank(id)
        return ActionStatus.success()
    }

    @GetMapping
    fun all(filter: Criteria,
            @RequestParam(defaultValue = "10") size: Int,
            @RequestParam page: Int) = try {
        ActionStatus.success(dictsService.findScienceRanks(filter, page, size))
    } catch (ex: Exception) {
        logger.warn(ex) {}
        ActionStatus.except(ex.message)
    }
}

