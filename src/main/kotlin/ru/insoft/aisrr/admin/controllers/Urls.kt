package ru.insoft.aisrr.admin.controllers

import ru.insoft.aisrr.controllers.Urls

object AdminUrls {
    const val COMMON = "${Urls.API}/admin"
    const val USERS = "$COMMON/users"
    const val USER = "$USERS/{id}"
    const val GROUPS = "$COMMON/groups"
    const val HOLIDAYS = "$COMMON/holidays"
}

object DictsUrls {
    const val COMMON = "${AdminUrls.COMMON}/dicts"
    const val COUNTRY = "${COMMON}/countries"
    const val REJECT_REASON = "${COMMON}/reject-reasons"
    const val STORAGE = "${COMMON}/storages"
    const val ARCHIVE = "${COMMON}/archives"
    const val SCIENCERANK = "${COMMON}/science-ranks"
    const val EDUCATION = "${COMMON}/educations"
    const val TOPICHEADING = "${COMMON}/topic-headings"
    const val RRDETAILS = "${COMMON}/reading-rooms"
}

object PageUrls {
    const val LOGIN = "/enter"
    const val ADMIN = "/admin"
    const val ADMIN_CHLD = "/admin/**"
}

object UtilUrls {
    const val ME = "${Urls.API}/me"
    const val LOGIN = "${Urls.API}/login"
}


