package ru.insoft.aisrr.admin.controllers

import mu.KotlinLogging
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.PasswordDTO
import ru.insoft.aisrr.admin.models.UserDTO
import ru.insoft.aisrr.admin.service.UserService
import ru.insoft.aisrr.controllers.ActionStatus

private val logger = KotlinLogging.logger { }

@RestController
class UserController(private val userService: UserService) {

    @GetMapping(UtilUrls.ME)
    fun me(auth: Authentication?) = auth?.principal

    @PostMapping(AdminUrls.USERS)
    fun save(@RequestBody user: UserDTO): ActionStatus<Unit> {
        userService.save(user)
        return ActionStatus.success()
    }

    @PostMapping(AdminUrls.USER)
    fun changePassword(@PathVariable id: Long, @RequestBody newPassword: PasswordDTO): ActionStatus<Unit> {
        userService.changePassword(id, newPassword.password)
        return ActionStatus.success()
    }

    @GetMapping(AdminUrls.USER)
    fun findOne(@PathVariable id: Long) = userService.findOne(id)

    @GetMapping("${AdminUrls.USERS}/exists/{login}")
    fun exists(@PathVariable login: String) = try {
        userService.existsLogin(login)
    } catch (ex: Exception) {
        logger.warn(ex) {}
        true
    }

    @DeleteMapping(AdminUrls.USER)
    fun remove(@PathVariable id: Long): ActionStatus<Unit> {
        userService.remove(id)
        return ActionStatus.success()
    }

    @GetMapping(AdminUrls.USERS)
    fun users(filter: Criteria,
              @RequestParam(defaultValue = "10") size: Int,
              @RequestParam page: Long) = try {
        ActionStatus.success(userService.findAll(filter, page, size))
    } catch (ex: Exception) {
        logger.warn(ex) {}
        ActionStatus.except(ex.message)
    }

}
