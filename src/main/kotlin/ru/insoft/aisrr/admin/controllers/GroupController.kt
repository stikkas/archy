package ru.insoft.aisrr.admin.controllers

import mu.KotlinLogging
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.GroupDTO
import ru.insoft.aisrr.admin.service.GroupService
import ru.insoft.aisrr.controllers.ActionStatus
import ru.insoft.aisrr.controllers.Urls
import ru.insoft.aisrr.enums.Role

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping(AdminUrls.GROUPS)
class GroupController(private val groupService: GroupService) {

    @PostMapping
    fun save(@RequestBody group: GroupDTO): ActionStatus<Unit> {
        groupService.save(group)
        return ActionStatus.success()
    }

    @GetMapping(Urls.ID)
    fun findOne(@PathVariable id: Int) = groupService.findOne(id)

    @DeleteMapping(Urls.ID)
    fun remove(@PathVariable id: Int): ActionStatus<Unit> {
        groupService.remove(id)
        return ActionStatus.success()
    }

    @GetMapping
    fun groups(filter: Criteria,
               @RequestParam(defaultValue = "10") size: Int,
               @RequestParam page: Int) = try {
        ActionStatus.success(groupService.findAll(filter, page, size))
    } catch (ex: Exception) {
        logger.warn(ex) {}
        ActionStatus.except(ex.message)
    }

    @GetMapping("/has-role/{role}")
    fun hasRole(@PathVariable role: Role, @RequestParam groups: List<Int>) = try {
        if (groups.isEmpty()) {
            false
        } else {
            groupService.hasRole(groups, role)
        }
    } catch (ex: Exception) {
        logger.warn(ex) {}
        true
    }

    /**
     * Проверяется есть ли уже группа с такой ролью,
     * нужно для исключения создания нескольких групп с конкретными ролями (роли "Сотрудник ЧЗX")
     * если задан id, то эту группу не учитываем
     */
    @GetMapping("/has-group/{role}")
    fun hasGroup(@PathVariable role: Role, @RequestParam(required = false) id: Int?) = try {
        groupService.hasGroup(role, id)
    } catch (ex: Exception) {
        logger.warn(ex) {}
        true
    }

    /**
     * Проверяется есть ли группа с ролью из требуемых
     * нужно для исключения создания назначения нескольких групп с конкретными ролями (роли "Сотрудник ЧЗX")
     * пользователю
     */
    @GetMapping("/has-any-group")
    fun hasAnyGroup(@RequestParam roles: List<Role>, @RequestParam groups: List<Int>) = try {
        if (groups.isEmpty() || roles.isEmpty()) {
            false
        } else {
            groupService.hasAnyGroup(groups, roles)
        }
    } catch (ex: Exception) {
        logger.warn(ex) {}
        true
    }
}

