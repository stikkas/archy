package ru.insoft.aisrr.admin.controllers

import mu.KotlinLogging
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.admin.models.HolidayCriteria
import ru.insoft.aisrr.admin.models.HolidayDTO
import ru.insoft.aisrr.admin.service.HolidayService
import ru.insoft.aisrr.controllers.ActionStatus
import ru.insoft.aisrr.controllers.Urls

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping(AdminUrls.HOLIDAYS)
class HolidayController(private val holidayService: HolidayService) {

    @PostMapping
    fun save(@RequestBody holiday: HolidayDTO): ActionStatus<Unit> {
        holidayService.save(holiday)
        return ActionStatus.success()
    }

    @DeleteMapping(Urls.ID)
    fun remove(@PathVariable id: Long): ActionStatus<Unit> {
        holidayService.remove(id)
        return ActionStatus.success()
    }

    @GetMapping
    fun holidays(filter: HolidayCriteria,
                 @RequestParam(defaultValue = "10") size: Int,
                 @RequestParam page: Long) = try {
        ActionStatus.success(holidayService.findAll(filter, page, size))
    } catch (ex: Exception) {
        logger.warn(ex) {}
        ActionStatus.except(ex.message)
    }
}

