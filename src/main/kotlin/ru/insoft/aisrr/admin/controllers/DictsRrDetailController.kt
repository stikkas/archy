package ru.insoft.aisrr.admin.controllers

import mu.KotlinLogging
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.DictREntity
import ru.insoft.aisrr.admin.service.DictsService
import ru.insoft.aisrr.controllers.ActionStatus
import ru.insoft.aisrr.controllers.Urls

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping(DictsUrls.RRDETAILS)
class DictsRrDetailController(private val dictsService: DictsService) {

    @PostMapping
    fun save(@RequestBody entity: DictREntity): ActionStatus<Unit> {
        dictsService.saveRrDetail(entity)
        return ActionStatus.success()
    }

    @DeleteMapping(Urls.ID)
    fun remove(@PathVariable id: Int): ActionStatus<Unit> {
        dictsService.removeRrDetail(id)
        return ActionStatus.success()
    }

    @GetMapping
    fun all(filter: Criteria,
            @RequestParam(defaultValue = "10") size: Int,
            @RequestParam page: Int) = try {
        ActionStatus.success(dictsService.findRrDetails(filter, page, size))
    } catch (ex: Exception) {
        logger.warn(ex) {}
        ActionStatus.except(ex.message)
    }
}

