package ru.insoft.aisrr.admin

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import ru.insoft.aisrr.admin.controllers.AdminUrls
import ru.insoft.aisrr.admin.controllers.PageUrls
import ru.insoft.aisrr.admin.controllers.UtilUrls
import ru.insoft.aisrr.enums.Role

@Configuration
class SecurityConfig : WebSecurityConfigurerAdapter() {
    @Value("\${files.security.public}")
    lateinit var publicFiles: Array<String>

    private val om = ObjectMapper()

    override fun configure(http: HttpSecurity) {
        // @formatter:off
        http.authorizeRequests()
            .antMatchers(*publicFiles, UtilUrls.ME)
                .permitAll()
            .antMatchers(PageUrls.ADMIN, PageUrls.ADMIN_CHLD, "${AdminUrls.COMMON}/**")
                .hasAuthority(Role.ADMIN.authority)
            .anyRequest().authenticated()
                /*
            .and()
                .exceptionHandling()
                .authenticationEntryPoint{request, response, _ ->
                        var nextPage = request.requestURI
                        var redirectUrl = PageUrls.LOGIN
                        if (nextPage != PageUrls.LOGIN && nextPage != "/") {
                            if (request.queryString != null) {
                                nextPage += "?${request.queryString}"
                            }
                            redirectUrl += "?next=${response.encodeURL(nextPage)}"
                        }

                        response.sendRedirect(redirectUrl)
                    }
                 */
            .and()
                .formLogin()
                    .loginPage(PageUrls.LOGIN)
                    .loginProcessingUrl(UtilUrls.LOGIN)
                    .usernameParameter("user")
                    .passwordParameter("pass")
                    .successHandler{_, res, auth ->
                        om.writeValue(res.outputStream, auth.principal)
                    }
                    .failureHandler{_, res, ex ->
                        res.writer.print("");
                    }
                    .permitAll()
            .and()
                .logout()
                .logoutSuccessUrl(PageUrls.LOGIN)
            .and().cors().and().csrf().disable()
        // @formatter:on
    }

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()
}

