package ru.insoft.aisrr.admin.service

import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.GroupDTO
import ru.insoft.aisrr.admin.models.GroupRecord
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.models.Page

interface GroupService {
    fun findOne(id: Int): GroupDTO?
    fun save(user: GroupDTO)
    fun remove(id: Int)
    fun findAll(filter: Criteria, page: Int, size: Int): Page<GroupRecord>
    fun hasRole(groups: List<Int>, role: Role): Boolean
    fun hasAnyGroup(groups: List<Int>, roles: List<Role>): Boolean
    fun hasGroup(role: Role, id: Int?): Boolean
}
