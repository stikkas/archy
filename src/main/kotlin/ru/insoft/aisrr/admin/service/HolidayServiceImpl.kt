package ru.insoft.aisrr.admin.service

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.admin.models.HolidayCriteria
import ru.insoft.aisrr.admin.models.HolidayDTO
import ru.insoft.aisrr.admin.repo.HolidayMapper
import ru.insoft.aisrr.models.Page
import ru.insoft.aisrr.offset

@Service
@Transactional
class HolidayServiceImpl(private val holidayMapper: HolidayMapper) : HolidayService {
    override fun save(holiday: HolidayDTO) {
        if (holiday.id == null) {
            holidayMapper.insert(holiday)
        } else {
            holidayMapper.update(holiday)
        }
    }

    override fun remove(id: Long) = holidayMapper.remove(id)

    @Transactional(readOnly = true)
    override fun findAll(filter: HolidayCriteria, page: Long, size: Int) = runBlocking {
        val count = async { holidayMapper.count(filter) }
        val data = async { holidayMapper.findAll(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

}
