package ru.insoft.aisrr.admin.service

import ru.insoft.aisrr.admin.models.HolidayCriteria
import ru.insoft.aisrr.admin.models.HolidayDTO
import ru.insoft.aisrr.admin.models.HolidayRecord
import ru.insoft.aisrr.models.Page

interface HolidayService {
    fun save(holiday: HolidayDTO)
    fun remove(id: Long)
    fun findAll(filter: HolidayCriteria, page: Long, size: Int): Page<HolidayRecord>
}
