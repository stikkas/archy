package ru.insoft.aisrr.admin.service

import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl(private val userService: UserService) : UserDetailsService {

    override fun loadUserByUsername(name: String?) =
            name?.let { userService.findOne(name) } ?: throw UsernameNotFoundException("")

}
