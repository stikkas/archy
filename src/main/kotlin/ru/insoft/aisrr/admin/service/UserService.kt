package ru.insoft.aisrr.admin.service

import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.User
import ru.insoft.aisrr.admin.models.UserDTO
import ru.insoft.aisrr.admin.models.UserRecord
import ru.insoft.aisrr.models.Page

interface UserService {
    fun findOne(id: Long): UserDTO?
    fun findOne(login: String): User?
    fun existsLogin(login: String): Boolean
    fun save(user: UserDTO)
    fun remove(id: Long)
    fun findAll(filter: Criteria, page: Long, size: Int): Page<UserRecord>
    fun changePassword(id: Long, rawPassword: String)
}
