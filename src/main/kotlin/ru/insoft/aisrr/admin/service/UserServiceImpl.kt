package ru.insoft.aisrr.admin.service

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.UserDTO
import ru.insoft.aisrr.admin.repo.UserMapper
import ru.insoft.aisrr.models.Page
import ru.insoft.aisrr.offset
import java.lang.Long.max

@Transactional
@Service
class UserServiceImpl(private val userMapper: UserMapper,
                      private val passwordEncoder: BCryptPasswordEncoder) : UserService {

    @Transactional(readOnly = true)
    override fun findOne(login: String) = userMapper.findByLogin(login)

    @Transactional(readOnly = true)
    override fun existsLogin(login: String) = userMapper.existsLogin(login)

    @Transactional(readOnly = true)
    override fun findOne(id: Long) = userMapper.findById(id)

    override fun save(user: UserDTO) {
        if (user.id == null) {
            user.password = passwordEncoder.encode(user.password)
            userMapper.insert(user)
        } else {
            userMapper.update(user)
        }
    }

    override fun remove(id: Long) {
        userMapper.remove(id);
    }

    @Transactional(readOnly = true)
    override fun findAll(filter: Criteria, page: Long, size: Int) = runBlocking {
        val count = async { userMapper.count(filter) }
        val data = async { userMapper.findAll(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page)
    }

    override fun changePassword(id: Long, rawPassword: String) {
        this.userMapper.changePassword(id, passwordEncoder.encode(rawPassword))
    }
}



