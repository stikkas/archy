package ru.insoft.aisrr.admin.service

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.admin.models.Criteria
import ru.insoft.aisrr.admin.models.GroupDTO
import ru.insoft.aisrr.admin.repo.GroupMapper
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.models.Page
import ru.insoft.aisrr.offset
import kotlin.math.max

@Transactional
@Service
class GroupServiceImpl(private val groupMapper: GroupMapper) : GroupService {

    @Transactional(readOnly = true)
    override fun findOne(id: Int) = groupMapper.findById(id)

    override fun save(group: GroupDTO) {
        if (group.id == null) {
            groupMapper.insert(group)
        } else {
            groupMapper.update(group)
        }
    }

    override fun remove(id: Int) {
        groupMapper.remove(id);
    }

    @Transactional(readOnly = true)
    override fun findAll(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { groupMapper.count(filter) }
        val data = async { groupMapper.findAll(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }


    @Transactional(readOnly = true)
    override fun hasRole(groups: List<Int>, role: Role) = groupMapper.hasRole(groups, role)

    @Transactional(readOnly = true)
    override fun hasAnyGroup(groups: List<Int>, roles: List<Role>) = groupMapper.hasAnyGroup(groups, roles)

    @Transactional(readOnly = true)
    override fun hasGroup(role: Role, id: Int?) = groupMapper.hasGroup(role, id)
}



