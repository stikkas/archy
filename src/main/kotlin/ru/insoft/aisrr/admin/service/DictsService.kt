package ru.insoft.aisrr.admin.service

import ru.insoft.aisrr.admin.models.*
import ru.insoft.aisrr.admin.models.DictSEntity
import ru.insoft.aisrr.models.Page

interface DictsService {
    fun saveCountry(entity: DictEntity)
    fun removeCountry(id: Int)
    fun findCountries(filter: Criteria, page: Int, size: Int): Page<DictEntity>

    fun saveRejectReason(entity: DictEntity)
    fun removeRejectReason(id: Int)
    fun findRejectReasons(filter: Criteria, page: Int, size: Int): Page<DictEntity>

    fun saveStorage(entity: DictStorageEntity)
    fun removeStorage(id: Int)
    fun findStorages(filter: Criteria, page: Int, size: Int): Page<DictStorageEntity>

    fun saveArchive(entity: DictSEntity)
    fun removeArchive(id: Int)
    fun findArchives(filter: Criteria, page: Int, size: Int): Page<DictSEntity>

    fun saveEducation(entity: DictEntity)
    fun removeEducation(id: Int)
    fun findEducations(filter: Criteria, page: Int, size: Int): Page<DictEntity>

    fun saveScienceRank(entity: DictEntity)
    fun removeScienceRank(id: Int)
    fun findScienceRanks(filter: Criteria, page: Int, size: Int): Page<DictEntity>

    fun saveTopicHeading(entity: DictEntity)
    fun removeTopicHeading(id: Int)
    fun findTopicHeadings(filter: Criteria, page: Int, size: Int): Page<DictEntity>

    fun saveRrDetail(entity: DictREntity)
    fun removeRrDetail(id: Int)
    fun findRrDetails(filter: Criteria, page: Int, size: Int): Page<DictREntity>
}
