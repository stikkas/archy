package ru.insoft.aisrr.admin.service

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.admin.models.*
import ru.insoft.aisrr.admin.repo.DictsMapper
import ru.insoft.aisrr.admin.models.DictSEntity
import ru.insoft.aisrr.models.Page
import ru.insoft.aisrr.offset

@Transactional
@Service
class DictsServiceImpl(private val dictsMapper: DictsMapper) : DictsService {

    override fun saveCountry(entity: DictEntity) =
            if (entity.id == null) {
                dictsMapper.insertCountry(entity)
            } else {
                dictsMapper.updateCountry(entity)
            }

    override fun removeCountry(id: Int) = dictsMapper.removeCountry(id)

    @Transactional(readOnly = true)
    override fun findCountries(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countCountries(filter) }
        val data = async { dictsMapper.findCountries(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveRejectReason(entity: DictEntity) =
            if (entity.id == null) {
                dictsMapper.insertRejectReason(entity)
            } else {
                dictsMapper.updateRejectReason(entity)
            }

    override fun removeRejectReason(id: Int) = dictsMapper.removeRejectReason(id)

    @Transactional(readOnly = true)
    override fun findRejectReasons(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countRejectReasons(filter) }
        val data = async { dictsMapper.findRejectReasons(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveStorage(entity: DictStorageEntity) =
            if (entity.id == null) {
                dictsMapper.insertStorage(entity)
            } else {
                dictsMapper.updateStorage(entity)
            }

    override fun removeStorage(id: Int) = dictsMapper.removeStorage(id)

    @Transactional(readOnly = true)
    override fun findStorages(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countStorages(filter) }
        val data = async { dictsMapper.findStorages(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveRrDetail(entity: DictREntity) =
            if (entity.id == null) {
                dictsMapper.insertRrDetail(entity)
            } else {
                dictsMapper.updateRrDetail(entity)
            }

    override fun removeRrDetail(id: Int) = dictsMapper.removeRrDetail(id)

    @Transactional(readOnly = true)
    override fun findRrDetails(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countRrDetails(filter) }
        val data = async { dictsMapper.findRrDetails(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveEducation(entity: DictEntity) =
            if (entity.id == null) {
                dictsMapper.insertEducation(entity)
            } else {
                dictsMapper.updateEducation(entity)
            }

    override fun removeEducation(id: Int) = dictsMapper.removeEducation(id)

    @Transactional(readOnly = true)
    override fun findEducations(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countEducations(filter) }
        val data = async { dictsMapper.findEducations(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveTopicHeading(entity: DictEntity) =
            if (entity.id == null) {
                dictsMapper.insertTopicHeading(entity)
            } else {
                dictsMapper.updateTopicHeading(entity)
            }

    override fun removeTopicHeading(id: Int) = dictsMapper.removeTopicHeading(id)

    @Transactional(readOnly = true)
    override fun findTopicHeadings(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countTopicHeadings(filter) }
        val data = async { dictsMapper.findTopicHeadings(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveScienceRank(entity: DictEntity) =
            if (entity.id == null) {
                dictsMapper.insertScienceRank(entity)
            } else {
                dictsMapper.updateScienceRank(entity)
            }

    override fun removeScienceRank(id: Int) = dictsMapper.removeScienceRank(id)

    @Transactional(readOnly = true)
    override fun findScienceRanks(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countScienceRanks(filter) }
        val data = async { dictsMapper.findScienceRanks(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }

    override fun saveArchive(entity: DictSEntity) =
            if (entity.id == null) {
                dictsMapper.insertArchive(entity)
            } else {
                dictsMapper.updateArchive(entity)
            }

    override fun removeArchive(id: Int) = dictsMapper.removeArchive(id)

    @Transactional(readOnly = true)
    override fun findArchives(filter: Criteria, page: Int, size: Int) = runBlocking {
        val count = async { dictsMapper.countArchives(filter) }
        val data = async { dictsMapper.findArchives(filter, page.offset(size), size) }
        Page(data.await(), size, count.await(), page.toLong())
    }
}


