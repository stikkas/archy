package ru.insoft.aisrr.enums

import org.springframework.security.core.GrantedAuthority
import ru.insoft.aisrr.models.Dict

enum class Role(override val id: Int, val label: String) : IntId, GrantedAuthority {
    /**
     * имеет доступ ко всему, имеет право создавать, удалять, изменять пользователей системы
     */
    ADMIN(1, "Администратор"),
    /**
     * должен входить в систему только с АРМ ЧЗ №1 и имеет право оформлять заказы для ЧЗ №1
     */
    RR1(10, "Сотрудник ЧЗ1"),
    /**
     * должен входить в систему только с АРМ ЧЗ №2 и имеет право оформлять заказы для ЧЗ №2
     */
    RR2(11, "Сотрудник ЧЗ2"),
    /**
     * должен входить в систему только с АРМ ЧЗ №3 и имеет право оформлять заказы для ЧЗ №3
     */
    RR3(12, "Сотрудник ЧЗ3"),
    CHIEF_RR(13, "Начальник всех читальных залов"),
    /**
     * имеет доступ к вкладке Начальник отдела хранения при оформлении заказа
     */
    CHIEF_ARCHIVE(15, "Начальник отдела хранения"),
    /**
     * лицо выдающее заказы, имеет доступ к вкладке Хранитель при оформлении заказа
     */
    EXECUTOR(16, "Исполнитель");

    override fun codes() = values()
    override val code = name

    override fun getAuthority() = name

    companion object {
        fun dict() = values().map { Dict(it.name, it.label) }
    }
}


