package ru.insoft.aisrr.enums

import ru.insoft.aisrr.models.Dict

enum class OrderType(override val id: Int, val label: String) : IntId {
    STANDARD(1, "Заказ на выдачу архивных документов"),
    FP(2, "ФП заказ на выдачу архивных документов");

    override fun codes() = values()
    override val code = name

    companion object {
        fun dict() = values().map { Dict(it.name, it.label) }
    }
}

