package ru.insoft.aisrr.enums

import ru.insoft.aisrr.models.Dict

enum class OrderState(override val id: Int, val label: String) : IntId {
    PRE(1, "Предзаказ"),
    ACCEPT(2, "Принят"),
    CANCEL1(3, "Аннулирован"),
    REJECT(4, "Отказано"),
    ONEXEC(5, "На исполнении"),
    CANCEL2(6, "Аннулирован"),
    READY(7, "Подготовлен"),
    CANCEL3(8, "Аннулирован"),
    EXEC(9, "Выдано в ЧЗ"),
    DUMP(10, "Списано"),
    RETURN(11, "Возвращено в хранилище");

    override fun codes() = values()
    override val code = name

    companion object {
        fun dict() = values().filter { it != CANCEL2 && it != CANCEL3 }.map { Dict(it.name, it.label) }
    }
}

