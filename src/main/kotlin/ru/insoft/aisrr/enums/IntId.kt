package ru.insoft.aisrr.enums

interface IntId {
    val id: Int
    fun codes(): Array<out IntId>
    val code: String
}
