package ru.insoft.aisrr.enums

import ru.insoft.aisrr.models.Dict

/**
 * Разрешение на выдачу
 */
enum class PermissionIssue(override val id: Int, val label: String) : IntId {
    ALLOWED(1, "Разрешено"),
    DENIED(2, "Отказано");

    override fun codes() = values()
    override val code = name

    companion object {
        fun dict() = values().map { Dict(it.name, it.label) }
    }
}

