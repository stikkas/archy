package ru.insoft.aisrr.enums

import ru.insoft.aisrr.models.Dict

enum class ReadingRoom(override val id: Int, val label: String) : IntId {
    RR1(1, "№1"),
    RR2(2, "№2"),
    RR3(3, "№3");

    override fun codes() = values()
    override val code = name

    companion object {
        fun dict() = values().map { Dict(it.name, it.label) }
    }
}

