package ru.insoft.aisrr.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.models.Visitor
import ru.insoft.aisrr.repo.VisitorMapper

@Transactional
@Service
class VisitorServiceImpl(private val visitorMapper: VisitorMapper) : VisitorService {

    @Transactional(readOnly = true)
    override fun findOne(id: Long) = visitorMapper.findOne(id)

    override fun save(visitor: Visitor) {
        if (visitor.id == null) {
            visitorMapper.insert(visitor)
        } else {
            visitorMapper.update(visitor)
        }
    }

    override fun remove(id: Long) = visitorMapper.remove(id)
}
