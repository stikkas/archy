package ru.insoft.aisrr.services

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.models.order.*
import ru.insoft.aisrr.repo.OrderDocumentMapper
import ru.insoft.aisrr.repo.OrderMapper
import java.util.*
import kotlin.collections.HashMap

@Transactional
@Service
class OrderServiceImpl(private val daysService: DaysService,
                       private val orderMapper: OrderMapper,
                       private val documentMapper: OrderDocumentMapper) : OrderService {

    override fun remove(id: Long) {
        orderMapper.remove(id)
    }

    override fun saveRegistration(registration: OrderRegistration) {
        registration.status = registration.status ?: OrderState.PRE
        if (registration.status == OrderState.ACCEPT) {
            val cal = Calendar.getInstance()
            registration.regDate = cal.time
            var (issueDate, controlReturnDate) = daysService.getDate(registration.readingRoom!!, cal.time, 3, 19)
            registration.issueDate = issueDate
            registration.controlReturnDate = controlReturnDate
        }

        if (registration.id == null) {
            orderMapper.insert(registration)
            updateDocs(registration)
        } else {
            val id = registration.id!!
            val currentStatus = findRegistration(id)?.status!!
            registration.status?.let {
                if (it == OrderState.PRE || (it == OrderState.ACCEPT && currentStatus == OrderState.PRE)) {
                    orderMapper.updateRegistration(registration)
                    updateDocs(registration)
                } else if ((it == OrderState.EXEC && it == currentStatus) || (it == OrderState.DUMP && currentStatus == OrderState.EXEC)) {
                    if (it == OrderState.DUMP) {
                        registration.returnDate = registration.docs!!.filter { d -> d.spisanoDate != null }
                                .map { d -> d.spisanoDate!! }.maxBy { t -> t.time }
                        orderMapper.updateStatus(it, id, registration.returnDate)
                    }
                    registration.docs?.forEach { d ->
                        documentMapper.updateDump(d)
                    }
                } else if ((it == OrderState.CANCEL1 && (currentStatus == OrderState.ACCEPT || currentStatus == OrderState.PRE)) || (
                                it == OrderState.CANCEL2 && (currentStatus == OrderState.REJECT || currentStatus == OrderState.ONEXEC)
                                ) || (it == OrderState.CANCEL3 && currentStatus == OrderState.READY)) {
                    orderMapper.updateStatus(it, id)
                } else {
                    null
                }
            }
        }
    }

    override fun saveProcessing(process: OrderProcess) {
        if (process.status == OrderState.REJECT || process.status == OrderState.ONEXEC) {
            process.permissionDate = Date()
        }
        orderMapper.updateProcessing(process)
    }

    override fun saveExecution(execution: OrderExecution) {
        val currentStatus = findExecution(execution.id!!)?.status!!
        execution.status?.let {
            if (it == OrderState.ONEXEC || (it == OrderState.READY && currentStatus == OrderState.ONEXEC)) {
                if (it == OrderState.READY && execution.docs?.all { d -> d.rejectReasonId != null } == true) {
                    execution.status = OrderState.REJECT
                }
                orderMapper.updateExecution(execution)
                execution.docs?.forEach { d ->
                    documentMapper.updateOnexec(d)
                }
            } else if (it == OrderState.EXEC && currentStatus == OrderState.READY) {
                orderMapper.updateExecution(execution)
            } else if ((it == OrderState.DUMP && it == currentStatus) || (it == OrderState.RETURN && currentStatus == OrderState.DUMP)) {
                orderMapper.updateExecution(execution)
                execution.docs?.forEach { d ->
                    documentMapper.updateReturn(d)
                }
            } else {
                null
            }
        }
    }

    @Transactional(readOnly = true)
    override fun findRegistration(id: Long) = runBlocking {
        val regAsync = async { orderMapper.findRegistration(id) }
        val docsAsync = async { documentMapper.findDocs(id) }
        regAsync.await()?.apply {
            docs = docsAsync.await()
        }
    }

    @Transactional(readOnly = true)
    override fun findProcessing(id: Long) = runBlocking {
        val regAsync = async { orderMapper.findProcessing(id) }
        val docsAsync = async { documentMapper.findDocs(id) }
        regAsync.await()?.apply {
            docs = docsAsync.await()
        }
    }

    @Transactional(readOnly = true)
    override fun findExecution(id: Long) = runBlocking {
        val regAsync = async { orderMapper.findExecution(id) }
        val docsAsync = async { documentMapper.findDocs(id) }
        regAsync.await()?.apply {
            docs = docsAsync.await()
        }
    }

    @Transactional(readOnly = true)
    override fun check(orderId: Long?, order: OrderRegistration?) = CheckedData().apply {
        val ords = orderMapper.findRelated(orderId, order?.applicantId).toMutableList()
        runBlocking {
            ords.map { it to async { documentMapper.findCheckedDocs(it.id) } }.forEach {
                it.first.docs = it.second.await()
            }
        }
        if (order != null && (order.id == null || !ords.any { it.id == order.id })) {
            // новый заказ, в базе еще нет
            // или не новый но не попал в выдачу из-за статуса, например
            ords.add(CheckedDataOrder().apply {
                archiveId = order.archiveId
                docs = order.docs?.map {
                    CheckedDocument().apply {
                        fundNumber = it.fundNumber
                        pages = it.pages ?: 0
                    }
                }
            })
        }
        files = ords.map { it.files }.sum()
        pages = ords.map { it.pages }.sum()
        pass = files <= maxFiles && pages <= maxAllPages
        if (pass && pages > maxSpecialPages) {
            var arch6Pages = 0
            var fundsPages = HashMap<String?, Int>()
            ords.forEach {
                it.docs?.forEach { d ->
                    if (it.archiveId == 6) {
                        arch6Pages += d.pages
                    }
                    if (fonds.contains(d.fundNumber)) {
                        fundsPages[d.fundNumber] = d.pages + (fundsPages[d.fundNumber] ?: 0)
                    }
                }
            }
            pass = arch6Pages <= maxSpecialPages && fundsPages.all { it.value <= maxSpecialPages }
        }
        orders = ords
    }

    private fun updateDocs(registration: OrderRegistration) {
        val orderId = registration.id!!
        registration.docs?.forEach {
            if (it.id == null) {
                documentMapper.insert(it, orderId)
            } else {
                documentMapper.update(it)
            }
        }

        if (registration.docs?.isNotEmpty() == true) {
            documentMapper.remove(registration.docs!!.map { it.id!! }, listOf(orderId))
        }
    }

    companion object {
        private val fonds = listOf(357, 439, 458, 642, 864, 1186, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340,
                1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1372, 1375, 1384, 1389, 1391, 1401, 1405,
                1406, 1431, 1433, 1441, 1447, 1458, 1459, 1463, 1464, 1465, 1466, 1467, 1468, 1469, 1470, 1475, 1478, 1479, 1480, 1582, 1614, 1637, 1640,
                1650, 1673, 1735, 1737, 1739, 1740, 1742, 1744, 1748, 1749, 1751, 1755, 1756, 1757, 1759, 1761, 1764, 1765, 1766, 1845, 1846, 1847, 1848, 1849, 1851, 1852,
                1854, 1857, 1858, 1859, 1860, 1861, 1862, 1863, 1864, 1866, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1878, 1879, 1880, 1885, 1886, 1887, 1961, 2021, 2049,
                2075, 2076, 2077, 2078, 2079, 2136, 2202, 2203, 2241, 2244, 2251, 2253, 2254, 2255, 2256, 2263, 2317, 2347, 2348, 2379, 2391
        ).map { "$it" }
        private const val maxAllPages = 1500
        private const val maxSpecialPages = 500
        private const val maxFiles = 20
    }

}


