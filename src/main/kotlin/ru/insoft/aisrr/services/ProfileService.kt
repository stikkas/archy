package ru.insoft.aisrr.services

import ru.insoft.aisrr.models.Profile

interface ProfileService {

    fun findOne(id: Long): Profile?
    fun save(profile: Profile)
    fun remove(id: Long)
}

