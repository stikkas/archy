package ru.insoft.aisrr.services

import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.models.Dict
import ru.insoft.aisrr.models.DictS

interface DictService {

    fun countries(): List<Dict<Int>>
    fun archives(applicantId: Long?): List<Dict<Int>>
    fun archivesShort(): List<Dict<Int>>
    fun groups(): List<Dict<Int>>
    fun educations(): List<Dict<Int>>
    fun scienceRanks(): List<Dict<Int>>
    fun topicHeadings(): List<Dict<Int>>
    fun users(query: String?, size: Int?): List<DictS<Long>>
    fun logins(): List<String>
    fun executors(): List<Dict<Long>>
    fun months(): List<Dict<Int>>
    fun storages(rr: ReadingRoom?, archive: Int?): List<Dict<Int>>
    fun rejectReasons(): List<Dict<Int>>
    fun researchThemes(archive: Int, profile: Long): List<Dict<Long>>
    fun archivesRr(query: ReadingRoom?): List<Dict<Int>>
    fun rrsArchive(query: Int?): List<Dict<String>>
}

