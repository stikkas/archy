package ru.insoft.aisrr.services

import java.io.OutputStream
import java.util.*

interface ReportService {
    fun orderDetails(id: Long, out: OutputStream)
    fun attendanceRr(start: Date, end: Date, out: OutputStream)
}

