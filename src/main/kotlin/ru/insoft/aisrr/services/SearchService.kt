package ru.insoft.aisrr.services

import ru.insoft.aisrr.models.*

interface SearchService {

    fun findUsers(filter: UserCriteria, page: Long, size: Int): Page<UserTableRow>

    fun findVisitors(filter: VisitorCriteria, page: Long, size: Int): Page<VisitorTableRow>

    fun findVisits(id: Long, page: Long, size: Int): Page<Visit>

    fun findUserOrders(id: Long, page: Long, size: Int): Page<UserOrder>

    fun findOrders(filter: OrderCriteria, page: Long, size: Int): Page<OrderTableRow>

}

