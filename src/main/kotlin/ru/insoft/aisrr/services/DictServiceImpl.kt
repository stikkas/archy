package ru.insoft.aisrr.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.admin.repo.GroupMapper
import ru.insoft.aisrr.admin.repo.UserMapper
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.models.Dict
import ru.insoft.aisrr.repo.DictMapper
import ru.insoft.aisrr.repo.ProfileMapper

@Service
@Transactional(readOnly = true)
class DictServiceImpl(private val dictMapper: DictMapper,
                      private val groupMapper: GroupMapper,
                      private val userMapper: UserMapper,
                      private val profileMapper: ProfileMapper) : DictService {

    override fun logins() = dictMapper.logins()

    override fun groups() = groupMapper.dict()

    override fun users(query: String?, size: Int?) = profileMapper.dict(query?.toLowerCase(), size!!)

    override fun executors() = userMapper.dict(Role.EXECUTOR)

    override fun topicHeadings() = dictMapper.topicHeadings()

    override fun educations() = dictMapper.educations()

    override fun scienceRanks() = dictMapper.scienceRanks()

    override fun countries() = dictMapper.countries()

    override fun archives(query: Long?) = dictMapper.archives(query)

    override fun archivesRr(rr: ReadingRoom?) = dictMapper.archivesRr(rr)

    override fun rrsArchive(archiveId: Int?) = dictMapper.rrsArchive(archiveId).map{ Dict(it.name, it.label) }

    override fun months() = dictMapper.months()

    override fun storages(rr: ReadingRoom?, archive: Int?) = dictMapper.storages(rr, archive)

    override fun archivesShort() = dictMapper.archivesShort()

    override fun rejectReasons() = dictMapper.rejectReasons()

    override fun researchThemes(archive: Int, profile: Long) = dictMapper.researchThemes(archive, profile)
}
