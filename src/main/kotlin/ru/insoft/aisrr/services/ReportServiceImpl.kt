package ru.insoft.aisrr.services

import com.itextpdf.text.*
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.springframework.stereotype.Service
import ru.insoft.aisrr.enums.OrderType
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.models.order.PrintDocument
import ru.insoft.aisrr.models.order.PrintOrder
import ru.insoft.aisrr.repo.DictMapper
import ru.insoft.aisrr.repo.OrderDocumentMapper
import ru.insoft.aisrr.repo.OrderMapper
import ru.insoft.aisrr.repo.VisitorMapper
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

private val logger = KotlinLogging.logger { }

@Service
class ReportServiceImpl(private val orderMapper: OrderMapper,
                        private val visitorMapper: VisitorMapper,
                        private val dictMapper: DictMapper,
                        private val documentMapper: OrderDocumentMapper) : ReportService {

    override fun attendanceRr(start: Date, end: Date, out: OutputStream) = runBlocking {
        createDocument(10f, out, """Отчет "Посещаемость читального зала"""") { doc ->
            val archives = dictMapper.archivesShort(true)
            val data = listOf(ReadingRoom.RR1, ReadingRoom.RR2, ReadingRoom.RR3).map { r ->
                r to archives.map { a ->
                    async { visitorMapper.countVisits(start, end, r, a.value) } to
                            async { visitorMapper.countUniqVisits(start, end, r, a.value) }
                }
            }

            val sdf = SimpleDateFormat("dd.MM.yyyy г.")
            var p = Paragraph("Посещаемость читального зала", titleFont)
            p.alignment = Element.ALIGN_CENTER
            doc.add(p)
            p = Paragraph("за период с ${sdf.format(start)} по ${sdf.format(end)}", normStdFont)
            p.alignment = Element.ALIGN_CENTER
            p.spacingAfter = 20f
            doc.add(p)
            var table = PdfPTable(archives.size + 3)
            table.widthPercentage = 100f
            table.setWidths(intArrayOf(1, 2, *((1..archives.size).map { 2 }.toIntArray()), 2))
            arrayOf("", "Кол-во посещений", *archives.map { it.label }.toTypedArray(), "Итого по ЧЗ").forEach {
                table.addCell(createCell(it))
            }
            val (allItog, uniqItog) = data.map { d ->
                var cell = createCell("ЧЗ ${d.first.id}")
                cell.rowspan = 2
                table.addCell(cell)
                table.addCell(createCell("всего"))
                val (allList, uniqList) = d.second.map { it.first.await() to it.second.await() }.unzip()
                addCountersFormRr(table, allList)
                table.addCell(createCell("уникальных"))
                addCountersFormRr(table, uniqList)
                allList to uniqList
            }.unzip()

            val cell = createCell("Итого по отделу")
            cell.colspan = 2
            cell.rowspan = 2
            table.addCell(cell)
            addCountersItogArchive(table, allItog)
            addCountersItogArchive(table, uniqItog)
            doc.add(table)
        }
    }

    override fun orderDetails(id: Long, out: OutputStream) = runBlocking {
        createDocument(8f, out, "Детали заказа") { doc ->
            val now = SimpleDateFormat("dd.MM.yyyy г.").format(Date())
            val order = async { orderMapper.findPrintOrder(id) }
            val preparedDocs = async { documentMapper.findPrintDocs(id) }
            fillPage(doc, order.await(), preparedDocs.await(), "РАЗРЕШАЮ", now)
        }
    }

    private inline fun createDocument(border: Float, out: OutputStream, title: String, block: (doc: Document) -> Unit) {
        try {
            val offset: Float = Utilities.millimetersToPoints(border)
            val doc = Document(PageSize.A4)
            doc.setMargins(offset, offset, offset, offset)
            PdfWriter.getInstance(doc, out)
            doc.open()
            doc.addAuthor("AIS-RR")
            doc.addCreator("AIS-RR")
            doc.addTitle(title)
            block(doc)
            doc.close()
        } catch (ex: DocumentException) {
            logger.error(ex) {}
        }
    }

    private fun addCountersFormRr(table: PdfPTable, counts: List<Long>) {
        var all = 0L
        counts.forEach {
            table.addCell(createCell(if (it == 0L) "" else it.toString()))
            all += it
        }
        table.addCell(createCell(if (all == 0L) "" else all.toString()))
    }

    private fun addCountersItogArchive(table: PdfPTable, counts: List<List<Long>>) {
        var all = 0L
        for (i in 0 until counts[0].size) {
            var count = 0L
            for (el in counts) {
                count += el[i]
            }
            table.addCell(createCell(if (count == 0L) "" else count.toString()))
            all += count
        }
        table.addCell(createCell(if (all == 0L) "" else all.toString()))
    }

    private fun fillPage(doc: Document, order: PrintOrder?, docs: List<PrintDocument>, statusTitle: String, date: String) {
        var table = PdfPTable(2)
        table.widthPercentage = 100f
        table.setWidths(intArrayOf(1, 1))
        var p = Paragraph(order?.archive ?: "", stdFont)
        p.indentationLeft = 40f

        var cell = PdfPCell()
        cell.border = 0
        cell.addElement(p)
        cell.addElement(createPodpis("_".repeat(49), 26, 10))
        cell.addElement(createPodpis("(название отдела хранения документов)", 60, 12))
        val fp = if (order?.type == OrderType.FP) "ФП " else ""
        arrayOf("${fp}ЗАКАЗ (ТРЕБОВАНИЕ) НА ВЫДАЧУ", "АРХИВНЫХ ДОКУМЕНТОВ, КОПИЙ", "ФОНДА ПОЛЬЗОВАНИЯ, ОПИСЕЙ ДЕЛ").forEach {
            p = Paragraph(it, boldFont)
            p.indentationLeft = 20f
            p.leading = 11.5f
            cell.addElement(p)
        }

        table.addCell(cell)

        cell = PdfPCell()
        cell.border = 0
        p = Paragraph("$statusTitle выдачу документов", Font(stdBaseFont, 12f))
        p.indentationLeft = 60f
        cell.addElement(p)
        p = Paragraph("_".repeat(32), stdFont)
        p.indentationLeft = 60f
        cell.addElement(p)
        cell.addElement(createPodpis("наименование должности", 100, 3))
        p = Paragraph("_".repeat(32), stdFont)
        p.indentationLeft = 60f
        cell.addElement(p)
        cell.addElement(createPodpis("подпись             расшифровка подписи", 80, 3))
        p = Paragraph(date, stdFont)
        p.indentationLeft = 70f
        cell.addElement(p)
        var chunk = Chunk("_".repeat(14), stdFont)
        chunk.textRise = 14f
        p = Paragraph(chunk)
        p.indentationLeft = 60f
        cell.addElement(p)
        cell.addElement(createPodpis("дата", 90, 16))
        table.addCell(cell)
        doc.add(table)
        p = Paragraph("${order?.fio ?: ""}, №${order?.personNumber ?: ""}", stdFont)
        p.indentationLeft = 40f
        p.leading = 7f
        doc.add(p)
        doc.add(createPodpis("_".repeat(110), 20, 12))
        doc.add(createPodpis("(фамилия, инициалы, № личного дела пользователя)", 160, 16))

        val strings = ArrayList<String>()
        var curString = ""
        order?.thema?.split(" ")?.forEach {
            if (curString.length + it.length > maxLetters) {
                strings.add(curString)
                curString = ""
            }
            curString += " $it"
        }

        if (curString.isNotEmpty()) {
            strings.add(curString)
        }

        strings.forEachIndexed { i, s ->
            chunk = Chunk(s, stdFont)
            p = Paragraph(s, stdFont)
            p.indentationLeft = 20f
            p.leading = 0f
            doc.add(p)
            doc.add(createPodpis("_".repeat(110), 20, 12))
            if (i == 0) {
                doc.add(createPodpis("(тема исследования)", 200, 14))
            }
        }

        table = PdfPTable(8)
        table.widthPercentage = 100f
        table.setWidths(floatArrayOf(1f, 1f, 1f, 5.7f, 1f, 2f, 2.3f, 2.8f))
        table.addCell(createCell("№ фонда", true))
        table.addCell(createCell("№ описи", true))
        table.addCell(createCell("№ ед. хр. / дела", true))
        table.addCell(createCell("Заголовок ед. хр.", false, true))
        cell = createCell("Кол-во листов, время звучания, метраж", true)
        cell.fixedHeight = 110f
        table.addCell(cell)
        table.addCell(createCell("Расписка пользователя в получении документов, дата"))
        table.addCell(createCell("Расписка работника читального зала в возвращении документов пользователем, дата"))
        table.addCell(createCell("Расписка работника архивохранилища в возвращении документов в архивохранилище, дата"))
        (1..8).forEach {
            table.addCell(createCell("$it", false, true, true))
        }
        docs.forEach {
            table.addCell(createCell(it.fundNumber, false, true, true))
            table.addCell(createCell(it.opisNumber, false, true, true))
            table.addCell(createCell(it.unitNumber, false, true, true))
            table.addCell(createCell(it.unitTitle, false, true))
            table.addCell(createCell(it.pages?.toString() ?: "", false, true, true))
            var maxEmpty = 3
            if (it.reason != null) {
                table.addCell(createCell(it.reason, false, true, true))
                maxEmpty = 2
            }
            (1..maxEmpty).forEach { table.addCell(createCell("")) }
        }
        doc.add(table)
        p = Paragraph("«______»__________________20       г.", stdFont)
        p.tabSettings = TabSettings(130f)
        p.add(Chunk.TABBING)
        p.add(Chunk("Подпись пользователя", boldFont))
        p.add(Chunk("_".repeat(30), stdFont))
        doc.add(p)

        val font = Font(stdBaseFont, 10f)
        oath.forEach {
            p = Paragraph(it, font)
            doc.add(p)
        }

        p = Paragraph("Подпись пользователя", boldFont)
        p.indentationLeft = 270f
        p.add(Chunk("_".repeat(30), stdFont))
        doc.add(p)
    }

    private val stdBaseFont = getFont("fonts/Times_New_Roman.ttf")
    private val boldBaseFont = getFont("fonts/Times_New_Roman_Bold.ttf")
    private val underlineFont = Font(stdBaseFont, 8.5f)

    private val stdFont = Font(stdBaseFont, 11f)
    private val boldFont = Font(boldBaseFont, 11f)

    private val normStdFont = Font(stdBaseFont, 14f)
    private val titleFont = Font(boldBaseFont, 16f)
    private val maxLetters = 82

    private val oath = arrayOf(
            "Обязуюсь сдать все дела, документы, копии фонда пользования, превышающие лимит, до момента выдачи новых дел.",
            "Извещен, что в случае неисполнения данного требования дела, превышающие установленный лимит, будут",
            "возвращены архивом в архивохранилище.")

    private fun getFont(resourceFileName: String): BaseFont? {
        var font: BaseFont? = null
        try {
            javaClass.classLoader.getResourceAsStream(resourceFileName)
            font = BaseFont.createFont(resourceFileName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
        } catch (ex: DocumentException) {
            logger.error(ex) {}
        } catch (ex: IOException) {
            logger.error(ex) {}
        }
        return font
    }

    private fun createCell(text: String, rotation: Boolean = false, valign: Boolean = false, halign: Boolean = false, fontSize: Float = 10.5f) =
            PdfPCell(Phrase(text, Font(stdBaseFont, fontSize))).also {
                it.rotation = if (rotation) 90 else 0
                it.verticalAlignment = if (valign) Element.ALIGN_MIDDLE else Element.ALIGN_TOP
                it.horizontalAlignment = if (halign) Element.ALIGN_CENTER else Element.ALIGN_LEFT
                it.paddingLeft = 3f
                it.paddingBottom = 6f
            }

    private fun createCell(text: String) = createCell(text, false, true, true)

    private fun createPodpis(text: String, identation: Int, rise: Int): Paragraph {
        val ck = Chunk(text, underlineFont)
        ck.textRise = rise.toFloat()
        val p = Paragraph(ck)
        p.indentationLeft = identation.toFloat()
        return p
    }
}

