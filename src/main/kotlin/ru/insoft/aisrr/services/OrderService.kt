package ru.insoft.aisrr.services

import ru.insoft.aisrr.models.order.CheckedData
import ru.insoft.aisrr.models.order.OrderExecution
import ru.insoft.aisrr.models.order.OrderProcess
import ru.insoft.aisrr.models.order.OrderRegistration

interface OrderService {

    fun findRegistration(id: Long): OrderRegistration?
    fun findProcessing(id: Long): OrderProcess?
    fun findExecution(id: Long): OrderExecution?
    fun saveRegistration(registration: OrderRegistration)
    fun saveProcessing(process: OrderProcess)
    fun saveExecution(execution: OrderExecution)
    fun remove(id: Long)
    fun check(orderId: Long?, order: OrderRegistration? = null): CheckedData
}

