package ru.insoft.aisrr.services

import net.objectlab.kit.datecalc.common.DefaultHolidayCalendar
import net.objectlab.kit.datecalc.common.HolidayHandlerType
import net.objectlab.kit.datecalc.common.WorkingWeek
import net.objectlab.kit.datecalc.jdk8.Jdk8WorkingWeek
import net.objectlab.kit.datecalc.jdk8.LocalDateKitCalculatorsFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.repo.DaysMapper
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

/**
 * Определяет дату отстающую от заданной на определенное кол-во рабочих дней
 */
@Transactional(readOnly = true)
@Service
class DaysServiceImpl(private val daysMapper: DaysMapper) : DaysService {
    private val ww = object : Jdk8WorkingWeek(27) {}  // Понедельник, Вторник, Четверг, Пятница
    private val calcName = "mycalc"

    override fun getDate(rr: ReadingRoom, from: Date, vararg counts: Int): List<Date> {
        val cal = Calendar.getInstance()
        cal.time = from
        val startYear = cal.get(Calendar.YEAR)
        val startMonth = cal.get(Calendar.MONTH)
        val startDay = cal.get(Calendar.DATE)
        cal.add(Calendar.DATE, (counts.max() ?: 0) + 30) // Даем запас на месяц
        val holidays = daysMapper.days(listOf(startYear, cal.get(Calendar.YEAR)), listOf(startMonth, cal.get(Calendar.MONTH)), rr).flatMap { h ->
            h.days.map { d ->
                LocalDate.of(h.year, h.month + 1, d)
            }
        }.toMutableSet()
        LocalDateKitCalculatorsFactory.getDefaultInstance().registerHolidays(calcName, DefaultHolidayCalendar<LocalDate>(holidays))
        val dateCalculator = LocalDateKitCalculatorsFactory.getDefaultInstance().getDateCalculator(calcName, HolidayHandlerType.FORWARD)
        dateCalculator.setWorkingWeek(ww)

        dateCalculator.startDate = LocalDate.of(startYear, startMonth + 1, startDay)
        return counts.map {
            dateCalculator.moveByBusinessDays(it)
            Date.from(dateCalculator.currentBusinessDate.atStartOfDay(ZoneId.systemDefault()).toInstant())
        }
    }
}
