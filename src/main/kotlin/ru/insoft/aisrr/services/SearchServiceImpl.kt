package ru.insoft.aisrr.services

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.models.OrderCriteria
import ru.insoft.aisrr.models.Page
import ru.insoft.aisrr.models.UserCriteria
import ru.insoft.aisrr.models.VisitorCriteria
import ru.insoft.aisrr.offset
import ru.insoft.aisrr.repo.OrderMapper
import ru.insoft.aisrr.repo.ProfileMapper
import ru.insoft.aisrr.repo.VisitMapper
import ru.insoft.aisrr.repo.VisitorMapper

@Transactional(readOnly = true)
@Service
class SearchServiceImpl(private val profileMapper: ProfileMapper,
                        private val visitorMapper: VisitorMapper,
                        private val orderMapper: OrderMapper,
                        private val visitMapper: VisitMapper) : SearchService {

    override fun findVisitors(filter: VisitorCriteria, page: Long, size: Int) = runBlocking {
        val data = async { visitorMapper.findAll(filter, page.offset(size), size) }
        val count = async { visitorMapper.count(filter) }
        Page(data.await(), size, count.await(), page)
    }

    override fun findVisits(id: Long, page: Long, size: Int) = runBlocking {
        val data = async { visitMapper.findAll(id, page.offset(size), size) }
        val count = async { visitMapper.count(id) }
        Page(data.await(), size, count.await(), page)
    }

    override fun findUserOrders(id: Long, page: Long, size: Int) = runBlocking {
        val data = async { orderMapper.findUserOrders(id, page.offset(size), size) }
        val count = async { orderMapper.countUserOrders(id) }
        Page(data.await(), size, count.await(), page)
    }

    override fun findUsers(filter: UserCriteria, page: Long, size: Int) = runBlocking {
        val data = async { profileMapper.findAll(filter, page.offset(size), size) }
        val count = async { profileMapper.count(filter) }
        Page(data.await(), size, count.await(), page)
    }

    override fun findOrders(filter: OrderCriteria, page: Long, size: Int) = runBlocking {
        val data = async { orderMapper.findAll(filter, page.offset(size), size) }
        val count = async { orderMapper.count(filter) }
        Page(data.await(), size, count.await(), page)
    }
}


