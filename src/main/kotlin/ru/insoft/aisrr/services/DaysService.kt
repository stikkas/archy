package ru.insoft.aisrr.services

import ru.insoft.aisrr.enums.ReadingRoom
import java.util.*

interface DaysService {
    fun getDate(rr: ReadingRoom, from: Date, vararg counts: Int): List<Date>
}

