package ru.insoft.aisrr.services

import ru.insoft.aisrr.models.Visitor

interface VisitorService {

    fun findOne(id: Long): Visitor?
    fun save(visitor: Visitor)
    fun remove(id: Long)
}

