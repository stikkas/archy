package ru.insoft.aisrr.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aisrr.admin.models.UserDTO
import ru.insoft.aisrr.admin.service.UserService
import ru.insoft.aisrr.models.Profile
import ru.insoft.aisrr.models.WithId
import ru.insoft.aisrr.repo.ArchRecordMapper
import ru.insoft.aisrr.repo.LinkMapper
import ru.insoft.aisrr.repo.ProfileMapper
import ru.insoft.aisrr.repo.ResearchTopicMapper

@Service
class ProfileServiceImpl(private val profileMapper: ProfileMapper,
                         private val recordMapper: ArchRecordMapper,
                         private val researchTopicMapper: ResearchTopicMapper,
                         private val userService: UserService) : ProfileService {

    @Transactional
    override fun remove(id: Long) {
        profileMapper.remove(id)
    }

    @Transactional
    override fun save(profile: Profile) {
        val user = UserDTO().apply {
            firstName = profile.firstName
            lastName = profile.lastName
            middleName = profile.middleName
        }
        if (profile.id == null) {
            var lgn = transliterate("${user.lastName} ${user.firstName[0]}${user.middleName[0]}")
            var i = 0;
            while (true) {
                if (userService.existsLogin(lgn)) {
                    lgn += ++i
                } else {
                    break
                }
            }
            userService.save(user.apply {
                login = lgn
                password = "123"
            })
            profile.id = user.id
            profileMapper.insert(profile)
            val pn = profile.personNumber!!
            profile.regs?.forEach {
                it.archiveId?.let { ai ->
                    it.personNumber = "$pn-$ai"
                }
            }
        } else {
            user.id = profile.id
            userService.save(user)
            profileMapper.update(profile)
        }

        val profileId = profile.id!!

        saveLinks(profile.regs, profileId, recordMapper)
        profile.regs?.map { r ->
            saveLinks(r.topics, r.id!!, researchTopicMapper)
        }
        profile.regs?.let { rs ->
            if (rs.isNotEmpty()) {
                removeLinks(rs.flatMap { it.topics ?: listOf() }, rs.map { it.id!! }, researchTopicMapper)
            }
            removeLinks(rs, listOf(profileId), recordMapper)
        }
    }

    @Transactional(readOnly = true)
    override fun findOne(id: Long) = profileMapper.findOne(id)

    private fun <T : WithId> saveLinks(items: List<T>?, profileId: Long, mapper: LinkMapper<T>) = items?.forEach {
        if (it.id == null) {
            mapper.insert(it, profileId)
        } else {
            mapper.update(it)
        }
    }

    private fun <T : WithId> removeLinks(items: List<T>?, parentIds: List<Long>, mapper: LinkMapper<T>) = items?.let { its ->
        mapper.remove(its.map { it.id!! }, parentIds)
    }

    private fun transliterate(message: String): String {
        val builder = StringBuilder()
        for (element in message.toLowerCase()) {
            for (x in abcCyr.indices) {
                if (element == abcCyr[x]) {
                    builder.append(abcLat[x])
                    break
                }
            }
        }
        return builder.toString()
    }

    private val abcCyr = charArrayOf(' ', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м',
            'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы',
            'ь', 'э', 'ю', 'я', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z')
    private val abcLat = arrayOf("_", "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "y", "k", "l", "m",
            "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sch", "", "i",
            "", "e", "ju", "ja", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")
}


