package ru.insoft.aisrr

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import kotlin.math.max

@SpringBootApplication
class AisRRApplication

fun main(args: Array<String>) {
    runApplication<AisRRApplication>(*args)
}

inline fun Int.offset(size: Int) = (max(this, 1) - 1) * size
inline fun Long.offset(size: Int) = (max(this, 1) - 1) * size
