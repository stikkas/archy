package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.models.DictS
import ru.insoft.aisrr.models.Profile
import ru.insoft.aisrr.models.UserCriteria
import ru.insoft.aisrr.models.UserTableRow

@Mapper
interface ProfileMapper {
    fun findOne(id: Long): Profile?
    fun insert(profile: Profile)
    fun update(profile: Profile)
    fun remove(id: Long)
    fun registered(id: Long): Boolean

    fun findAll(@Param("filter") filter: UserCriteria,
                @Param("start") start: Long, @Param("limit") limit: Int): List<UserTableRow>

    fun count(@Param("filter") filter: UserCriteria): Long

    fun dict(@Param("query") query: String?, @Param("size") size: Int): List<DictS<Long>>
}

