package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.models.Visitor
import ru.insoft.aisrr.models.VisitorCriteria
import ru.insoft.aisrr.models.VisitorTableRow
import java.util.*

@Mapper
interface VisitorMapper {

    fun findAll(@Param("filter") filter: VisitorCriteria,
                @Param("start") start: Long, @Param("limit") limit: Int): List<VisitorTableRow>

    fun count(@Param("filter") filter: VisitorCriteria): Long

    fun findOne(id: Long): Visitor?
    fun insert(visitor: Visitor)
    fun update(visitor: Visitor)
    fun remove(id: Long)
    fun registered(id: Long): Boolean
    fun countVisits(@Param("start") start: Date, @Param("end") end: Date,
                    @Param("rr") readingRoom: ReadingRoom, @Param("archive") archive: Int): Long

    fun countUniqVisits(@Param("start") start: Date, @Param("end") end: Date,
                        @Param("rr") readingRoom: ReadingRoom, @Param("archive") archive: Int): Long
}

