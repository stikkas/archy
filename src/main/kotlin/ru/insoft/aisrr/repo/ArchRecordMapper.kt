package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import ru.insoft.aisrr.models.ArchRecord

@Mapper
interface ArchRecordMapper : LinkMapper<ArchRecord> {
    override fun insert(it: ArchRecord, parentId: Long)
    override fun update(it: ArchRecord)
    override fun remove(ids: List<Long>, parentIds: List<Long>)
}

