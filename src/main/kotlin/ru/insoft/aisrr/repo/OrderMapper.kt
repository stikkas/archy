package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.models.OrderCriteria
import ru.insoft.aisrr.models.OrderTableRow
import ru.insoft.aisrr.models.UserOrder
import ru.insoft.aisrr.models.order.*
import java.util.*

@Mapper
interface OrderMapper {
    fun findAll(@Param("filter") filter: OrderCriteria,
                @Param("start") start: Long, @Param("limit") limit: Int): List<OrderTableRow>

    fun count(@Param("filter") filter: OrderCriteria): Long

    fun findRegistration(id: Long): OrderRegistration?
    fun findProcessing(id: Long): OrderProcess?
    fun findExecution(id: Long): OrderExecution?
    fun insert(order: OrderRegistration)
    fun updateRegistration(order: OrderRegistration)
    fun updateProcessing(order: OrderProcess)
    fun updateExecution(order: OrderExecution)
    fun remove(id: Long)
    fun updateStatus(@Param("status") status: OrderState, @Param("id") id: Long,
                     @Param("returnDate") returnDate: Date? = null)

    fun findPrintOrder(id: Long): PrintOrder?
    fun findRelated(@Param("orderId") orderId: Long?,
                    @Param("applicantId") applicantId: Long?,
                    @Param("sts") states: List<OrderState> = listOf(
                            OrderState.ACCEPT, OrderState.ONEXEC, OrderState.READY, OrderState.EXEC)): List<CheckedDataOrder>

    fun findUserOrders(@Param("profileId") profileId: Long, @Param("offset") offset: Long, @Param("size") size: Int): List<UserOrder>
    fun countUserOrders(@Param("profileId") profileId: Long): Long
}

