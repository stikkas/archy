package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.models.Visit

@Mapper
interface VisitMapper {

    fun findAll(@Param("id") id: Long,
                @Param("start") start: Long, @Param("limit") limit: Int): List<Visit>

    fun count(@Param("id") id: Long): Long

}

