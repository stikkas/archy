package ru.insoft.aisrr.repo

interface LinkMapper<T> {
    fun insert(it: T, parentId: Long)
    fun update(it: T)
    fun remove(ids: List<Long>, parentIds: List<Long>)
}
