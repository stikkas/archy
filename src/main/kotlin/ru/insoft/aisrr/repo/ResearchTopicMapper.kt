package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import ru.insoft.aisrr.models.ResearchTopic

@Mapper
interface ResearchTopicMapper : LinkMapper<ResearchTopic> {
    override fun insert(it: ResearchTopic, parentId: Long)
    override fun update(it: ResearchTopic)
    override fun remove(ids: List<Long>, parentIds: List<Long>)
}

