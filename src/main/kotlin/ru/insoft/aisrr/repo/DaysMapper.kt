package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.models.Holiday

@Mapper
interface DaysMapper {

    fun days(@Param("years") years: List<Int>,
             @Param("months") months: List<Int>,
             @Param("rr") rr: ReadingRoom
    ): List<Holiday>
}
