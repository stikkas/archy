package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.models.Dict

@Mapper
interface DictMapper {

    fun countries(): List<Dict<Int>>

    fun archives(@Param("applicantId") applicantId: Long?): List<Dict<Int>>

    fun archivesShort(@Param("byId") byId: Boolean = false): List<Dict<Int>>

    fun educations(): List<Dict<Int>>

    fun scienceRanks(): List<Dict<Int>>

    fun topicHeadings(): List<Dict<Int>>

    fun logins(): List<String>

    fun months(): List<Dict<Int>>

    fun storages(@Param("rr") rr: ReadingRoom?, @Param("archive") archive: Int?): List<Dict<Int>>

    fun rejectReasons(): List<Dict<Int>>

    fun researchThemes(@Param("archive") archive: Int, @Param("profile") profile: Long): List<Dict<Long>>

    fun archivesRr(@Param("rr") rr: ReadingRoom?): List<Dict<Int>>

    fun rrsArchive(@Param("archiveId") archiveId: Int?): List<ReadingRoom>
}
