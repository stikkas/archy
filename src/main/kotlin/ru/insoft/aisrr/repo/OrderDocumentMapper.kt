package ru.insoft.aisrr.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.models.order.CheckedDocument
import ru.insoft.aisrr.models.order.OrderDocument
import ru.insoft.aisrr.models.order.PrintDocument

@Mapper
interface OrderDocumentMapper : LinkMapper<OrderDocument> {
    override fun insert(@Param("doc") it: OrderDocument, @Param("parentId") parentId: Long)
    override fun update(it: OrderDocument)
    override fun remove(@Param("ids") ids: List<Long>, @Param("parentIds") parentIds: List<Long>)
    fun findDocs(orderId: Long): List<OrderDocument>

    fun updateOnexec(doc: OrderDocument)
    fun updateReturn(doc: OrderDocument)
    fun updateDump(doc: OrderDocument)
    fun findPrintDocs(id: Long): List<PrintDocument>
    fun findCheckedDocs(id: Long): List<CheckedDocument>
}

