package ru.insoft.aisrr.controllers

import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.insoft.aisrr.admin.models.User
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.models.OrderCriteria
import ru.insoft.aisrr.models.UserCriteria
import ru.insoft.aisrr.models.VisitorCriteria
import ru.insoft.aisrr.services.SearchService

@RestController
@RequestMapping(SearchUrls.ROOT)
class SearchController(private val searchService: SearchService) {

    @GetMapping(SearchUrls.USERS)
    fun users(filter: UserCriteria,
              @RequestParam(defaultValue = "50") size: Int,
              @RequestParam page: Long): ActionStatus<*> {
        return try {
            ActionStatus.success(searchService.findUsers(filter, page, size))
        } catch (ex: Exception) {
            ActionStatus.except(ex.message)
        }
    }

    @GetMapping(SearchUrls.VISITORS)
    fun visitors(filter: VisitorCriteria,
                 @RequestParam(defaultValue = "50") size: Int,
                 @RequestParam page: Long): ActionStatus<*> {
        return try {
            ActionStatus.success(searchService.findVisitors(filter, page, size))
        } catch (ex: Exception) {
            ActionStatus.except(ex.message)
        }
    }

    @GetMapping(SearchUrls.VISITS)
    fun visits(@RequestParam id: Long, @RequestParam(defaultValue = "50") size: Int,
               @RequestParam page: Long): ActionStatus<*> {
        return try {
            ActionStatus.success(searchService.findVisits(id, page, size))
        } catch (ex: Exception) {
            ActionStatus.except(ex.message)
        }
    }

    @GetMapping(SearchUrls.USER_ORDERS)
    fun userOrders(@RequestParam id: Long, @RequestParam(defaultValue = "50") size: Int,
                   @RequestParam page: Long): ActionStatus<*> {
        return try {
            ActionStatus.success(searchService.findUserOrders(id, page, size))
        } catch (ex: Exception) {
            ActionStatus.except(ex.message)
        }
    }

    @GetMapping(SearchUrls.ORDERS)
    fun orders(filter: OrderCriteria,
               @RequestParam(defaultValue = "50") size: Int,
               @RequestParam page: Long, auth: Authentication): ActionStatus<*> {
        return try {
            var executor = false
            var chief = false
            val user = auth.principal as User
            user.roles?.let {
                it.forEach { r ->
                    if (r == Role.EXECUTOR) {
                        executor = true
                    } else if (r == Role.CHIEF_ARCHIVE) {
                        chief = true
                    }
                }
            }
            if (chief) {
                filter.state = if (filter.state == OrderState.PRE) null else filter.state
                filter.excludeStates = listOf(OrderState.PRE)
                filter.archive = user.archiveId
            } else if (executor) {
                filter.state = if (filter.state == OrderState.PRE || filter.state == OrderState.ACCEPT) null else filter.state
                filter.excludeStates = listOf(OrderState.PRE, OrderState.ACCEPT)
                filter.storeId = user.storageId
            }

            ActionStatus.success(searchService.findOrders(filter, page, size))
        } catch (ex: Exception) {
            ActionStatus.except(ex.message)
        }
    }
}

