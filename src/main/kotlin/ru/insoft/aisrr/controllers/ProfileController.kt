package ru.insoft.aisrr.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.models.Profile
import ru.insoft.aisrr.services.ProfileService
import ru.insoft.aisrr.validators.ProfileValidator
import javax.validation.Valid

@RestController
@RequestMapping("${Urls.PRIVATE}/profiles")
class ProfileController(private val profileService: ProfileService,
                        private val profileValidator: ProfileValidator) {

    @InitBinder("profile")
    fun initBinderProfile(binder: WebDataBinder) {
        binder.validator = profileValidator
    }

    @GetMapping("/{id}")
    fun getProfile(@PathVariable id: Long) = profileService.findOne(id)

    @PostMapping
    fun saveProfile(@Valid @RequestBody profile: Profile, errors: BindingResult) = if (errors.hasErrors()) {
        ActionStatus.invalid(errors.allErrors)
    } else {
        profileService.save(profile)
        ActionStatus.success(profile)
    }

    @DeleteMapping("/{id}")
    fun removeProfile(@PathVariable id: Long): Boolean {
        profileService.remove(id)
        return true;
    }
}
