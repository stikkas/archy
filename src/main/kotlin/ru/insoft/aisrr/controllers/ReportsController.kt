package ru.insoft.aisrr.controllers

import mu.KotlinLogging
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.insoft.aisrr.services.ReportService
import java.util.*
import javax.servlet.http.HttpServletResponse

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping("${Urls.PRIVATE}reports")
class ReportsController(private val reportService: ReportService) {

    @GetMapping("/quantity-rr")
    fun quantityRr(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam start: Date,
                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam end: Date, res: HttpServletResponse) {
        res.characterEncoding = "utf-8";
        res.setHeader("Content-Disposition", "attachment; filename=\"attendance-rr.pdf\"")
        res.contentType = "application/x-pdf; name=\"attendance-rr.pdf\""
        try {
            res.outputStream.use { out -> reportService.attendanceRr(start, end, out) }
        } catch (e: IllegalArgumentException) {
            logger.error(e) { "Неправильные даные запроса" }
            res.sendError(400)
        }
    }
}

