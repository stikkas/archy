package ru.insoft.aisrr.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.insoft.aisrr.enums.*
import ru.insoft.aisrr.services.DictService

@RestController
@RequestMapping(DictsUrls.ROOT)
class DictController(private val dictService: DictService) {

    @GetMapping("/countries")
    fun countries() = dictService.countries()

    @GetMapping("/archives-rr")
    fun archivesRr(@RequestParam(required = false) query: ReadingRoom?) = dictService.archivesRr(query)

    @GetMapping("/rrs-archive")
    fun rrsArchive(@RequestParam(required = false) query: Int?) = dictService.rrsArchive(query)

    @GetMapping("/archives")
    fun archives(@RequestParam(required = false) query: Long?) = dictService.archives(query)

    @GetMapping("/archives-short")
    fun archivesShort() = dictService.archivesShort()

    @GetMapping("/science-ranks")
    fun scienceRanks() = dictService.scienceRanks()

    @GetMapping("/educations")
    fun educations() = dictService.educations()

    @GetMapping("/topic-headings")
    fun topicHeadings() = dictService.topicHeadings()

    @GetMapping("/users")
    fun users(@RequestParam(required = false) query: String?, @RequestParam(defaultValue = "20") size: Int?) = dictService.users(query, size)

    @GetMapping("/executors")
    fun executors() = dictService.executors()

    @GetMapping("/months")
    fun months() = dictService.months()

    @GetMapping("/access-groups")
    fun groups() = dictService.groups()

    @GetMapping("/storages")
    fun storages(@RequestParam(required = false) rr: ReadingRoom?, @RequestParam(required = false) archive: Int?) = dictService.storages(rr, archive)

    @GetMapping("/research-themes")
    fun researchThemes(@RequestParam archive: Int, @RequestParam profile: Long) = dictService.researchThemes(archive, profile)

    @GetMapping("/logins")
    fun logins() = dictService.logins()

    @GetMapping("/order-states")
    fun orderStates() = OrderState.dict()

    @GetMapping("/order-types")
    fun orderTypes() = OrderType.dict()

    @GetMapping("/reading-rooms")
    fun readingRooms() = ReadingRoom.dict()

    @GetMapping("/access-roles")
    fun roles() = Role.dict()

    @GetMapping("/reject-reasons")
    fun rejectReasons() = dictService.rejectReasons()

    @GetMapping("/permission-issues")
    fun permissionIssues() = PermissionIssue.dict()
}

