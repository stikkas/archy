package ru.insoft.aisrr.controllers

import mu.KotlinLogging
import org.springframework.dao.DuplicateKeyException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import ru.insoft.aisrr.exception.ViolationException
import javax.servlet.http.HttpServletResponse

private val logger = KotlinLogging.logger {}

@ControllerAdvice
class ExceptionHandlerController {

    @ExceptionHandler(ViolationException::class)
    fun violation(response: HttpServletResponse, ex: ViolationException) = response.sendError(HttpStatus.CONFLICT.value(), ex.message)

    @ExceptionHandler(Exception::class)
    fun default(response: HttpServletResponse, ex: Exception) {
        logger.error(ex) { "Error caught by default handler of ExceptionHandlerController" }
        response.sendError(HttpStatus.NOT_FOUND.value(), ex.message)
    }

    @ExceptionHandler(DuplicateKeyException::class)
    fun duplicate(response: HttpServletResponse, ex: DuplicateKeyException) = response.sendError(HttpStatus.CONFLICT.value(),
            "Такая запись уже существует. Найдите ее через систему поиска и обновите, если хотите изменить.")
}
