package ru.insoft.aisrr.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import ru.insoft.aisrr.admin.controllers.PageUrls

@Controller
class PageController {
    @GetMapping(value = ["/main", "/users", "/users/**", "/visitors", "/visitors/**", "/orders", "/orders/**",
        "/reports", "/reports/**", "/admin", "/admin/**", PageUrls.LOGIN])
    fun pages() = "/"
}


