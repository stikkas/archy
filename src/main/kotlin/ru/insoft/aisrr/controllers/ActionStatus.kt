package ru.insoft.aisrr.controllers

import org.springframework.validation.ObjectError

class ActionStatus<T> {
    var status: Boolean = false
    var data: T? = null

    companion object {
        fun <U> success(data: U? = null) = ActionStatus<U>().apply {
            this.data = data
            status = true
        }

        fun except(message: String?) = ActionStatus<String>().apply {
            data = message
        }

        fun invalid(data: List<ObjectError>) = ActionStatus<List<ObjectError>>().apply {
            this.data = data
        }
    }
}
