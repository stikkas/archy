package ru.insoft.aisrr.controllers

import org.springframework.validation.BindingResult
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.models.Visitor
import ru.insoft.aisrr.services.VisitorService
import ru.insoft.aisrr.validators.VisitorValidator
import javax.validation.Valid

@RestController
@RequestMapping("${Urls.PRIVATE}/visitors")
class VisitorController(private val visitorService: VisitorService,
                        private val visitorValidator: VisitorValidator) {

    @InitBinder("visitor")
    fun initBinderProfile(binder: WebDataBinder) {
        binder.validator = visitorValidator
    }

    @GetMapping("/{id}")
    fun getVisitor(@PathVariable id: Long) = visitorService.findOne(id)

    @PostMapping
    fun saveVisitor(@Valid @RequestBody visitor: Visitor, errors: BindingResult) = if (errors.hasErrors()) {
        ActionStatus.invalid(errors.allErrors)
    } else {
        visitorService.save(visitor)
        ActionStatus.success(visitor)
    }

    @DeleteMapping("/{id}")
    fun removeVisitor(@PathVariable id: Long): Boolean {
        visitorService.remove(id)
        return true;
    }
}

