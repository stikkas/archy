package ru.insoft.aisrr.controllers

import mu.KotlinLogging
import org.springframework.security.core.Authentication
import org.springframework.validation.DirectFieldBindingResult
import org.springframework.validation.Validator
import org.springframework.web.bind.annotation.*
import ru.insoft.aisrr.admin.models.User
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.ReadingRoom
import ru.insoft.aisrr.enums.Role
import ru.insoft.aisrr.exception.ViolationException
import ru.insoft.aisrr.models.order.OrderExecution
import ru.insoft.aisrr.models.order.OrderProcess
import ru.insoft.aisrr.models.order.OrderRegistration
import ru.insoft.aisrr.services.OrderService
import ru.insoft.aisrr.services.ReportService
import ru.insoft.aisrr.validators.OrderExecutionValidator
import ru.insoft.aisrr.validators.OrderProcessValidator
import ru.insoft.aisrr.validators.OrderRegistrationValidator
import javax.servlet.http.HttpServletResponse

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping(OrderUrls.ROOT)
class OrderController(private val orderService: OrderService,
                      private val orderRegistrationValidator: OrderRegistrationValidator,
                      private val orderProcessValidator: OrderProcessValidator,
                      private val orderExecutionValidator: OrderExecutionValidator,
                      private val reportService: ReportService) {

    @GetMapping(OrderUrls.CHECK)
    fun check(@PathVariable id: Long) = orderService.check(id)

    @GetMapping(OrderUrls.PRINT)
    fun print(@PathVariable id: Long, res: HttpServletResponse) {
        res.characterEncoding = "utf-8";
        res.setHeader("Content-Disposition", "attachment; filename=\"order-details.pdf\"")
        res.contentType = "application/x-pdf; name=\"order-details.pdf\""
        try {
            res.outputStream.use { out -> reportService.orderDetails(id, out) }
        } catch (e: IllegalArgumentException) {
            logger.error(e) { "Неправильные даные запроса" }
            res.sendError(400)
        }
    }

    @GetMapping(OrderUrls.PROCESS)
    fun getProcessing(@PathVariable id: Long) = orderService.findProcessing(id)

    @GetMapping(OrderUrls.EXEC)
    fun getExecution(@PathVariable id: Long) = orderService.findExecution(id)

    @GetMapping(Urls.ID)
    fun getRegistration(@PathVariable id: Long) = orderService.findRegistration(id)

    @PostMapping(OrderUrls.EXEC)
    fun saveExecution(@RequestBody execution: OrderExecution,
                      auth: Authentication) = check(execution, orderExecutionValidator, "execution") {
        val user = auth.principal as User
        execution.executorId = user.id
        orderService.saveExecution(execution)
        ActionStatus.success(execution)
    }


    @PostMapping(OrderUrls.PROCESS)
    fun saveProcessing(@RequestBody process: OrderProcess,
                       auth: Authentication) = check(process, orderProcessValidator, "process") {
        val user = auth.principal as User
        process.chiefArchiveId = user.id
        orderService.saveProcessing(process)
        ActionStatus.success(process)
    }

    @PostMapping
    fun saveRegistration(@RequestBody registration: OrderRegistration,
                         auth: Authentication) = check(registration, orderRegistrationValidator, "registration") {
        if (registration.id == null || registration.status == OrderState.PRE || registration.status == OrderState.ACCEPT) {
            // пока заказ не оформлен меняем читальный зал на того кто выполняет эту операцию
            val user = auth.principal as User
            registration.registratorId = user.id
            registration.readingRoom = when (user.roles?.find { it == Role.RR1 || it == Role.RR2 || it == Role.RR3 }) {
                Role.RR1 -> ReadingRoom.RR1
                Role.RR2 -> ReadingRoom.RR2
                Role.RR3 -> ReadingRoom.RR3
                else -> throw ViolationException("Недостаточно прав для выполнения данной операции")
            }
        }
        if (registration.status == OrderState.ACCEPT && !orderService.check(registration.id, registration).pass) {
            throw ViolationException("Скорректируйте заказ. Превышение по количеству листов / дел")
        }
        orderService.saveRegistration(registration)
        ActionStatus.success(registration)
    }

    @DeleteMapping(Urls.ID)
    fun remove(@PathVariable id: Long): Boolean {
        orderService.remove(id)
        return true;
    }

    private fun check(target: Any, validator: Validator, name: String, block: () -> ActionStatus<out Any>): ActionStatus<out Any> {
        val errors = DirectFieldBindingResult(target, name)
        validator.validate(target, errors)
        return if (errors.hasErrors()) {
            ActionStatus.invalid(errors.allErrors)
        } else {
            block()
        }
    }
}

