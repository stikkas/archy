package ru.insoft.aisrr.controllers

object Urls {
    const val API = "/api"
    const val PRIVATE = "$API/private/"
    const val ID = "/{id}"
}

object SearchUrls {
    const val ROOT = "${Urls.PRIVATE}search"
    const val USERS = "/users"
    const val VISITORS = "/visitors"
    const val VISITS = "/visits"
    const val ORDERS = "/orders"
    const val USER_ORDERS = "/user-orders"
}

object DictsUrls {
    const val ROOT = "${Urls.PRIVATE}dicts"
}

object OrderUrls {
    const val ROOT = "${Urls.PRIVATE}orders"
    const val PROCESS = "${Urls.ID}/process"
    const val EXEC = "${Urls.ID}/execution"
    const val CHECK = "${Urls.ID}/check"
    const val PRINT = "${Urls.ID}/print"
}

