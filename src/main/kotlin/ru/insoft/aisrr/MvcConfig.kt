package ru.insoft.aisrr

import org.springframework.context.annotation.Configuration
import org.springframework.format.FormatterRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import ru.insoft.aisrr.formatters.PermissionIssueFormatter
import ru.insoft.aisrr.formatters.OrderStateFormatter
import ru.insoft.aisrr.formatters.OrderTypeFormatter
import ru.insoft.aisrr.formatters.ReadingRoomFormatter

@Configuration
class MvcConfig : WebMvcConfigurer {

    override fun addFormatters(registry: FormatterRegistry) {
        registry.addFormatter(ReadingRoomFormatter())
        registry.addFormatter(OrderStateFormatter())
        registry.addFormatter(OrderTypeFormatter())
        registry.addFormatter(PermissionIssueFormatter())
    }
}

