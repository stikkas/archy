package ru.insoft.aisrr.models

import org.springframework.format.annotation.DateTimeFormat
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.OrderType
import ru.insoft.aisrr.enums.ReadingRoom
import java.net.URLDecoder
import java.util.*

/**
 * Критерии поиска на странице "Журнал заказов"
 */
class OrderCriteria {
    /**
     * id архивохранилища для исполнителя
     */
    var storeId: Int? = null

    /**
     * Статусы, которые не должны отображаться для некоторых ролей
     */
    var excludeStates: List<OrderState>? = null

    /**
     * Читальный зал
     */
    var rr: ReadingRoom? = null

    /**
     * Номер заказа
     */
    var onumber: Long? = null

    /**
     * Отдел хранения
     */
    var archive: Int? = null

    /**
     * ФИО пользователя
     */
    var applicant: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Дата приема заказа
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var rdate: Date? = null

    /**
     * Дата исполнения
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var edate: Date? = null

    /**
     * Контрольная дата возврата
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var crdate: Date? = null

    /**
     * Вид заказа
     */
    var type: OrderType? = null

    /**
     * Статус заказа
     */
    var state: OrderState? = null

    /**
     * Исполнитель
     */
    var executor: Long? = null

    /**
     * Поле для сортировки
     */
    var sortField: String? = null

    /**
     * Направление сортировки
     */
    var sortOrder: String? = null
}

