package ru.insoft.aisrr.models

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.ReadingRoom
import java.util.*

class UserOrder {
    var id: Long? = null

    /**
     * № заказа
     */
    var nymber: Long? = null

    /**
     * Дата приема заказа
     */
    var regDate: Date? = null

    /**
     * Читальный зал
     */
    var room: String? = null
        get() = rr?.label

    @JsonIgnore
    var rr: ReadingRoom? = null

    /**
     * Отдел хранения
     */
    var archive: String? = null

    /**
     * Статус заказа
     */
    var state: String? = null
        get() = status?.label

    @JsonIgnore
    var status: OrderState? = null
}


