package ru.insoft.aisrr.models

import org.springframework.format.annotation.DateTimeFormat
import java.net.URLDecoder
import java.util.*

/**
 * Критерии поиска на странице "Посетители"
 */
class VisitorCriteria {
    /**
     * Дата посещения 'с'
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var fromDate: Date? = null

    /**
     * Дата посещения 'по'
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var toDate: Date? = null

    /**
     * Фамилия
     */
    var lname: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Имя
     */
    var fname: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Отчество
     */
    var mname: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Разовое посещение
     */
    var once: Boolean = false

}
