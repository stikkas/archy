package ru.insoft.aisrr.models

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.OrderType
import ru.insoft.aisrr.enums.ReadingRoom
import java.util.*

class OrderTableRow {
    var id: Long? = null

    /**
     * Номер заказа
     */
    var orderNumber: Long? = null

    /**
     * Читальный зал
     */
    @JsonIgnore
    var readingRoom: ReadingRoom? = null

    val readRoom: String
        get() = readingRoom?.let { it.label } ?: ""

    /**
     * Отдел хранения
     */
    var archive: String? = null

    /**
     * ФИО пользователя
     */
    var applicant: String? = null

    /**
     * Дата приемя заказа
     */
    var regDate: Date? = null

    /**
     * Дата исполнения
     */
    var execDate: Date? = null

    /**
     * Контрольная дата возврата
     */
    var returnDate: Date? = null

    /**
     * Вид заказа
     */
    @JsonIgnore
    var orderType: OrderType? = null

    val type: String
        get() = orderType?.let { it.label } ?: ""

    /**
     * Статус заказа
     */
    @JsonIgnore
    var state: OrderState? = null

    val status: String
        get() = state?.let { it.label } ?: ""

    /**
     * Исполнитель
     */
    var executor: String? = null
}



