package ru.insoft.aisrr.models

import java.util.*

class ResearchTopic : WithId {
    override var id: Long? = null
    var topicId: Int? = null
    var themeName: String? = null
    var period: String? = null
    var target: String? = null
    var startDate: Date? = null
    var endDate: Date? = null
}

