package ru.insoft.aisrr.models

import ru.insoft.aisrr.enums.ReadingRoom
import java.util.*

class Visitor {
    var id: Long? = null

    /**
     * Дата посещения
     */
    var visitDate: Date? = null

    /**
     * Фамилия
     */
    var lastName: String? = null

    /**
     * Имя
     */
    var firstName: String? = null

    /**
     * Отчество
     */
    var middleName: String? = null

    /**
     * Отделы хранения
     */
    var archives: List<Int>? = null

    /**
     * Разовое посещение
     */
    var once: Boolean = false

    /**
     * Документ - основание
     */
    var docCause: String? = null

    /**
     * Цель исследования
     */
    var target: String? = null

    /**
     * Используемые документы
     */
    var usedDocs: String? = null

    /**
     * Примечание
     */
    var remark: String? = null

    /**
     * Признак регистрации
     */
    var registered: Boolean = false

    /**
     * Профиль зарегистрированного пользователя
     */
    var profileId: Long? = null

    /**
     * Читальный зал
     */
    var rr: ReadingRoom? = null
}

