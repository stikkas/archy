package ru.insoft.aisrr.models

import org.apache.ibatis.annotations.Param

data class Dict<T>(@Param("value") val value: T, @Param("label") val label: String)
