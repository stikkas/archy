package ru.insoft.aisrr.models

import org.apache.ibatis.annotations.Param

data class DictS<T>( @Param("value") val value: T, @Param("short") val short: String, @Param("label") val label: String)
