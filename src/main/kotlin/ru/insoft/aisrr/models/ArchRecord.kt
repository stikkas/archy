package ru.insoft.aisrr.models

import java.util.*

class ArchRecord : WithId {
    override var id: Long? = null
    var archiveId: Int? = null
    var personNumber: String? = null
    var approveDate: Date? = null

    /**
     * Темы исследования
     */
    var topics: List<ResearchTopic>? = null
}

