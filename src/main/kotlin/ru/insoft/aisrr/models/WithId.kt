package ru.insoft.aisrr.models

interface WithId {
    var id: Long?
}
