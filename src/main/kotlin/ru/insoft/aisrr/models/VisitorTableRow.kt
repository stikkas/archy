package ru.insoft.aisrr.models

import java.util.*

class VisitorTableRow {
    var id: Long? = null

    /**
     * Дата посещения
     */
    var visitDate: Date? = null

    /**
     * Фамилия
     */
    var lastName: String? = null

    /**
     * Имя
     */
    var firstName: String? = null

    /**
     * Отчество
     */
    var middleName: String? = null

    /**
     * Отделы хранения
     */
    var archives: List<String>? = null

}

