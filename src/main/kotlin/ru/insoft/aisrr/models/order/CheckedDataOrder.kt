package ru.insoft.aisrr.models.order

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import ru.insoft.aisrr.enums.OrderState

class CheckedDataOrder {
    @JsonIgnore
    var id: Long = 0
    /**
     * id архива
     */
    @JsonIgnore
    var archiveId: Int? = null

    /**
     * Номер заказа
     */
    var orderNumber: Long? = null

    /**
     * Отдел хранения (архив)
     */
    var archive: String? = null

    /**
     * Кол-во дел
     */
    var files = 0
        get() = docs?.size ?: 0

    /**
     * Кол-во листов
     */
    var pages = 0
        get() = docs?.map { it.pages }?.sum() ?: 0
    /**
     * Статус заказа
     */
    @JsonIgnore
    var state: OrderState? = null

    var status: String = ""
        @JsonProperty("state") get() = state?.label ?: ""

    /**
     * Детальная информация по заказанным делам
     */
    @JsonIgnore
    var docs: List<CheckedDocument>? = null
}

