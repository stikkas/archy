package ru.insoft.aisrr.models.order

import ru.insoft.aisrr.enums.PermissionIssue
import java.util.*

class OrderProcess : AbstractOrder() {

    /**
     * Начальник отдела хранения
     */
    var chiefArchive: String? = null
    var chiefArchiveId: Long? = null

    /**
     * Разрешение на выдачу
     */
    var permissionIssue: PermissionIssue? = null
        get() = field ?: PermissionIssue.ALLOWED

    /**
     * Дата разрешения / отказа
     */
    var permissionDate: Date? = null

    /**
     * Изменение сроков выдачи
     */
    var changeIssueDate: Date? = null

    /**
     * Причина (изменения сроков выдачи)
     */
    var changeIssueReason: String? = null

    /**
     * Номер хранилища
     */
    var storageId: Int? = null

    /**
     * Причина отказа
     */
    var rejectReason: String? = null
}

