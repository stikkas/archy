package ru.insoft.aisrr.models.order

import java.util.*

class CheckedData {
    /**
     * Прошла или нет проверка
     */
    var pass = true

    /**
     * Дата проверки
     */
    val date = Date()

    /**
     *  Кол-во дел
     */
    var files = 0

    /**
     * Кол-во листов
     */
    var pages = 0

    /**
     * Информация по заказам
     */
    var orders: List<CheckedDataOrder>? = null

}
