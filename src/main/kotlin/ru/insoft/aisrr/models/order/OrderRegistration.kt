package ru.insoft.aisrr.models.order

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class OrderRegistration : AbstractOrder() {

    /**
     * Сотрудник читального зала
     */
    var registrator: String? = null
    var registratorId: Long? = null

    /**
     * Дата приемя заказа
     */
    var regDate: Date? = null

    /**
     * Дата выдачи
     */
    var issueDate: Date? = null

    /**
     * Дата возврата (фактическая)
     */
    var returnDate: Date? = null

    /**
     * Контрольная дата возврата
     */
    var controlReturnDate: Date? = null

    /**
     * Причина
     */
    var cause: String? = null
}

