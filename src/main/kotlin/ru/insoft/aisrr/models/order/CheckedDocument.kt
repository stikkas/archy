package ru.insoft.aisrr.models.order

class CheckedDocument {
    /**
     * Номер фонда
     */
    var fundNumber: String? = null

    /**
     * Кол-во листов
     */
    var pages = 0
}

