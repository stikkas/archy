package ru.insoft.aisrr.models.order

import java.util.*

class OrderExecution : AbstractOrder() {
    /**
     * Исполнитель
     */
    var executor: String? = null
    var executorId: Long? = null
    /**
     * Дата выдачи
     */
    var issueDate: Date? = null
    /**
     * Номер хранилища
     */
    var storageId: Int? = null
}


