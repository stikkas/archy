package ru.insoft.aisrr.models.order

import org.apache.ibatis.annotations.Param
import ru.insoft.aisrr.enums.OrderType

/**
 * Используется для отображения на бланке
 */
data class PrintOrder(@Param("archive") val archive: String, @Param("type") val type: OrderType,
                      @Param("fio") val fio: String, @Param("personNumber") val personNumber: Int,
                      @Param("thema") val thema: String)

