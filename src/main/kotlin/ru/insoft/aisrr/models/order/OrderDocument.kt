package ru.insoft.aisrr.models.order

import java.util.*

class OrderDocument {
    var id: Long? = null
    /**
     * Номер фонда
     */
    var fundNumber: String? = null

    /**
     * Номер описи
     */
    var opisNumber: String? = null

    /**
     * Номер ед. хранения / дела
     */
    var unitNumber: String? = null

    /**
     * Заголовок единицы хранения
     */
    var unitTitle: String? = null

    /**
     * Кол-во листов
     */
    var pages: Int? = null

    /**
     *  Дело подготовлено
     */
    var prepared: Boolean? = null

    /**
     * Причина отказа
     */
    var rejectReasonId: Int? = null

    /**
     * Списано
     */
    var spisanoDate: Date? = null

    /**
     * Возвращено в архивохранилище
     */
    var returnDate: Date? = null
}
