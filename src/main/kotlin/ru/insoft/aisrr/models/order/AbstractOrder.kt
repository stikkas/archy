package ru.insoft.aisrr.models.order

import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.OrderType
import ru.insoft.aisrr.enums.ReadingRoom
import java.util.*

abstract class AbstractOrder {
    var id: Long? = null

    /**
     * Читальный зал
     * привызывается по регистратору на стадии регистрации, дальше только для чтения
     */
    var readingRoom: ReadingRoom? = null

    val readRoom: String
        get() = readingRoom?.let { it.label } ?: ""

    /**
     * Читальный зал выдачи заказа
     */
    var issueReadingRoom: ReadingRoom? = null

    val issueReadRoom: String
        get() = issueReadingRoom?.let { it.label } ?: ""

    /**
     * Статус заказа
     */
    var status: OrderState? = null

    /**
     * Номер заказа
     */
    var orderNumber: Long? = null

    /**
     * Тема исследования
     */
    var topicHeadingId: Long? = null

    /**
     * Отдел хранения
     */
    var archiveId: Int? = null

    /**
     * Вид заказа
     */
    lateinit var type: OrderType

    /**
     * Данные по документам
     */
    var docs: List<OrderDocument>? = null

    /**
     * Идентификатор пользователя делавшего заказ
     */
    var applicantId: Long? = null

    /**
     * Выдано в ЧЗ
     */
    var issueRrDate: Date? = null

    // Только для отображения в просмотре
    var lastName: String? = null
    var firstName: String? = null
    var middleName: String? = null
    var birthYear: Int? = null
    var personNumber: Int? = null
}

