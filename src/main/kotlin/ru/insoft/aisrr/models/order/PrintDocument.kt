package ru.insoft.aisrr.models.order

import org.apache.ibatis.annotations.Param

/**
 * Используется для отображения на бланке
 */
data class PrintDocument(@Param("fundNumber") val fundNumber: String,
                         @Param("opisNumber") val opisNumber: String,
                         @Param("unitNumber") val unitNumber: String,
                         @Param("unitTitle") val unitTitle: String,
                         @Param("pages") val pages: Int?,
                         @Param("reason") val reason: String?)
