package ru.insoft.aisrr.models

import java.util.*

class Profile {
    var id: Long? = null
    var lastName: String = ""
    var firstName: String = ""
    var middleName: String = ""

    var changeFIO: String? = null
    var birthYear: Int? = null
    var countryId: Int? = null
    var personNumber: Int? = null
    var firstRegDate: Date? = null
    var issuePassDate: Date? = null
    var ban: Boolean = false
    var banCause: String? = null
    var organization: String? = null
    var position: String? = null
    var educationId: Int? = null
    var scienceRankId: Int? = null
    var remark: String? = null
    var oldNumbers: String? = null
    var email: String? = null
    var registered: Boolean = false

    /**
     * регистрации в архивах
     */
    var regs: List<ArchRecord>? = null
}
