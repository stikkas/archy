package ru.insoft.aisrr.models

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.insoft.aisrr.enums.ReadingRoom
import java.util.*

class Visit {
    var id: Long? = null

    /**
     * Дата посещения
     */
    var date: Date? = null

    /**
     * Читальный зал
     */
    var room: String? = null
        get() = rr?.label

    @JsonIgnore
    var rr: ReadingRoom? = null

    /**
     * Отделы хранения
     */
    var archives: List<String>? = null
}


