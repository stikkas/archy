package ru.insoft.aisrr.models

data class Holiday(val year: Int, val month: Int, val days: List<Int>)

