package ru.insoft.aisrr.models

class UserTableRow {
    var id: Long? = null

    /**
     * Номер личного дела
     */
    var personNumber: Int? = null

    /**
     * Фамилия
     */
    var lastName: String? = null

    /**
     * Имя
     */
    var firstName: String? = null

    /**
     * Отчество
     */
    var middleName: String? = null

    /**
     * Год рождения
     */
    var birthYear: Int? = null

    /**
     * Отдел хранения
     */
    var archives: List<String>? = null
}

