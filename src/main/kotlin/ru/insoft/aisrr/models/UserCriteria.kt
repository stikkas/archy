package ru.insoft.aisrr.models

import java.net.URLDecoder

/**
 * Критерии поиска на странице "Пользователи"
 */
class UserCriteria {
    /**
     * Фамилия
     */
    var lname: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Имя
     */
    var fname: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Отчество
     */
    var mname: String? = null
        get() = field?.let { URLDecoder.decode(field, "UTF-8") }

    /**
     * Год рождения
     */
    var byear: Int? = null

    /**
     * № личного дела
     */
    var pnumber: Int? = null

    override fun toString(): String {
        return "UserCriteria(lname=$lname, fname=$fname, mname=$mname, byear=$byear, pnumber=$pnumber)"
    }

}
