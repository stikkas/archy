package ru.insoft.aisrr.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.OrderType

@MappedTypes(OrderType::class)
class OrderTypeHandler : AbstractEnumHandler<OrderType>(OrderType.FP)

