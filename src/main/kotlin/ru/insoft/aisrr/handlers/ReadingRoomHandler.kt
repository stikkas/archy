package ru.insoft.aisrr.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.ReadingRoom

@MappedTypes(ReadingRoom::class)
class ReadingRoomHandler : AbstractEnumHandler<ReadingRoom>(ReadingRoom.RR1)

