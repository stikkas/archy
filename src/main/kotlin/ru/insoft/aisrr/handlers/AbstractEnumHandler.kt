package ru.insoft.aisrr.handlers

import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import ru.insoft.aisrr.enums.IntId
import java.sql.CallableStatement
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

abstract class AbstractEnumHandler<T : IntId>(private val example: T) : BaseTypeHandler<T>() {
    @Throws(SQLException::class)
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: T, jdbcType: JdbcType?) {
        ps.setInt(i, parameter.id)
    }

    @Throws(SQLException::class)
    override fun getNullableResult(rs: ResultSet, columnName: String) = byValue(rs.getInt(columnName))

    @Throws(SQLException::class)
    override fun getNullableResult(rs: ResultSet, columnIndex: Int) = byValue(rs.getInt(columnIndex))

    @Throws(SQLException::class)
    override fun getNullableResult(cs: CallableStatement, columnIndex: Int) = byValue(cs.getInt(columnIndex))

    private fun byValue(value: Int) = this.example.codes().find { it.id == value } as? T
}
