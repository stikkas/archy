package ru.insoft.aisrr.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.OrderState

@MappedTypes(OrderState::class)
class OrderStateHandler : AbstractEnumHandler<OrderState>(OrderState.ACCEPT)
