package ru.insoft.aisrr.handlers

import org.apache.ibatis.type.MappedTypes
import ru.insoft.aisrr.enums.PermissionIssue

@MappedTypes(PermissionIssue::class)
class PermissionIssueHandler : AbstractEnumHandler<PermissionIssue>(PermissionIssue.ALLOWED)
