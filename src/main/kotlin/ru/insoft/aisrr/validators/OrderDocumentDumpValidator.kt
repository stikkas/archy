package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.order.OrderDocument

/**
 * Проверяется когда заказ переходит в состояние "Списано"
 */
@Component
class OrderDocumentDumpValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderDocument) {
        if (rejectReasonId == null) {
            spisanoDate ?: errors.rejectValue("spisanoDate", "Списано")
        }
    }

    override fun supports(clazz: Class<*>) = OrderDocument::class.java == clazz
}

