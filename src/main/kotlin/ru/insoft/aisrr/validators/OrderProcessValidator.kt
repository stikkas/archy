package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.enums.PermissionIssue
import ru.insoft.aisrr.models.order.OrderProcess

@Component
class OrderProcessValidator : Validator {
    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderProcess) {
        permissionIssue ?: errors.rejectValue("permissionIssue", "Разрешение на выдачу")
        if (status == OrderState.REJECT) {
            if (permissionIssue != PermissionIssue.DENIED) {
                errors.rejectValue("permissionIssue", "Неверное значение разрешения на выдачу")
            }
            ValidationUtils.rejectIfEmpty(errors, "rejectReason", "Причина отказа")
        } else if (status == OrderState.ONEXEC) {
            issueReadingRoom ?: errors.rejectValue("issueReadingRoom", "Читальный зал выдачи заказа")
            storageId ?: errors.rejectValue("storageId", "№ хранилища")
        } else if (status != OrderState.ACCEPT) {
            errors.rejectValue("status", "Статус")
        }
    }

    override fun supports(clazz: Class<*>) = OrderProcess::class.java == clazz
}
