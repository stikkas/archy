package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.order.OrderDocument

/**
 * Проверяется когда заказ переходит в состояние "Подготовлен"
 */
@Component
class OrderDocumentReadyValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderDocument) {
        pages ?: errors.rejectValue("pages", "Количество листов")
        if (prepared != true) {
            rejectReasonId ?: errors.rejectValue("rejectReasonId", "Причина отказа")
        }
    }

    override fun supports(clazz: Class<*>) = OrderDocument::class.java == clazz
}
