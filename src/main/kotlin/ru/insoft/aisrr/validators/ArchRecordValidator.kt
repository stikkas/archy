package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.ArchRecord

@Component
class ArchRecordValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as ArchRecord) {
        archiveId ?: errors.rejectValue("archiveId", "Название отдела")
        approveDate ?: errors.rejectValue("approveDate", "Дата разрешения")
    }

    override fun supports(clazz: Class<*>) = ArchRecord::class.java == clazz
}

