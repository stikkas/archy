package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.models.order.OrderRegistration

@Component
class OrderRegistrationValidator(private val docValidator: OrderDocumentValidator,
                                 private val dumpValidator: OrderDocumentDumpValidator) : AbstractOrderValidator() {

    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderRegistration) {
        applicantId ?: errors.rejectValue("applicantId", "Пользователь")
        val emptyDocs = docs.isNullOrEmpty()
        val notPreOrder = !(status == null || status == OrderState.PRE)
        if (notPreOrder) {
            if (emptyDocs) {
                errors.rejectValue("docs", "Документы")
            }
        }
        if (status == OrderState.ACCEPT) {
            topicHeadingId ?: errors.rejectValue("topicHeadingId", "Тема исследования")
            archiveId ?: errors.rejectValue("archiveId", "Отдел хранения")
            if (!emptyDocs) {
                checkDocs(docs!!, errors, docValidator)
            }
        } else if (status == OrderState.DUMP) {
            if (!emptyDocs) {
                checkDocs(docs!!, errors, dumpValidator)
            }
        } else if (notPreOrder && !(status == OrderState.EXEC || status == OrderState.CANCEL1
                        || status == OrderState.CANCEL2 || status == OrderState.CANCEL3)) {
            errors.rejectValue("status", "Статус")
        }
    }

    override fun supports(clazz: Class<*>) = OrderRegistration::class.java == clazz
}
