package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.order.OrderDocument

/**
 * Проверяется когда заказ переходит в состояние "Возвращено в хранилище"
 */
@Component
class OrderDocumentReturnValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderDocument) {
        if (rejectReasonId == null) {
            returnDate ?: errors.rejectValue("returnDate", "Возвращено в архивохранилище")
        }
    }

    override fun supports(clazz: Class<*>) = OrderDocument::class.java == clazz
}

