package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.insoft.aisrr.enums.OrderState
import ru.insoft.aisrr.models.order.OrderExecution

@Component
class OrderExecutionValidator(private val readyValidator: OrderDocumentReadyValidator,
                              private val returnValidator: OrderDocumentReturnValidator) : AbstractOrderValidator() {

    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderExecution) {
        if (docs.isNullOrEmpty()) {
            errors.rejectValue("docs", "Документы")
        } else {
            val documents = docs!!
            when (status) {
                OrderState.ONEXEC, OrderState.DUMP -> null
                OrderState.READY -> checkDocs(documents, errors, readyValidator)
                OrderState.EXEC -> issueRrDate ?: errors.rejectValue("issueRrDate", "Выдано в ЧЗ")
                OrderState.RETURN -> checkDocs(documents, errors, returnValidator)
                else -> errors.rejectValue("status", "Статус")
            }
        }
    }

    override fun supports(clazz: Class<*>) = OrderExecution::class.java == clazz
}

