package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.ArchRecord
import ru.insoft.aisrr.models.Profile

@Component
class ProfileValidator(private val topicValidator: ResearchTopicValidator,
                       private val archRecordValidator: ArchRecordValidator) : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as Profile) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "Фамилия")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "Имя")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "middleName", "Отчество")
        if (registered) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthYear", "Год рождения")
            countryId ?: errors.rejectValue("countryId", "Гражданство")
            firstRegDate ?: errors.rejectValue("firstRegDate", "Дата первой регистрации")
            if (regs.isNullOrEmpty()) {
                errors.rejectValue("regs", "Принадлежность к архиву")
            } else {
                regs?.let {
                    validateRegistrations(it, errors)
                }
            }

        }
    }

    override fun supports(clazz: Class<*>) = Profile::class.java == clazz

    private fun validateRegistrations(regs: List<ArchRecord>, errors: Errors) {
        for ((i, r) in regs.withIndex()) {
            r?.let {
                errors.pushNestedPath(String.format("regs[%d]", i))
                ValidationUtils.invokeValidator(archRecordValidator, it, errors);
                if (r.topics.isNullOrEmpty()) {
                    errors.rejectValue("topics", "Тема исследования")
                } else {
                    r.topics?.let { ts ->
                        validateTopics(ts, errors)
                    }
                }
                errors.popNestedPath()
            }
        }
    }

    private fun validateTopics(topics: List<*>, errors: Errors) {
        for ((i, t) in topics.withIndex()) {
            t?.let {
                errors.pushNestedPath(String.format("topics[%d]", i))
                ValidationUtils.invokeValidator(topicValidator, it, errors);
                errors.popNestedPath()
            }
        }
    }
}
