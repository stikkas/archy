package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.order.OrderDocument

@Component
class OrderDocumentValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as OrderDocument) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fundNumber", "№ фонда")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "opisNumber", "№ описи")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "unitNumber", "№ ед. хранения/дела")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "unitTitle", "Заголовок единицы хранения")
    }

    override fun supports(clazz: Class<*>) = OrderDocument::class.java == clazz
}
