package ru.insoft.aisrr.validators

import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.order.OrderDocument

abstract class AbstractOrderValidator : Validator {

    protected fun checkDocs(docs: List<OrderDocument>, errors: Errors, validator: Validator) {
        docs.let {
            for ((i, d) in it.withIndex()) {
                d?.let { doc ->
                    errors.pushNestedPath(String.format("docs[%d]", i))
                    ValidationUtils.invokeValidator(validator, doc, errors);
                    errors.popNestedPath()
                }
            }
        }
    }
}

