package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.Visitor

@Component
class VisitorValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as Visitor) {
        if (once) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "Фамилия")
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "Имя")
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "middleName", "Отчество")
        } else if (profileId == null) {
            errors.rejectValue("profileId", "Пользователь")
        }
        if (registered) {
            if (archives.isNullOrEmpty()) {
                errors.rejectValue("archives", "Отдел хранения")
            }
            if (visitDate == null) {
                errors.rejectValue("visitDate", "Дата посещения")
            }
        }
    }

    override fun supports(clazz: Class<*>) = Visitor::class.java == clazz

}
