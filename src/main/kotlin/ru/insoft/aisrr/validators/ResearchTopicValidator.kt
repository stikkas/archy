package ru.insoft.aisrr.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator
import ru.insoft.aisrr.models.ResearchTopic

@Component
class ResearchTopicValidator : Validator {

    override fun validate(target: Any, errors: Errors): Unit = with(target as ResearchTopic) {
        topicId ?: errors.rejectValue("topicId", "Рубрикатор тем")
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "themeName", "Название темы")
        startDate ?: errors.rejectValue("startDate", "Дата начальная")
        endDate ?: errors.rejectValue("endDate", "Дата конечная")
    }

    override fun supports(clazz: Class<*>) = ResearchTopic::class.java == clazz
}
