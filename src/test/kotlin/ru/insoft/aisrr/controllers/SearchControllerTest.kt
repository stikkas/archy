package ru.insoft.aisrr.controllers

import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.util.UriComponentsBuilder
import ru.insoft.aisrr.models.Page
import ru.insoft.aisrr.models.UserTableRow
import javax.sql.DataSource

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(scripts = ["classpath:/profiles.sql", "classpath:/store-departments.sql"])
@ActiveProfiles("test")
class SearchControllerTest {

    @LocalServerPort
    private var port: Int = 0

    @Autowired
    private lateinit var template: TestRestTemplate

    @Autowired
    private lateinit var dataSource: DataSource

    private lateinit var jdbcTemplate: JdbcTemplate

    @Before
    fun init() {
        jdbcTemplate = JdbcTemplate(dataSource)
    }

    @After
    fun afterTest() {
        jdbcTemplate.execute("delete from profile")
        jdbcTemplate.execute("delete from archive")
    }

    @Test
    fun search() {
        check(8)
        check(2, "МотылЬ")
        check(2, "Мотылькова", "мариН")
        check(2, "Мотылькова", "Марина", "василь")
        check(2, "мотыль", "Марина", "Васильевна", "VVD-13-1198")
        check(2, byear = 1968, fname = "марин")
    }

    private fun check(found: Int, lname: String? = null, fname: String? = null, mname: String? = null,
                      pnumber: String? = null, byear: Int? = null) {
        val builder = UriComponentsBuilder.fromHttpUrl("http://localhost:$port${SearchUrls.ROOT}${SearchUrls.USERS}")
                .queryParam("page", 1)

        lname?.let {
            builder.queryParam("lname", it)
        }

        fname?.let {
            builder.queryParam("fname", it)
        }

        mname?.let {
            builder.queryParam("mname", it)
        }

        pnumber?.let {
            builder.queryParam("pnumber", it)
        }

        byear?.let {
            builder.queryParam("byear", it)
        }

        val uri = builder.toUriString()
        val res = template
                .exchange(uri, HttpMethod.GET, null, object : ParameterizedTypeReference<ActionStatus<Page<UserTableRow>>>() {})
                .body!!
        assertThat(res.status).isTrue()
        val page = res.data as Page<UserTableRow>;
        assertThat(page.first).isTrue()
        assertThat(page.count.toInt()).isEqualTo(found)
        assertThat(page.last).isTrue()
        assertThat(page.pages).isEqualTo(1)
        assertThat(page.size).isEqualTo(50)
        assertThat(page.data.size).isEqualTo(found)
    }
}
