package ru.insoft.aisrr.services

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class ReportServiceImplTest {

    @Autowired
    private lateinit var reportService: ReportService

    @Test
    fun orderDetails() {
        val path = Paths.get(System.getProperty("java.io.tmpdir"), "order-details.pdf")

        Files.newOutputStream(path).use {
            reportService.orderDetails(10, it)
        }

        Runtime.getRuntime().exec("/usr/bin/evince $path");
    }

    @Test
    fun attendanceRr() {
        val path = Paths.get(System.getProperty("java.io.tmpdir"), "attendance-rr.pdf")

        Files.newOutputStream(path).use {
            val now = Calendar.getInstance()
            val end = now.time
            now.add(Calendar.YEAR, -1)
            val start = now.time
            reportService.attendanceRr(start, end, it)
        }

        Runtime.getRuntime().exec("/usr/bin/evince $path");
    }
}
